﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfRouteControl
{
    // 2-я часть контрол.xaml.cs
    public partial class RouteControl : UserControl
    {

        // Добавить пустые строки DataGrid **************************************************************************
        // Добавление пустых строк:
        // - при изменении размера datagrid

        private void AddEmptyRows()
        {
            /*
                        try
                        {
                            int сountRowsAll = dgvSRanges.Items.Count; // количество всех строк в таблице
                            double hs = 23; // высота строки
                            double ah = dgvSRanges.ActualHeight; // визуализированная высота dataGrid
                            double chh = dgvSRanges.ColumnHeaderHeight; // высота заголовка

                            int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                            int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                            int index = -1;
                            for (int i = 0; i < count; i++)
                            {
                                // Удалить пустые строки в dgv
                                index = ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges.ToList().FindIndex(x => x.Id < 0);
                                if (index != -1)
                                {
                                    ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges.RemoveAt(index);
                                }
                            }

                            List<TableSectorsRanges> list = new List<TableSectorsRanges>();
                            for (int i = 0; i < countRows - сountRowsAll; i++)
                            {
                                TableSectorsRanges strSR = new TableSectorsRanges
                                {
                                    Id = -2,
                                    AngleMin = -2,
                                    AngleMax = -2
                                };
                                list.Add(strSR);
                            }

                            for (int i = 0; i < list.Count; i++)
                            {
                                ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges.Add(list[i]);
                            }
                        }
                        catch { }
            */


            try
            {
                int сountRowsAll = dgRoute.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = dgRoute.ActualHeight; // визуализированная высота dataGrid
                double chh = dgRoute.ColumnHeaderHeight; // высота заголовка

                // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки
                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs);


                /*
                                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                                int index = -1;
                                for (int i = 0; i < count; i++)
                                {
                                    // Удалить пустые строки в dgv
                                    // ???????????????????????????????
                                    index = ((GlobalRouteParam)dgRoute.DataContext).CollRouteParam.ToList().FindIndex(x => x.objtableRoute.Id < 0);
                                    if (index != -1)
                                    {
                                        ((GlobalRouteParam)dgRoute.DataContext).CollRouteParam.RemoveAt(index);
                                    }
                                }
                */
                /*
                                List<TableSectorsRanges> list = new List<TableSectorsRanges>();
                                for (int i = 0; i < countRows - сountRowsAll; i++)
                                {
                                    TableSectorsRanges strSR = new TableSectorsRanges
                                    {
                                        Id = -2,
                                        AngleMin = -2,
                                        AngleMax = -2
                                    };
                                    list.Add(strSR);
                                }

                                for (int i = 0; i < list.Count; i++)
                                {
                                    ((GlobalSRanges)dgvSRanges.DataContext).CollectionSRanges.Add(list[i]);
                                }
                */


                List<RouteParam> list = new List<RouteParam>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    RouteParam strSR = new RouteParam();
                    strSR.Caption = "77";//.objtableRoute.Caption = "77";
                    list.Add(strSR);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalRouteParam)dgRoute.DataContext).CollRouteParam.Add(list[i]);
                }


            } // TRY
            catch { }



        }
        // ************************************************************************** Добавить пустые строки DataGrid



    } // Class
} // Namespsce
