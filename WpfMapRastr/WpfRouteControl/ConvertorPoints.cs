﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System;
using System.Globalization;
using System.Windows.Data;

namespace WpfRouteControl
{
    [ValueConversion(sourceType: typeof(int), targetType: typeof(string))]
    public class ConvertorPoints: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string ss = "";

            ss = System.Convert.ToString((int)value);

            return ss;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {

            int ii = 0;
            string ss = "";

            ss = (string)value;
            ii = System.Convert.ToInt32(ss);

            return ii;
        }

    } // Class
} // Namespace
