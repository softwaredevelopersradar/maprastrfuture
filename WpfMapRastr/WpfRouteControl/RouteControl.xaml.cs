﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using ModelsTablesDBLib;

namespace WpfRouteControl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class RouteControl : UserControl
    {
        public RouteControl()
        {
            InitializeComponent();

            dgRoute.DataContext = new GlobalRouteParam();
            dgRoute.ItemsSource = (dgRoute.DataContext as GlobalRouteParam).CollRouteParam;
            //((GlobalRouteParam)dgRoute.DataContext).CollRouteParam обращение к коллекции

        }

        // CHANGE ***************************************************************************************
        private void ButtonChange_Click(object sender, RoutedEventArgs e)
        {

        }
        // *************************************************************************************** CHANGE

        // ADD ******************************************************************************************
        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {

        }
        // ****************************************************************************************** ADD

        // DELETE ***************************************************************************************
        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {

        }
        // *************************************************************************************** DELETE

        // CLEAR ****************************************************************************************
        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {

        }
        // **************************************************************************************** CLEAR

        // SizeChanged **********************************************************************************
        // Добавление пустых строк при изменении размера datagrid

        private void dgRoute_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }
        // ********************************************************************************** SizeChanged

        
        //private void AddEmptyRows()
        //{
/*
            try
            {
                dgRoute.Items.Refresh();
                int сountRowsAll = dgRoute.Items.Count; // количество имеющихся строк в таблице
                int maxRows = 10;
                for (int i = 0; i < maxRows - сountRowsAll; i++)
                {
                    ((GlobalState)dgRoute.DataContext).FullParams.Add(new FullParam() { letter = i + 1 });
                }
            }
            catch { }
*/
        //}






    }  // Class
} // Namespace
