﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ClientDataBase;

using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

using System.Windows.Threading;
using System.Threading.Tasks;

// Otl
// Для YAML реализации
using YamlDotNet.Serialization;

using WpfMapRastrControl;
using WpfAzimuthControl;
using ModelsTablesDBLib;
using Bearing;
using GeoCalculator;
using InheritorsEventArgs;
using ClassLibraryIniFiles;
using ControlSettingsMap;
using ADSBControl;
using AzimutTask;

using System.ComponentModel;
using System.Runtime.CompilerServices;

// Semen
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;

namespace WpfMapRastr
{
    public partial class MainWindow
    {
        // LOAD_TABLES ****************************************************************************************
        private async void LoadTables1()
        //private void LoadTables1()
        {
            // JS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // JS

            // Здесь просто отрисуются все станции, которые получили, если нажата птичка по станциям
            var stations = await clientDB.Tables[NameTable.TableASP].LoadAsync<TableASP>();

            // Убрать без координат
            if (stations.Count > 0)
            {
                for (int iijjs = (stations.Count - 1); iijjs >= 0; iijjs--)
                {
                    if (
                        (stations[iijjs].Coordinates.Latitude == -1) ||
                        (stations[iijjs].Coordinates.Longitude == -1) ||
                        (stations[iijjs].Coordinates.Latitude == 0) ||
                        (stations[iijjs].Coordinates.Longitude == 0)
                        )
                    {
                        try
                        {
                            stations.Remove(stations[iijjs]);
                        }
                        catch
                        { }
                    }
                }
            }

            if (stations.Count > 0)
            {
                for (int iijjss = (stations.Count - 1); iijjss >= 0; iijjss--)
                {
                    try
                    {
                        stations[iijjss].Coordinates.Latitude = stations[iijjss].Coordinates.Latitude < 0 ? stations[iijjss].Coordinates.Latitude * -1 : stations[iijjss].Coordinates.Latitude;
                        stations[iijjss].Coordinates.Longitude = stations[iijjss].Coordinates.Longitude < 0 ? stations[iijjss].Coordinates.Longitude * -1 : stations[iijjss].Coordinates.Longitude;
                    }
                    catch
                    { }
                }
            }

            int flPel = -1;

            if ((mapCtrl.objClassInterfaceMap.ShowJS == true) && (mapCtrl.objClassInterfaceMap.Show_SRW_STRF_RS == true))
                flPel = 1;

            // Добавляем элементы из входного массива от БД (тип TableASP) в
            // массив классов АСП (List<ClassJS>)
            if (stations.Count > 0)
            {
                for (int iijjs1 = 0; iijjs1 < stations.Count; iijjs1++)
                {
                    objClassFunctionsMain.AddOneJS(stations[iijjs1], flPel);
                }

            }
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> JS

            // ИРИ ФРЧ >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            //List<TempFWS> lstTempFWS = new List<TempFWS>();
            var obj1 = await clientDB.Tables[NameTable.TempFWS].LoadAsync<TempFWS>();
            //lstTempFWS = obj1;

            // Otl ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
            // QQQ  Otladka
/*          
                                    TempFWS obj;

                                    Random rand = new Random();
                                    Random rand1 = new Random();
                                    int j = 0;
                                    int j1 = 0;
                                    int f = 50;
                                    double lt_temp = 0;
                                    double ln_temp = 0;

                                    for (j1 = 0; j1 < 10; j1++)
                                    {
                                        for (j = 0; j < 5; j++)
                                        {
                                            lt_temp = Convert.ToDouble(rand.Next(GlobalVarMapMain.lat1_temp1, GlobalVarMapMain.lat2_temp1)) / 100000;
                                            ln_temp = Convert.ToDouble(rand1.Next(GlobalVarMapMain.lon1_temp1, GlobalVarMapMain.lon2_temp1)) / 100000;
                                            obj = new TempFWS();
                                            obj.Coordinates = new Coord();
                                            obj.Coordinates.Latitude = lt_temp;
                                            obj.Coordinates.Longitude = ln_temp;
                                            obj.FreqKHz = f;
                                            //e.Table.Add(obj);
                                            obj1.Add(obj);

                                    }
                                         if (j1 < 4)
                                            f += 50;
                                        else
                                            f += 250;
                                    }

                        obj1[0].ListQ = new System.Collections.ObjectModel.ObservableCollection<JamDirect>();
                        JamDirect o1 = new JamDirect();
                        o1.NumberASP = 111;
                        o1.Bearing = 50;
                        obj1[0].ListQ.Add(o1);
            

            
                        for(int ii=0;ii<20;ii++)
                        {
                            obj1[ii].Coordinates.Latitude = 56.18;
                            obj1[ii].Coordinates.Longitude = 22.98 + ii * 0.3;
                        }
*/            
            // ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo Otl

            // ----------------------------------------------------------------- OnTempFWSUp

            // Свой список __________________________________________________________________________
            // Создание своего списка классов ИРИ ФРЧ (в т.ч. для пеленгов)

            // Добавляем элементы из входного массива от БД (тип TempFWS) в
            // массив классов ИРИ ФРЧ (List<Class_IRIFRCH>)
            if (obj1.Count > 0)
            {
                for (int iijfr1 = 0; iijfr1 < obj1.Count; iijfr1++)
                {
                    objClassFunctionsMain.AddOneIRIFRCH(obj1[iijfr1]);
                }

            }

            // QQQ Otladka
            //mapCtrl.objClassInterfaceMap.List_SRW_FRF[0].IsSelected = true;
            // __________________________________________________________________________ Свой список


            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ИРИ ФРЧ

            // ИРИ ФРЧ ЦР >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            var obj2 = await clientDB.Tables[NameTable.TableReconFWS].LoadAsync<TableReconFWS>();
            mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD = obj2;

            if (mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.Count > 0)
            {
                for (int iijf1 = (mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.Count - 1); iijf1 >= 0; iijf1--)
                {
                    if (
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD[iijf1].Coordinates.Latitude == -1) ||
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD[iijf1].Coordinates.Longitude == -1) ||
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD[iijf1].Coordinates.Latitude == 0) ||
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD[iijf1].Coordinates.Longitude == 0)
                        )
                    {
                        try
                        {
                            mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.Remove(mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD[iijf1]);
                        }
                        catch
                        { }
                    }
                }
            }

            // !!! Перерисовка в конце
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ИРИ ФРЧ ЦР

            // ИРИ ФРЧ РП >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            var obj3 = await clientDB.Tables[NameTable.TableSuppressFWS].LoadAsync<TableSuppressFWS>();
            mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS = obj3;

            if (mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.Count > 0)
            {
                for (int iijf2 = (mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.Count - 1); iijf2 >= 0; iijf2--)
                {
                    if (
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS[iijf2].Coordinates.Latitude == -1) ||
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS[iijf2].Coordinates.Longitude == -1) ||
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS[iijf2].Coordinates.Latitude == 0) ||
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS[iijf2].Coordinates.Longitude == 0)
                        )
                    {
                        // 1202
                        try
                        {
                            mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.Remove(mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS[iijf2]);
                        }
                        catch
                        { }
                    }
                }
            }

            // !!! Перерисовка в конце
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ИРИ ФРЧ РП

            // ИРИ ППРЧ >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            var obj4 = await clientDB.Tables[NameTable.TableReconFHSS].LoadAsync<TableReconFHSS>();
            mapCtrl.objClassInterfaceMap.List_SRW_STRF_Recon = obj4;
            var obj4_1 = await clientDB.Tables[NameTable.TableSourceFHSS].LoadAsync<TableSourceFHSS>();
            mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord = obj4_1;

            // Otl1 ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
            // QQQ1  Otladka
            // Для ввода своих ППРЧ просто откомментировать этот кусок otl1
            /*
                                                TableReconFHSS obj;
                                                TableSourceFHSS obj_1;

                                                Random rand = new Random();
                                                Random rand1 = new Random();
                                                int j = 0;
                                                int j1 = 0;
                                                int f = 50;
                                                int num = 0;
                                                double lt_temp = 0;
                                                double ln_temp = 0;

                                                for (j1 = 0; j1 < 10; j1++)

                                                {
                                                    for (j = 0; j < 5; j++)
                                                    {
                                                        lt_temp = Convert.ToDouble(rand.Next(GlobalVarMapMain.lat1_temp1, GlobalVarMapMain.lat2_temp1)) / 100000;
                                                        ln_temp = Convert.ToDouble(rand1.Next(GlobalVarMapMain.lon1_temp1, GlobalVarMapMain.lon2_temp1)) / 100000;
                                                        obj = new TableReconFHSS();
                                                        obj_1 = new TableSourceFHSS();
                                                        obj.Id = num;
                                                        obj_1.IdFHSS = num;
                                                        obj_1.Coordinates = new Coord();
                                                        obj_1.Coordinates.Latitude = lt_temp;
                                                        obj_1.Coordinates.Longitude = ln_temp;
                                                        obj.FreqMinKHz = f;
                                                        obj.FreqMaxKHz = f + 2;
                                                        obj4.Add(obj);
                                                        obj4_1.Add(obj_1);
                                                        num += 1;

                                                }
                                                     if (j1 < 4)
                                                        f += 50;
                                                    else
                                                        f += 250;
                                                }
            */
            // ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo Otl1



            //obj1[0].ListQ = new System.Collections.ObjectModel.ObservableCollection<JamDirect>();
            //JamDirect o1 = new JamDirect();
            //o1.NumberASP = 111;
            //o1.Bearing = 50;
            //obj1[0].ListQ.Add(o1);

            // ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo Otl



            /*
                        if (mapCtrl.objClassInterfaceMap.List_SRW_STRF.Count != 0)
                        {
                            for (int iijf3 = (mapCtrl.objClassInterfaceMap.List_SRW_STRF.Count - 1); iijf3 >= 0; iijf3--)
                            {
                                int indxf = -1;
                                indxf = mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord.FindIndex(x => (x.IdFHSS == mapCtrl.objClassInterfaceMap.List_SRW_STRF[iijf3].Id));
                                if (indxf >= 0)
                                {
                                    if (
                                        (mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord[indxf].Coordinates.Latitude == -1) ||
                                        (mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord[indxf].Coordinates.Longitude == -1) ||
                                        (mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord[indxf].Coordinates.Latitude == 0) ||
                                        (mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord[indxf].Coordinates.Longitude == 0)
                                        )
                                        mapCtrl.objClassInterfaceMap.List_SRW_STRF.Remove(mapCtrl.objClassInterfaceMap.List_SRW_STRF[iijf3]);
                                }
                            }
                        }
            */


            // Свой список __________________________________________________________________________
            // Создание своего списка классов ИРИ ППРЧ (в т.ч. для пеленгов)

            // Добавляем элементы из входного массива от БД (тип TableReconFHSS+TableSourceFHSS) в
            // массив классов ИРИ ППРЧ (List<Class_IRIPPRCH>)

            if (obj4_1.Count > 0)
            {
                for (int iijfr5 = 0; iijfr5 < obj4_1.Count; iijfr5++)
                {
                    objClassFunctionsMain.AddOneIRIPPRCH(obj4_1[iijfr5], obj4);
                }

            }

            // __________________________________________________________________________ Свой список

            // !!! Перерисовка в конце
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ИРИ ППРЧ

            // ИРИ ППРЧ РП >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            // !!! Не рисуем
            var obj5 = await clientDB.Tables[NameTable.TableSuppressFHSS].LoadAsync<TableSuppressFHSS>();
            mapCtrl.objClassInterfaceMap.List_SRW_STRF_RS = obj5;

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ИРИ ППРЧ РП

            // Airplanes >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            List<TempADSB> listTableADSB = new List<TempADSB>();

            var objAirPl = await clientDB.Tables[NameTable.TempADSB].LoadAsync<TempADSB>();
            listTableADSB = objAirPl;

            // Если нет координат -> удаляем элемент из списка
            //  NEW1
            if (listTableADSB.Count > 0)
            {
                for (int iijl = (listTableADSB.Count - 1); iijl >= 0; iijl--)
                {
                    if (
                        (listTableADSB[iijl].Coordinates.Latitude == -1) ||
                        (listTableADSB[iijl].Coordinates.Longitude == -1) ||
                        (listTableADSB[iijl].Coordinates.Latitude == 0) ||
                        (listTableADSB[iijl].Coordinates.Longitude == 0)
                        )
                        if (listTableADSB.Count > 0)
                        {
                            // 1202
                            try
                            {
                                listTableADSB.Remove(listTableADSB[iijl]);
                            }
                            catch
                            {
                                //MessageBox.Show("Error1");
                            }

                        }
                }
            }
            // ...............................................................

            // NEW1
            GlobalVarMapMain.fl_AirPlane = 0;
            objClassFunctionsMain.GetADSB_DB(listTableADSB, 0);

            DispatchIfNecessary(() =>
            {
                if (GlobalVarMapMain.FlagLanguage == false)
                    mapCtrl.Txt10.Text = "Number of aircraft:" + " " + Convert.ToString(mapCtrl.objClassInterfaceMap.List_AP.Count);
                else
                    mapCtrl.Txt10.Text = "Количество ВО:" + " " + Convert.ToString(mapCtrl.objClassInterfaceMap.List_AP.Count);


            });

            // ForArsen
            List<TempADSB> lst1 = new List<TempADSB>();

            for (int ia1 = 0; ia1 < mapCtrl.objClassInterfaceMap.List_AP.Count; ia1++)
            {
                ModelsTablesDBLib.TempADSB ob1 = new TempADSB();

                ob1.Coordinates = new Coord();
                ob1.ICAO = mapCtrl.objClassInterfaceMap.List_AP[ia1].ICAO;
                ob1.Id = mapCtrl.objClassInterfaceMap.List_AP[ia1].Id;
                ob1.Coordinates.Altitude = mapCtrl.objClassInterfaceMap.List_AP[ia1].Coordinates.Altitude;
                ob1.Coordinates.Latitude = mapCtrl.objClassInterfaceMap.List_AP[ia1].Coordinates.Latitude;
                ob1.Coordinates.Longitude = mapCtrl.objClassInterfaceMap.List_AP[ia1].Coordinates.Longitude;
                ob1.Time = mapCtrl.objClassInterfaceMap.List_AP[ia1].Time;
                lst1.Add(ob1);
            }

            // Arsen_Tab
            try
            {
                aDSBControl.AddRange(lst1);
            }
            catch
            {
                //MessageBox.Show("Error2");

            }
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Airplanes

            // Clear and redraw >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Clear and redraw

            // -------------------------------------------------------------------------------------------------
            // Подписка на все остальные события

            // Updated table of ИРИ ФРЧ
            // Для очистки
            (clientDB.Tables[NameTable.TempFWS] as ITableUpdate<TempFWS>).OnUpTable += OnTempFWSUp;
            (clientDB.Tables[NameTable.TempFWS] as ITableAddRange<TempFWS>).OnAddRange += OnTempFWSAddRange;

            // Updated table of ИРИ ФРЧ ЦР
            (clientDB.Tables[NameTable.TableReconFWS] as ITableUpdate<TableReconFWS>).OnUpTable += OnTableReconFWSUp;
            // !!! Не надо
            //(clientDB.Tables[NameTable.TableReconFWS] as ITableAddRange<TableReconFWS>).OnAddRange += OnTableReconFWSAddRange;

            // Updated table of ИРИ ФРЧ РП
            (clientDB.Tables[NameTable.TableSuppressFWS] as ITableUpdate<TableSuppressFWS>).OnUpTable += OnTableSuppressFWSUp;

            // Updated table of ИРИ ППРЧ РП
            (clientDB.Tables[NameTable.TableSuppressFHSS] as ITableUpdate<TableSuppressFHSS>).OnUpTable += OnTableSuppressFHSSUp;

            // ИРИ ППРЧ
            // Для очистки
            (clientDB.Tables[NameTable.TableReconFHSS] as ITableUpdate<TableReconFHSS>).OnUpTable += OnTableReconFHSSUp;
            // For Coordinates
            // Для очистки
            (clientDB.Tables[NameTable.TableSourceFHSS] as ITableUpdate<TableSourceFHSS>).OnUpTable += OnTableSourceFHSSUp;
            (clientDB.Tables[NameTable.TableReconFHSS] as ITableAddRange<TableReconFHSS>).OnAddRange += OnTableReconFHSSAddRange;
            (clientDB.Tables[NameTable.TableSourceFHSS] as ITableAddRange<TableSourceFHSS>).OnAddRange += OnTableSourceFHSSAddRange;

            // Updated table of ASP (СП)
            (clientDB.Tables[NameTable.TableASP] as ITableUpdate<TableASP>).OnUpTable += OnTableAspUp;

            // ??????????????????????????
            // Updated table of AP
            // OLD
            //(clientDB.Tables[NameTable.TempADSB] as ITableUpdate<TempADSB>).OnUpTable += OnTempADSB;
            //fill ADSB table by planes + Map
            (clientDB.Tables[NameTable.TempADSB] as ITableUpdate<TempADSB>).OnUpTable += HandlerUpdateTempADSB;
            (clientDB.Tables[NameTable.TempADSB] as ITableUpRecord<TempADSB>).OnAddRecord += HandlerAddRecordTempADSB;

            //  GPS
            (clientDB.Tables[NameTable.TempGNSS] as ITableUpdate<TempGNSS>).OnUpTable += OnTempGNSSUp;
            // -------------------------------------------------------------------------------------------------
            GlobalVarMapMain.fltbl = true;
            // -------------------------------------------------------------------------------------------------


        }

        // **************************************************************************************** LOAD_TABLES

    } // Class
} // Namespace
