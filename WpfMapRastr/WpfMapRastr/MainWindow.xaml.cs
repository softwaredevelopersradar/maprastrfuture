﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ClientDataBase;

using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

using System.Windows.Threading;
using System.Threading.Tasks;

// Otl
// Для YAML реализации
using YamlDotNet.Serialization;
// Для JSON реализации
//using System.Runtime.Serialization.Json;
//using System.Runtime.Serialization;
// Для реализации XML
//using System.Xml;
//using System.Xml.Linq;
//using ClassLibraryIniFiles;

using WpfMapRastrControl;
//using WpfTasksControl;
using WpfAzimuthControl;
using ModelsTablesDBLib;
using Bearing;
using GeoCalculator;
using InheritorsEventArgs;
using ClassLibraryIniFiles;
using ControlSettingsMap;
using ADSBControl;
using AzimutTask;

using System.ComponentModel;
using System.Runtime.CompilerServices;

// Semen
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;


namespace WpfMapRastr
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        // -------------------------------------------------------------------------------------------
        public WindowAzimuth objWindowAzimuth;
        public WindowTriang objWindowTriang;

        public ClassFunctionsMain objClassFunctionsMain;

        public GlobalVarLanguageYaml globalVarLanguageYaml;
        // -------------------------------------------------------------------------------------------

        // Az_птичка >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        // Azimuth*
        // Для азимута (нажатие/отжатие птички)

        private bool isShowProp;
        public bool IsShowProp
        {
            get => isShowProp;
            set
            {
                if (value == isShowProp) return;
                isShowProp = value;
                OnPropertyChanged();
            }
        }

        // !!! Обработчик смотри в блоке азимута
        public event PropertyChangedEventHandler PropertyChanged;
        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Az_птичка

        // --------------------------------------------------------------------------------------------
        // AAA
        // Для кнопки языка

        //public event EventHandler OnButtonLanguage;
        // --------------------------------------------------------------------------------------------

        // Constructor ********************************************************************************
        public MainWindow()
        {

            // ----------------------------------------------------------------------------------------
            InitializeComponent();
            // ----------------------------------------------------------------------------------------
            this.Loaded += MainWindow_Loaded;
            this.Closing += MainWindow_Closing;
            //this.Closed += MainWindow_Closed;
            // ----------------------------------------------------------------------------------------
            // Other windows

            objWindowAzimuth = new WindowAzimuth();
            GlobalVarMapMain.objWindowAzimuthG = objWindowAzimuth;

            objWindowTriang = new WindowTriang();
            GlobalVarMapMain.objWindowTriangG = objWindowTriang;

            objClassFunctionsMain = new ClassFunctionsMain();
            // ----------------------------------------------------------------------------------------

            // EVENTS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            // Click Button of Azimuth task (WpfTasksControl)
            //tasksCtrl.OnTaskAzimuth += ButtonAz;
            //tasksCtrl.OnTaskTriang += ButtonTriang;

            // Click Button "Calc" in Triangulation Control
            //objWindowTriang.tasksTriang.OnCalcTriang += ButtonCalcTriang;

            // Click Button "Clear" in Azimuth Control
            //objWindowAzimuth.tasksAzimuth.OnClearAzimuth += ButtonClearAzimuth;
            // Show azimuth title  near station
            //objWindowAzimuth.tasksAzimuth.OnCheckedShowAzimuth += CheckShowAzimuth;
            //objWindowAzimuth.tasksAzimuth.OnUnCheckedShowAzimuth += UnCheckShowAzimuth;

            // Mouse Click On the map
            mapCtrl.OnMouseCl += MapCtrl_OnMouseCl;
            ADSBCleanerEvent.OnCleanADSBTable += ADSBCleanerEvent_OnCleanADSBTable;

            //11_13new
            // Двойной клик по строке таблицы самолетов
            aDSBControl.OnSelectedRowADSB += new EventHandler<string>(ADSBControl_OnSelectedRowADSB);

            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
            // Language
            // Изменение языка в контроле Settings

            SettingsMap.OnLanguageChanged += SettingsMap_OnLanguageChanged;
            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> EVENTS

            // LANGUAGE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // Language
            // Установка языка проекта 

            // !!!
            // Считать язык из Settings
            //var lng = SettingsMap.Settings.Language;
            // Проверка
            //if (Convert.ToString(lng) == "Rus")
            // Установка языка в Settings
            //SettingsMap.Settings = new ClassSettings
            //{
            //    Language = ControlSettingsMap.Languages.Eng
            //};


            // .......................................................................................
            // Считываем текущий язык из файла LanguageSettings.yaml

            // Чтение
            // !!! До конструктора описан
            //GlobalVarLanguageYaml globalVarLanguageYaml = new GlobalVarLanguageYaml();
            //globalVarLanguageYaml = new GlobalVarLanguageYaml();

            //globalVarLanguageYaml.lang= YamlLoad("LanguageSettings.yaml");
             //GlobalVarLanguageYaml globalVarLanguageYaml = YamlLoad<GlobalVarLanguageYaml>("LanguageSettings.yaml");
              globalVarLanguageYaml = YamlLoad<GlobalVarLanguageYaml>("LanguageSettings.yaml");

            // Запись
            //globalVarLanguageYaml.lang = "Rus";
            //YamlSave<GlobalVarLanguageYaml>(globalVarLanguageYaml, "LanguageSettings.yaml");
            // .......................................................................................
            // Головная программа

            GlobalVarMapMain.LNG = globalVarLanguageYaml.lang;

            if (GlobalVarMapMain.LNG == "Eng")
                GlobalVarMapMain.FlagLanguage = false;
            else
                GlobalVarMapMain.FlagLanguage = true;

            ChoiceLanguage();
            // .......................................................................................
            // !!! В контроле Settings язык установим после считывании других переменных из .Yaml файла

            // .......................................................................................
            // Azimuth
            // Установка соответствующего языка в контроле азимута

            if (GlobalVarMapMain.FlagLanguage==true) // Русский
            {
                AzimutTask.Enums.Languages languages = AzimutTask.Enums.Languages.Rus;
                AzimutTaskCntr.ChangeLanguge(languages);
            }
            else
            {
                AzimutTask.Enums.Languages languages1 = AzimutTask.Enums.Languages.Eng;
                AzimutTaskCntr.ChangeLanguge(languages1);
            }
            // .......................................................................................
            // ADSB

            if (GlobalVarMapMain.FlagLanguage == true) // Русский
            {
                aDSBControl.TextBlockLen1.Text = "Кол-во ВО без координат: ";
                aDSBControl.TextBlockLen2.Text = "Кол-во ВО с координатами: ";
                aDSBControl.ColLen1.Header = "Фл";
                aDSBControl.ColLen2.Header = "Тип";
                aDSBControl.ColLen3.Header = "Шир,°         Долг,°";
                aDSBControl.ColLen5.Header = " Выс, м";
                aDSBControl.TTPrint.Content = "Печать файла";
                aDSBControl.TTSave.Content = "Сохранить файл как Word/Doc";
                aDSBControl.TTDel.Content = "Удалить все";
            }
            else
            {
                aDSBControl.TextBlockLen1.Text = "№ planes w/o coordinates: ";
                aDSBControl.TextBlockLen2.Text = "№ planes with coordinates: ";
                aDSBControl.ColLen1.Header = "Fl";
                aDSBControl.ColLen2.Header = "Type";
                aDSBControl.ColLen3.Header = "Lat,°         Long,°";
                aDSBControl.ColLen5.Header = " Alt, m";
                aDSBControl.TTPrint.Content = "Print file";
                aDSBControl.TTSave.Content = "Save file as Word/Doc";
                aDSBControl.TTDel.Content = "Delete all";
            }
            // .......................................................................................
            // Route

            //RouteTaskCntr.
            if (GlobalVarMapMain.FlagLanguage == true) // Русский
            {
                //RouteTaskCntr.bAdd
            }
            else
            {

            }
            // .......................................................................................

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> LANGUAGE

            // YAML >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // From YAML

            string s = "";

            InitMapYaml initMapYaml = new InitMapYaml();
            s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);

            // File .yaml was read --------------------------------------------------------------------

            if (s == "")
            {
                // To Property Grid
                SettingsMap.Settings = new ClassSettings
                {
                    // 0 - DD.dddddd
                    // 1 - DD MM.mmmmmm
                    // 2-  DD MM SS.ssssss
                    FormCoord = (FormatCoordinates)initMapYaml.FormatCoord,
                    IP_DB = initMapYaml.IP_DB,
                    NumPort = initMapYaml.NumPort_DB,
                    // 0 - WGS84degree
                    // 1 - SK42degree
                    // 2 - SK42_XY
                    // 3 - Mercator
                    SysCoord = (SystemCoordinates)initMapYaml.SysCoord,
                    Path = initMapYaml.Path,
                    PathMatr = initMapYaml.PathMatr,
                    // 1 - Gegraphic
                    // 0 - Mercator
                    TypeProjection = (TypesProjection)initMapYaml.TypeMap

                };

                // ..............................................................................
                // Language in Setting

                if (GlobalVarMapMain.FlagLanguage == false) // Английский
                    SettingsMap.Settings.Language = ControlSettingsMap.Languages.Eng;
                else
                    SettingsMap.Settings.Language = ControlSettingsMap.Languages.Rus;
                // ..............................................................................

                // ..............................................................................
                // Azimuth* SysCoord

                AzimutTask.Enums.EnumSysCoord enumSysCoord=new AzimutTask.Enums.EnumSysCoord();
                if ((initMapYaml.SysCoord == 2) || (initMapYaml.SysCoord == 3))
                    enumSysCoord = AzimutTask.Enums.EnumSysCoord.XY;
                else
                {
                    if(initMapYaml.FormatCoord==0)
                    {
                        enumSysCoord = AzimutTask.Enums.EnumSysCoord.Degree;
                    }
                    else
                    {
                        enumSysCoord = AzimutTask.Enums.EnumSysCoord.DdMmSs;
                    }

                } // degree
                AzimutTaskCntr.ChangeSystemCoord(enumSysCoord);
                // ..............................................................................


            } 
            // -------------------------------------------------------------------- File .yaml was read

            // File .yaml wasn't read -----------------------------------------------------------------

            else
            {
                if(GlobalVarMapMain.FlagLanguage==false)
                  MessageBox.Show("Can't open .yaml file");
                else
                    MessageBox.Show("Невозможно открыть .yaml файл");

            } 
            // ----------------------------------------------------------------- File .yaml wasn't read

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> YAML


            // ----------------------------------------------------------------------------------------
            GlobalVarMapMain.objMainWindowG = this;
            // ----------------------------------------------------------------------------------------
            // Azimuth*

            DataContext = this;
            // ----------------------------------------------------------------------------------------


        }
        // ******************************************************************************** Constructor

        // Change_Language ****************************************************************************
        // Language
        //  Событие от контрола Settings об изменении языка 

        private void SettingsMap_OnLanguageChanged(object sender, ControlSettingsMap.Languages e)
        {
            //throw new NotImplementedException();


            // .......................................................................................
            // Azimuth
            // Установка соответствующего языка в контроле азимута

            if (Convert.ToString(e) == "Rus")
            {
                AzimutTask.Enums.Languages languages = AzimutTask.Enums.Languages.Rus;
                AzimutTaskCntr.ChangeLanguge(languages);
            }
            else
            {
                AzimutTask.Enums.Languages languages1 = AzimutTask.Enums.Languages.Eng;
                AzimutTaskCntr.ChangeLanguge(languages1);
            }
            // .......................................................................................
            // ADSB

            if (Convert.ToString(e) == "Rus")
            {
                aDSBControl.TextBlockLen1.Text = "Кол-во ВО без координат: ";
                aDSBControl.TextBlockLen2.Text = "Кол-во ВО с координатами: ";
                aDSBControl.ColLen1.Header = "Фл";
                aDSBControl.ColLen2.Header = "Тип";
                aDSBControl.ColLen3.Header = "Шир,°         Долг,°";
                aDSBControl.ColLen5.Header = " Выс, м";
                aDSBControl.TTPrint.Content = "Печать файла";
                aDSBControl.TTSave.Content = "Сохранить файл как Word/Doc";
                aDSBControl.TTDel.Content = "Удалить все";
            }
            else
            {
                aDSBControl.TextBlockLen1.Text = "№ planes w/o coordinates: ";
                aDSBControl.TextBlockLen2.Text = "№ planes with coordinates: ";
                aDSBControl.ColLen1.Header = "Fl";
                aDSBControl.ColLen2.Header = "Type";
                aDSBControl.ColLen3.Header = "Alt,°         Long,°";
                aDSBControl.ColLen5.Header = " Alt, м";
                aDSBControl.TTPrint.Content = "Print file";
                aDSBControl.TTSave.Content = "Save file as Word/Doc";
                aDSBControl.TTDel.Content = "Delete all";
            }
            // .......................................................................................
            // Вся программа

            if (Convert.ToString(e) == "Rus")
            {
                GlobalVarMapMain.FlagLanguage = true;
            }
            else
            {
                GlobalVarMapMain.FlagLanguage = false;
            }

            ChoiceLanguage();
            // .......................................................................................
            // Записать в файл LanguageSettings.yaml

            // Запись
            //globalVarLanguageYaml.lang = "Rus";
            //YamlSave<GlobalVarLanguageYaml>(globalVarLanguageYaml, "LanguageSettings.yaml");


            if (Convert.ToString(e) == "Rus")
            {
                globalVarLanguageYaml.lang = "Rus";
                YamlSave<GlobalVarLanguageYaml>(globalVarLanguageYaml, "LanguageSettings.yaml");
            }
            else
            {
                globalVarLanguageYaml.lang = "Eng";
                YamlSave<GlobalVarLanguageYaml>(globalVarLanguageYaml, "LanguageSettings.yaml");
            }
            // .......................................................................................


        }
        // **************************************************************************** Change_Language

        // DoubleClickADSB ****************************************************************************
        // Обработчик двойного клика мыши на строке таблицы ADSB


        private void ADSBControl_OnSelectedRowADSB(object sender, string e)
                {
                    // НЕ пустая строка !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    if (e != "")
                    {
                        // ----------------------------------------------------------------------------------------
                        // Выделение оранжевым либо сброс обратно в серый

                        int index = mapCtrl.objClassInterfaceMap.List_AP.FindIndex(x => (x.ICAO == e));

                        if (index >= 0)
                        {
                    // """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
                    // Клик на НЕвыделенной строке

                    if (mapCtrl.objClassInterfaceMap.List_AP[index].flColor == 0)
                    {
                        //.................................................................................
                        if (
                             (GlobalVarMapMain.flairone == false) // Нет выделенных строк
                           )
                        {
                            mapCtrl.objClassInterfaceMap.List_AP[index].flColor = 1;
                            //&&&
                            GlobalVarMapMain.flairone = true;
                            GlobalVarMapMain.ICAO_LAST = mapCtrl.objClassInterfaceMap.List_AP[index].ICAO;
                        }
                        //.................................................................................
                        // Уже была выделенная строка

                        else
                        {
                            // Старую снимаем
                            int index2 = mapCtrl.objClassInterfaceMap.List_AP.FindIndex(x => (x.flColor == 1));
                            if(index2>=0)
                            {
                                mapCtrl.objClassInterfaceMap.List_AP[index2].flColor = 0;
                                if (mapCtrl.objClassInterfaceMap.List_AP[index2].ICAO == GlobalVarMapMain.ICAO_LAST)
                                {
                                    GlobalVarMapMain.flairone = false;
                                    GlobalVarMapMain.ICAO_LAST = "";
                                }
                            } // index2>=0

                            // Новую устанавливаем
                            mapCtrl.objClassInterfaceMap.List_AP[index].flColor = 1;
                            GlobalVarMapMain.flairone = true;
                            GlobalVarMapMain.ICAO_LAST = mapCtrl.objClassInterfaceMap.List_AP[index].ICAO;

                        } // Уже была выделенная строка
                        //.................................................................................

                    } // Клик на НЕвыделенной строке
                    // """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
                    // Повторный клик для сброса в серый

                    else
                    {
                        mapCtrl.objClassInterfaceMap.List_AP[index].flColor = 0;
                        //&&&
                        if (mapCtrl.objClassInterfaceMap.List_AP[index].ICAO == GlobalVarMapMain.ICAO_LAST)
                        {
                            GlobalVarMapMain.flairone = false;
                            GlobalVarMapMain.ICAO_LAST = "";
                        }
                    }
                          // """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

                    // ----------------------------------------------------------------------------------------
                    // Центровка

                    if (mapCtrl.objClassInterfaceMap.List_AP[index].flColor == 1)
                            {
                                // ....................................................................................
                                bool bvar = false;
                                double x = 0;
                                double y = 0;
                                double lat = 0;
                                double lon = 0;
                                // ....................................................................................
                                lat = mapCtrl.objClassInterfaceMap.List_AP[index].Coordinates.Latitude;
                                lon = mapCtrl.objClassInterfaceMap.List_AP[index].Coordinates.Longitude;
                                // ....................................................................................
                                var p = Mercator.FromLonLat(lon, lat);
                                x = p.X;
                                y = p.Y;
                                // ....................................................................................
                                // MercatorMap or GeographicMap?

                                string s = "";
                                InitMapYaml initMapYaml = new InitMapYaml();
                                s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
                                // ....................................................................................
                                // MercatorMap

                                if (initMapYaml.TypeMap == 0)
                                {
                                    // Center map on pozition XY (Mercator)
                                    bvar = mapCtrl.objClassInterfaceMap.CenterMapToXY(x, y);
                                } // MercatorMap
                                  // ....................................................................................
                                  // GeographicMap

                                else
                                {
                                    // Center map on pozition LatLong (degree)
                                    bvar = mapCtrl.objClassInterfaceMap.CenterMapToLatLong(lat, lon);
                                } // GeographicMap
                                  // ....................................................................................

                            } // Выделили самолет оранжевым
                              // ----------------------------------------------------------------------------------------
                              // Clear and redraw

                            Dispatcher.Invoke(() =>
                            {
                                mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                            });
                            // ----------------------------------------------------------------------------------------

                        } // index>=0

                    } // НЕ пустая строка
                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! НЕ пустая строка

                    // ПУСТАЯ строка !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                    else
                    {
                        int index1 = mapCtrl.objClassInterfaceMap.List_AP.FindIndex(x => (x.flColor == 1));

                        if(index1>=0)
                        {
                            mapCtrl.objClassInterfaceMap.List_AP[index1].flColor = 0;
                            //&&&
                            if (mapCtrl.objClassInterfaceMap.List_AP[index1].ICAO == GlobalVarMapMain.ICAO_LAST)
                            {
                                GlobalVarMapMain.flairone = false;
                                GlobalVarMapMain.ICAO_LAST = "";
                            }

                            // Clear and redraw
                            Dispatcher.Invoke(() =>
                            {
                                mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                            });

                        } // index1>=0

                    } // ПУСТАЯ строка
                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ПУСТАЯ строка


                }
        
        // **************************************************************************** DoubleClickADSB

        // PropertyGridSettings ***********************************************************************
        /// <summary>
        /// New settings from control SettingsMap
        /// </summary>
        public void UpdateSettings(object sender, ControlSettingsMap.ClassSettings settings)
        {
            try
            {
                // ------------------------------------------------------------------------------------
                // Write to YAML

                string s = "";

                InitMapYaml initMapYaml = new InitMapYaml();
                s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
                // ...........................................................................
                // File .yaml was read

                if (s == "")
                {
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // If Map type was changed, close map and open new map
                    // 999

                    if(initMapYaml.TypeMap!=((byte)SettingsMap.Settings.TypeProjection))
                    {
                        mapCtrl.CloseMap();
                        mapCtrl.CloseMatrix();
                        mapCtrl.OpenMap(SettingsMap.Settings.Path + "\\1_1-1.tif");
                        //mapCtrl.OpenMatrix(SettingsMap.Settings.Path + "\\");
                    }
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    initMapYaml.IP_DB = SettingsMap.Settings.IP_DB;
                    initMapYaml.NumPort_DB = SettingsMap.Settings.NumPort;
                    initMapYaml.SysCoord = (byte)SettingsMap.Settings.SysCoord;
                    initMapYaml.FormatCoord = (byte)SettingsMap.Settings.FormCoord;
                    //initMapYaml.Path = SettingsMap.Settings.Path+"\\1_1-1.tif";
                    initMapYaml.Path = SettingsMap.Settings.Path;
                    initMapYaml.PathMatr = SettingsMap.Settings.PathMatr;
                    initMapYaml.TypeMap = (byte)SettingsMap.Settings.TypeProjection;


                    // ..............................................................................
                    // Azimuth* SysCoord

                    AzimutTask.Enums.EnumSysCoord enumSysCoord = new AzimutTask.Enums.EnumSysCoord();
                    if ((initMapYaml.SysCoord == 2) || (initMapYaml.SysCoord == 3))
                        enumSysCoord = AzimutTask.Enums.EnumSysCoord.XY;
                    else
                    {
                        if (initMapYaml.FormatCoord == 0)
                        {
                            enumSysCoord = AzimutTask.Enums.EnumSysCoord.Degree;
                        }
                        else
                        {
                            enumSysCoord = AzimutTask.Enums.EnumSysCoord.DdMmSs;
                        }

                    } // degree
                    AzimutTaskCntr.ChangeSystemCoord(enumSysCoord);

                    // ..............................................................................

                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // 19.07 GNSS
                    // Change SK in GNSS

                    long ichislo1 = 0;
                    double dchislo1 = 0;
                    long ichislo2 = 0;
                    double dchislo2 = 0;
                    double xxx = 0;
                    double yyy = 0;

                    if ((GlobalVarMapMain.GNSS_Lat != -1) && (GlobalVarMapMain.GNSS_Long != -1))
                    {
                        // WGS 84
                        if (initMapYaml.SysCoord == 0)
                        {
                            // Latitude
                            ichislo1 = (long)(GlobalVarMapMain.GNSS_Lat * 1000000);
                            dchislo1 = ((double)ichislo1) / 1000000;
                            // Longitude
                            ichislo2 = (long)(GlobalVarMapMain.GNSS_Long * 1000000);
                            dchislo2 = ((double)ichislo2) / 1000000;

                            DispatchIfNecessary(() =>
                            {
                                mapCtrl.Txt9.Text = "GNSS:   N " + Convert.ToString(dchislo1) + '\xB0' + "   " +
                                                         "W " + Convert.ToString(dchislo2) + '\xB0' +
                                                 "   " + Convert.ToString(GlobalVarMapMain.GNSS_Numb_Sat) + "   " + GlobalVarMapMain.GNSS_time;
                            });

                        } // WGS84

                        // SK42,m
                        else if (initMapYaml.SysCoord == 2)
                        {
                            ClassGeoCalculator.f_WGS84_Krug(GlobalVarMapMain.GNSS_Lat,
                                                            GlobalVarMapMain.GNSS_Long, 
                                                            ref xxx, ref yyy);
                            DispatchIfNecessary(() =>
                            {
                                mapCtrl.Txt9.Text = "GNSS:   X " + Convert.ToString((int)xxx) + " m" + "   " + "Y " + Convert.ToString((int)yyy) + " m" +
                                                 "   " + Convert.ToString(GlobalVarMapMain.GNSS_Numb_Sat) + "   " + GlobalVarMapMain.GNSS_time;
                            });

                        } // SK42,m

                        // SK42grad
                        else if (initMapYaml.SysCoord == 1)
                        {
                            double xxx1 = 0;
                            double yyy1 = 0;

                            ClassGeoCalculator.f_WGS84_SK42_BL(GlobalVarMapMain.GNSS_Lat,
                                                               GlobalVarMapMain.GNSS_Long,
                                                               ref xxx1, ref yyy1);

                            // Latitude
                            ichislo1 = (long)(xxx1 * 1000000);
                            dchislo1 = ((double)ichislo1) / 1000000;
                            // Longitude
                            ichislo2 = (long)(yyy1 * 1000000);
                            dchislo2 = ((double)ichislo2) / 1000000;

                            DispatchIfNecessary(() =>
                            {
                                mapCtrl.Txt9.Text = "GNSS:   N " + Convert.ToString(dchislo1) + '\xB0' + "   " +
                                                         "W " + Convert.ToString(dchislo2) + '\xB0' +
                                                 "   " + Convert.ToString(GlobalVarMapMain.GNSS_Numb_Sat) + "   " +
                                                         GlobalVarMapMain.GNSS_time;
                            });

                        } //SK42grad

                        // Mercator,m
                        else if (initMapYaml.SysCoord == 3)
                        {
                            double xxx2 = 0;
                            double yyy2 = 0;

                            ClassGeoCalculator.f_WGS84_Mercator(GlobalVarMapMain.GNSS_Lat,
                                                                GlobalVarMapMain.GNSS_Long,
                                                                ref xxx2, ref yyy2);

                            DispatchIfNecessary(() =>
                            {
                                mapCtrl.Txt9.Text = "GNSS:   X " + Convert.ToString((int)xxx2) + " m" + "   " + "Y " + Convert.ToString((int)yyy2) + " m" +
                                                 "   " + Convert.ToString(GlobalVarMapMain.GNSS_Numb_Sat) + "   " + GlobalVarMapMain.GNSS_time;
                            });

                        } // Mercator,m


                    } // Lat,Long!=-1
                      // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                      // Пишем класс обратно в файл

                    try
                    {
                        ClassYaml.WriteYaml(
                                            "Setting.yaml",
                                            initMapYaml
                                           );

                        // Change SystemCoordinates On the Map (Отображение внизу карты)
                        mapCtrl.DrawCurrentCoordinates();

                        // Azimuth*
                        // !!! Здесь нужно занести координаты источника в окошки Таниного контрола
                        objClassFunctionsMain.DrawCurrentCoordinatesAzimuth();

                    }
                    catch
                    {
                        if (GlobalVarMapMain.FlagLanguage == false)
                            MessageBox.Show("Can't write .yaml file");
                        else
                            MessageBox.Show("Невозможно записать .yaml файл");

                    }

                } // IF( Файл .yaml was read)
                  // ...........................................................................
                  // File .yaml wasn't read

                else
                {
                    if (GlobalVarMapMain.FlagLanguage == false)
                        MessageBox.Show("Can't read .yaml file");
                    else
                        MessageBox.Show("Невозможно прочитать .yaml файл");

                } // ELSE (// File .yaml wasn't read)
                  // ...........................................................................

                // ------------------------------------------------------------------------------------

            } // try get data from PropertyGrid

            catch
            {
                if (GlobalVarMapMain.FlagLanguage == false)
                    MessageBox.Show("Can't get data");
                else
                    MessageBox.Show("Невозможно получить данные");

            }


        }
        // *********************************************************************** PropertyGridSettings

        // Mouse Click On the map *********************************************************************
        // 666
        private void MapCtrl_OnMouseCl(object sender, ClassArg e)
        {

            // All_Part *******************************************************************************
            long ichislo = 0;
            double dchislo = 0;

            //  WGS84
            GlobalVarMapMain.LAT_Rastr = e.lat;
            if (GlobalVarMapMain.LAT_Rastr < -90)
                GlobalVarMapMain.LAT_Rastr = -90;
            if (GlobalVarMapMain.LAT_Rastr > 90)
                GlobalVarMapMain.LAT_Rastr = 90;

            GlobalVarMapMain.LONG_Rastr = e.lon;
            if (GlobalVarMapMain.LONG_Rastr < -180)
                GlobalVarMapMain.LONG_Rastr = -180;
            if (GlobalVarMapMain.LONG_Rastr > 180)
                GlobalVarMapMain.LONG_Rastr = 180;

            GlobalVarMapMain.H_Rastr = e.h;

            // Mercator
            GlobalVarMapMain.X_Rastr = e.x;
            GlobalVarMapMain.Y_Rastr = e.y;
            // MGRS
            GlobalVarMapMain.Str_Rastr = e.str;

            // Get current dir
            string dir = "";
            dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            string sall = "";
            // ******************************************************************************* All_Part

            // Triang *********************************************************************************

            if (GlobalVarMapMain.flTriang == true)
            {

                // ..................................................................................
                // Pel1

                if ((GlobalVarMapMain.flPeleng1==false)&&(GlobalVarMapMain.flPeleng2 == false))
                {

                    GlobalVarMapMain.flPeleng1 = true;

                    // Latitude
                    ichislo = (long)(e.lat * 1000000);
                    dchislo = ((double)ichislo) / 1000000;

                    DispatchIfNecessary(() =>
                    {
                        objWindowTriang.tasksTriang.textBox1.Text = "N " + Convert.ToString(dchislo) + '\xB0';
                    });
                    GlobalVarMapMain.LatPel1 = dchislo;

                    // Longitude
                    ichislo = (long)(e.lon * 1000000);
                    dchislo = ((double)ichislo) / 1000000;
                    DispatchIfNecessary(() =>
                    {
                        objWindowTriang.tasksTriang.textBox2.Text = "W " + Convert.ToString(dchislo) + '\xB0';
                    });
                    GlobalVarMapMain.LonPel1 = dchislo;

                    sall = mapCtrl.DrawImage(
                                                 GlobalVarMapMain.LAT_Rastr,
                                                 GlobalVarMapMain.LONG_Rastr,
                                                 // Path to image
                                                 dir + "\\Images\\OtherObject\\" + "SquareRed.png",
                                                 // Text
                                                 "",
                                                 // Scale
                                                 0.6
                                      );


                } // (GlobalVarMapMain.flPeleng1==false)&&(GlobalVarMapMain.flPeleng1 == false)
                  // ..................................................................................
                 // Pel2

                else if ((GlobalVarMapMain.flPeleng1 == true) && (GlobalVarMapMain.flPeleng2 == false))
                {
                    GlobalVarMapMain.flPeleng2 = true;

                    // Latitude
                    ichislo = (long)(e.lat * 1000000);
                    dchislo = ((double)ichislo) / 1000000;
                    DispatchIfNecessary(() =>
                    {
                        objWindowTriang.tasksTriang.textBox3.Text = "N " + Convert.ToString(dchislo) + '\xB0';
                    });
                    GlobalVarMapMain.LatPel2 = dchislo;

                    // Longitude
                    ichislo = (long)(e.lon * 1000000);
                    dchislo = ((double)ichislo) / 1000000;
                    DispatchIfNecessary(() =>
                    {
                        objWindowTriang.tasksTriang.textBox4.Text = "W " + Convert.ToString(dchislo) + '\xB0';
                    });
                    GlobalVarMapMain.LonPel2 = dchislo;

                    sall = mapCtrl.DrawImage(
                                                 GlobalVarMapMain.LAT_Rastr,
                                                 GlobalVarMapMain.LONG_Rastr,
                                                 // Path to image
                                                 dir + "\\Images\\OtherObject\\" + "SquareRed.png",
                                                 // Text
                                                 "",
                                                 // Scale
                                                 0.6
                                      );



                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // Otl
                    // !!! Проверка функции расстояния между точками с учетом кривизны Земли
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                } // (GlobalVarMapMain.flPeleng1==true)&&(GlobalVarMapMain.flPeleng1 == false)
                  // ..................................................................................

                else
                {
                    if (GlobalVarMapMain.FlagLanguage == false)
                        MessageBox.Show("Coordinates are entered");
                    else
                        MessageBox.Show("Координаты введены");

                    return;
                }
                // ..................................................................................


            } // GlobalVarMapMain.flTriang == true
              // ******************************************************************************* Triang

            // Azimuth ********************************************************************************
            // Azimuth*

            if ((mapCtrl.objClassInterfaceMap.flAzimuth == true) && (mapCtrl.objClassInterfaceMap.ShowJS == true))
            {
                // Source coordinates
                mapCtrl.objClassInterfaceMap.LatSource_Azimuth = GlobalVarMapMain.LAT_Rastr;
                mapCtrl.objClassInterfaceMap.LongSource_Azimuth = GlobalVarMapMain.LONG_Rastr;
                mapCtrl.objClassInterfaceMap.HSource_Azimuth = GlobalVarMapMain.H_Rastr;

                // !!! Здесь нужно занести координаты источника в окошки Таниного контрола
                objClassFunctionsMain.DrawCurrentCoordinatesAzimuth();

                mapCtrl.objClassInterfaceMap.CheckShowAzimuth = AzimutTaskCntr.IsShowValue;

                // !!! Расчет идет в перерисовке азимута
                mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();

            }
            // ------------------------------------------------------------------------------------------


            /*

                        if (GlobalVarMapMain.flAzimuth == true)
                        {
                        // ----------------------------------------------------------------------------------------
                         objClassFunctionsMain.ClearAzimuth();
                        // ----------------------------------------------------------------------------------------
                        // Latitude
                        ichislo = (long)(e.lat * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        objWindowAzimuth.tasksAzimuth.Txt1.Text = "N " + Convert.ToString(dchislo) + '\xB0';

                        // Longitude
                        ichislo = (long)(e.lon * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        objWindowAzimuth.tasksAzimuth.Txt3.Text = "W " + Convert.ToString(dchislo) + '\xB0';

                        // Altitude
                        ichislo = (long)(e.h * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        objWindowAzimuth.tasksAzimuth.Txt5.Text = Convert.ToString(dchislo) + " m";

                        objWindowAzimuth.tasksAzimuth.Txt7.Text = string.Format("    MGRS: {0}", e.str);
                        // ----------------------------------------------------------------------------------------

                         // ----------------------------------------------------------------------------------------

                            double az = 0;
                            string s3 = "";

                            for (int i = 0; i < mapCtrl.objClassInterfaceMap.List_JS.Count; i++)
                            {

                                az = mapCtrl.CalcAzimuth(mapCtrl.objClassInterfaceMap.List_JS[i].Coordinates.Latitude, mapCtrl.objClassInterfaceMap.List_JS[i].Coordinates.Longitude,
                                                         GlobalVarMapMain.LAT_Rastr, GlobalVarMapMain.LONG_Rastr);

                                ichislo = (long)(az * 1000);
                                dchislo = ((double)ichislo) / 1000;
                                az = dchislo;
                                GlobalVarMapMain.listAzimuth.Add(az);


                            } // for

                            mapCtrl.objClassMapRastrReDraw.ReDrawAzimuth(
                                                                         GlobalVarMapMain.flAzimuth,
                                                                         GlobalVarMapMain.CheckShowAzimuth,
                                                                         GlobalVarMapMain.LAT_Rastr,
                                                                         GlobalVarMapMain.LONG_Rastr,
                                                                         GlobalVarMapMain.listAzimuth
                                                                         );

                            // ----------------------------------------------------------------------------------------

                        } // GlobalVarMapMain.flAzimuth == true
            */
            // ******************************************************************************** Azimuth

        }
        // ********************************************************************* Mouse Click On the map

        // Click Button of Azimuth task (WpfTasksControl) *******************************************************
        private void ButtonAz(object sender, EventArgs e)
        {
/*
            // -------------------------------------------------------------------------------------
            if (objWindowAzimuth == null)
            {
                objWindowAzimuth = new WindowAzimuth();
                objWindowAzimuth.Owner = this;
                GlobalVarMapMain.objWindowAzimuthG = objWindowAzimuth;
                objWindowAzimuth.Show();

                //ClassMapRastrClear.f_Clear_FormAz1();
            }
            else
            {
                objWindowAzimuth.Show();
            }

            GlobalVarMapMain.flAzimuth = true;
            // -------------------------------------------------------------------------------------

            // Otl ---------------------------------------------------------------------------------
            // --------------------------------------------------------------------------------- Otl
*/
        }
        // ******************************************************* Click Button of Azimuth task (WpfTasksControl)

        // Show azimuth title near station **********************************************************************

        private void CheckShowAzimuth(object sender, EventArgs e)
        {
/*
            GlobalVarMapMain.CheckShowAzimuth = true;

            mapCtrl.objClassMapRastrReDraw.ReDrawAzimuth(
                                                         GlobalVarMapMain.flAzimuth,
                                                         GlobalVarMapMain.CheckShowAzimuth,
                                                         GlobalVarMapMain.LAT_Rastr,
                                                         GlobalVarMapMain.LONG_Rastr,
                                                         GlobalVarMapMain.listAzimuth
                                                         );

        }
        private void UnCheckShowAzimuth(object sender, EventArgs e)
        {
            GlobalVarMapMain.CheckShowAzimuth = false;

            mapCtrl.objClassMapRastrReDraw.ReDrawAzimuth(
                                                         GlobalVarMapMain.flAzimuth,
                                                         GlobalVarMapMain.CheckShowAzimuth,
                                                         GlobalVarMapMain.LAT_Rastr,
                                                         GlobalVarMapMain.LONG_Rastr,
                                                         GlobalVarMapMain.listAzimuth
                                                         );
*/
        }

        // ********************************************************************** Show azimuth title near station


        // Click Button "Clear" in Azimuth Control **************************************************************

        public void ButtonClearAzimuth(object sender, EventArgs e)

        {
            objClassFunctionsMain.ClearAzimuth();
        }
        // ************************************************************** Click Button "Clear" in Azimuth Control

        // Click Button of Triangulation task (WpfTasksControl) *************************************************
        private void ButtonTriang(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------------------
            if (objWindowTriang == null)
            {
                objWindowTriang = new WindowTriang();
                objWindowTriang.Owner = this;
                GlobalVarMapMain.objWindowTriangG = objWindowTriang;
                objWindowTriang.Show();
            }
            else
            {
                objWindowTriang.Show();
            }
            // -------------------------------------------------------------------------------------------------
            GlobalVarMapMain.flTriang = true;
            // -------------------------------------------------------------------------------------------------

        }
        // ************************************************* Click Button of Triangulation task (WpfTasksControl)

        // Click Button "Calc" in Triangulation Control *********************************************************

        private void ButtonCalcTriang(object sender, EventArgs e)
        {
            objWindowTriang.Owner = this;
            objWindowTriang.Show();
            GlobalVarMapMain.flTriang = true;

            // Otl ---------------------------------------------------------------------------------------------
            // --------------------------------------------------------------------------------------------- Otl

            // Otl1 ---------------------------------------------------------------------------------------------
            double a1 = 0;
            double b1 = 0;
            double c1 = 0;
            double d1 = 0;
            double f1 = 0;
            double g1 = 0;
            double a2 = 0;
            double b2 = 0;
            double c2 = 0;
            double d2 = 0;
            double f2 = 0;
            double g2 = 0;

            DispatchIfNecessary(() =>
            {
                GlobalVarMapMain.Pel1Grad = Convert.ToDouble(objWindowTriang.tasksTriang.textBox5.Text);
                GlobalVarMapMain.Pel2Grad = Convert.ToDouble(objWindowTriang.tasksTriang.textBox6.Text);
            });

            double[] arr_Pel1 = new double[GlobalVarMapMain.numberofdots*2+1];     // R,Широта,долгота
            double[] arr_Pel2 = new double[GlobalVarMapMain.numberofdots * 2+1];     // R,Широта,долгота
            double[] arr_Pel_XYZ1 = new double[GlobalVarMapMain.numberofdots * 3+1];
            double[] arr_Pel_XYZ2 = new double[GlobalVarMapMain.numberofdots * 3+1];


            ClassBearing.f_Bearing(
                                   GlobalVarMapMain.Pel1Grad,
                                   GlobalVarMapMain.distance,
                                   GlobalVarMapMain.numberofdots,
                                   GlobalVarMapMain.LatPel1,
                                   GlobalVarMapMain.LonPel1,
                                   1,  // WGS84
                                   ref a1, ref b1, ref c1,
                                   ref d1, ref f1, ref g1,
                                   ref arr_Pel1,
                                   ref arr_Pel_XYZ1
                                   );

            ClassBearing.f_Bearing(
                                   GlobalVarMapMain.Pel2Grad,
                                   GlobalVarMapMain.distance,
                                   GlobalVarMapMain.numberofdots,
                                   GlobalVarMapMain.LatPel2,
                                   GlobalVarMapMain.LonPel2,
                                   1,  // WGS84
                                   ref a2, ref b2, ref c2,
                                   ref d2, ref f2, ref g2,
                                   ref arr_Pel2,
                                   ref arr_Pel_XYZ2
                                   );
            // .................................................................................................
            // Peleng1

            List<Mapsui.Geometries.Point> pointPel1 = new List<Mapsui.Geometries.Point>();
            int i1 = 0;

            pointPel1.Add(new Mapsui.Geometries.Point(GlobalVarMapMain.LonPel1, GlobalVarMapMain.LatPel1));

            for (i1=0;i1< GlobalVarMapMain.numberofdots * 2;i1+=2)
            {
                pointPel1.Add(new Mapsui.Geometries.Point(arr_Pel1[i1+1], arr_Pel1[i1]));

            }

            string s1 = mapCtrl.DrawLinesLatLong(
                                                // Degree
                                                pointPel1,
                                                // Color
                                                100,
                                                255,
                                                0,
                                                0
                                               );

            // .................................................................................................
            // Peleng2
            List<Mapsui.Geometries.Point> pointPel2 = new List<Mapsui.Geometries.Point>();
            int i2 = 0;

            pointPel2.Add(new Mapsui.Geometries.Point(GlobalVarMapMain.LonPel2, GlobalVarMapMain.LatPel2));

            for (i2 = 0; i2 < GlobalVarMapMain.numberofdots * 2; i2 += 2)
            {
                pointPel2.Add(new Mapsui.Geometries.Point(arr_Pel2[i2 + 1], arr_Pel2[i2 ]));

            }

            string s2 = mapCtrl.DrawLinesLatLong(
                                                // Degree
                                                pointPel2,
                                                // Color
                                                100,
                                                255,
                                                0,
                                                0
                                               );
            // .................................................................................................
            // Triangulation1
            // .................................................................................................
            // Triangulation2

            Coord JamStObj = new Coord();

            List<JamBearing> JamSt = new List<JamBearing>();

            JamBearing objJamBearing = new JamBearing();
            objJamBearing.Coordinate = new Coord();
            objJamBearing.Bearing = (float)GlobalVarMapMain.Pel1Grad;
            objJamBearing.Coordinate.Latitude = GlobalVarMapMain.LatPel1;
            objJamBearing.Coordinate.Longitude = GlobalVarMapMain.LonPel1;
            JamSt.Add(objJamBearing);

            JamBearing objJamBearing1 = new JamBearing();
            objJamBearing1.Coordinate = new Coord();
            objJamBearing1.Bearing = (float)GlobalVarMapMain.Pel2Grad;
            objJamBearing1.Coordinate.Latitude = GlobalVarMapMain.LatPel2;
            objJamBearing1.Coordinate.Longitude = GlobalVarMapMain.LonPel2;
            JamSt.Add(objJamBearing1);

            JamStObj=ClassBearing.DefineCoordTriang(JamSt);

            // Get current dir
            string dir1 = "";
            dir1 = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            if ((JamStObj.Latitude!=-1) &&(JamStObj.Longitude!=-1))
            {
                string s5 = mapCtrl.DrawImage(
                                             JamStObj.Latitude,
                                             JamStObj.Longitude,
                                             // Path to image
                                             dir1 + "\\Images\\OtherObject\\" + "tr10.png",
                                             // Text
                                             "",
                                             // Scale
                                             0.6
                                  );
            }
            // .................................................................................................

            // --------------------------------------------------------------------------------------------- Otl1

        }
     // ********************************************************* Click Button "Calc" in Triangulation Control

     // Loaded ************************************************************************************************   
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
         {
            //MessageBox.Show("Load");
            objWindowAzimuth.Owner = this;
            objWindowTriang.Owner = this;


            CultureInfo current = System.Threading.Thread.CurrentThread.CurrentUICulture;
            if (current.TwoLetterISOLanguageName != "fr")
            {
                CultureInfo newCulture = CultureInfo.CreateSpecificCulture("en-US");
                System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture;
                // Make current UI culture consistent with current culture.
                System.Threading.Thread.CurrentThread.CurrentCulture = newCulture;
            }

        }
        // ************************************************************************************************ Loaded

        // Closing ************************************************************************************************
        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
     {
      // objWindowAzimuth.Close();
      // objWindowTriang.Close();

      // Out from application
      Application.Current.Shutdown();
     }
     // ************************************************************************************************ Closing

     // Settings -> Property Grid ****************************************************************************
     // The OLD -> Ybrat

        private void ButtonSettings_Click(object sender, RoutedEventArgs e)
        {
            // --------------------------------------------------------------------------------------------------
            // True
            if (GlobalVarMapMain.ButtonSettingsOn==true)
            {
                Image myImage1 = new Image();
                BitmapImage bi1 = new BitmapImage();
                bi1.BeginInit();
                bi1.UriSource = new Uri("Resources/RedKv.PNG", UriKind.Relative);
                bi1.EndInit();

                GlobalVarMapMain.ButtonSettingsOn = false;
                //ImSettings.Source = bi1;

                //PropGridCtrl.Visibility = Visibility.Collapsed;

                Col0.Width = new GridLength(1, GridUnitType.Pixel);

            } // True
            // --------------------------------------------------------------------------------------------------
            // False

            else
            {
                Image myImage3 = new Image();
                BitmapImage bi3 = new BitmapImage();
                bi3.BeginInit();
                bi3.UriSource = new Uri("Resources/file_document_box_outline.PNG", UriKind.Relative);
                bi3.EndInit();

                GlobalVarMapMain.ButtonSettingsOn = true;
                //ImSettings.Source = bi3;

                Col0.Width = new GridLength(451, GridUnitType.Pixel);


            } // False
            // --------------------------------------------------------------------------------------------------

        }
        // *************************************************************************** Settings -> Property Grid




    } // Class
} // Namespace
