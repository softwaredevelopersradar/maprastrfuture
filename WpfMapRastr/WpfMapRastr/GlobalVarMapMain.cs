﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using ModelsTablesDBLib;
using ClientDataBase;

namespace WpfMapRastr
{
    class GlobalVarMapMain
    {

        // Form ********************************************************************************
        // Объекты форм для использования в других формах (Инициализируются в Main)

        public static MainWindow objMainWindowG;
        public static WindowAzimuth objWindowAzimuthG;
        public static WindowTriang objWindowTriangG;

        // ******************************************************************************** Form

        // MAP *********************************************************************************
        // !!! Сюда заносятся координаты клика мыши на карте (или при движении мыши)

        // Mercator
        public static double X_Rastr = 0;
        public static double Y_Rastr = 0;
        public static string Str_Rastr = "";

        public static double LAT_Rastr = 0;
        public static double LONG_Rastr = 0;
        public static double H_Rastr = 0;

        // Для оценки изменения местоположения
        public static double deltaS = 250; // m
        // ********************************************************************************* MAP

        // From YAML/PropertyGrid **************************************************************

        //public static string IP_DB = "";
        //public static int NumbPort_DB = 0;
        // 0 - WGS84degree
        // 1 - SK42degree
        // 2 - SK42_XY
        // 3 - Mercator
        //public static byte SysCoord = 0;
        // 0 - DD.dddddd
        // 1 - DD MM.mmmmmm
        // 2-  DD MM SS.ssssss
        //public static byte FormatCoord = 0;
        // ************************************************************** From YAML/PropertyGrid

        // JS **********************************************************************************

        //public static List<TableASP> listJS = new List<TableASP>();

        // ********************************************************************************** JS

        // Azimuth ******************************************************************************
        //public static List<double> listAzimuth = new List<double>();
        //public static bool flAzimuth = false;
        //public static bool CheckShowAzimuth = false;
        // ****************************************************************************** Azimuth

        // Triang ******************************************************************************
        public static List<double> listPeleng1 = new List<double>();
        public static List<double> listPeleng2 = new List<double>();

        public static double LatPel1 = 0;
        public static double LonPel1 = 0;
        public static double LatPel2 = 0;
        public static double LonPel2 = 0;
        public static double Pel1Grad = 0;
        public static double Pel2Grad = 0;

        public static bool flTriang = false;
        public static bool flPeleng1 = false;
        public static bool flPeleng2 = false;

        public static double distance = 100000; // m distance of calculation
        public static uint numberofdots = 1000;  // number of points
        public static double distance_pel = 100000; // m bearing visibility

        public static double LatTriang = 0;
        public static double LonTriang = 0;

        // ****************************************************************************** Triang

        // AirPlanes ***************************************************************************

        // new1
        //public static List<TempADSB> listTableADSB = new List<TempADSB>();

        public static int fl_AirPlane = 0;
        public static int numbtr = 50;

        public static double Smax_traj = 50000; // 50km
        //public static double Smax_traj = 10000; // 50km

        public static int fltmr_air = 0;
        public static System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        public static double ltmin_air = 50.757331;
        public static double ltmax_air = 56.5699;
        public static double lnmin_air = 22.497829;
        public static double lnmax_air = 33.71487;

        public static bool fltbl = false;

        //&&&
        public static bool flairone = false;
        public static string ICAO_LAST = "";

        public static List<string> sICAO = new List<string>();

        // *************************************************************************** AirPlanes

        // Flags ********************************************************************************
        public static bool ButtonSettingsOn = true;

        // Флаг языка: по умолчанию английский (false)
        // true  -> русский
        public static bool FlagLanguage = false;
        // Rus/Eng
        public static string LNG = "";
        //public static List<string> sICAO = new List<string>();
        //GlobalVarLanguageYaml globalVarLanguageYaml = new GlobalVarLanguageYaml();

        // ******************************************************************************** Flags

        //  GNSS ********************************************************************************
        public static double GNSS_Lat = -1;
        public static double GNSS_Long = -1;
        public static string GNSS_time = "";
        public static int GNSS_Numb_Sat = 0;
        //  ******************************************************************************** GNSS

        // Otl **********************************************************************************

        // Belarus
        public static int lat1_temp1 = 5386500;
        public static int lat2_temp1 = 5397332;
        public static int lon1_temp1 = 2747061;
        public static int lon2_temp1 = 2768947;


/*
        // Emirat
        public static int lat1_temp1 = 2163350;
        public static int lat2_temp1 = 2337590;
        public static int lon1_temp1 = 5162403;
        public static int lon2_temp1 = 5707373;
*/
        public static int sh1=0;
        // ********************************************************************************** Otl


    } // Class
} // NameSpace
