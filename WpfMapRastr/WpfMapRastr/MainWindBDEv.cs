﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ClientDataBase;

using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

using System.Windows.Threading;
using System.Threading.Tasks;

// Otl
// Для YAML реализации
using YamlDotNet.Serialization;

using WpfMapRastrControl;
using WpfAzimuthControl;
using ModelsTablesDBLib;
using Bearing;
using GeoCalculator;
using InheritorsEventArgs;
using ClassLibraryIniFiles;
using ControlSettingsMap;
using ADSBControl;
using AzimutTask;

using System.ComponentModel;
using System.Runtime.CompilerServices;

// Semen
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;

namespace WpfMapRastr
{
    public partial class MainWindow
    {
        // DATA_BASE *********************************************************************************************

        public void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }


        // GPS **************************************************************************************************
        // Updated table of GPS

        private void OnTempGNSSUp(object sender, TableEventArs<TempGNSS> e)
        {
            if (GlobalVarMapMain.fltbl == false)
                return;

            // Otl ----------------------------------------------------------
            // 19.07
            // ---------------------------------------------------------- Otl
            long ichislo1 = 0;
            double dchislo1 = 0;
            long ichislo2 = 0;
            double dchislo2 = 0;
            int i = 0;
            string s = "";

            // ---------------------------------------------------------------
            // SystemCoordinates?

            string s1 = "";
            InitMapYaml initMapYaml = new InitMapYaml();
            s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);

            // -------------------------------------------------------------------------------------------------
            // 19.07 GNSS

            // FOR
            for (i = 0; i < e.Table.Count; i++)
            {
                // .............................................................................................
                // Latitude
                if (e.Table[i].Location.Latitude < 0)
                    e.Table[i].Location.Latitude = -e.Table[i].Location.Latitude;
                ichislo1 = (long)(e.Table[i].Location.Latitude * 1000000);
                dchislo1 = ((double)ichislo1) / 1000000;
                // Longitude
                if (e.Table[i].Location.Longitude < 0)
                    e.Table[i].Location.Longitude = -e.Table[i].Location.Longitude;
                ichislo2 = (long)(e.Table[i].Location.Longitude * 1000000);
                dchislo2 = ((double)ichislo2) / 1000000;
                // .............................................................................................
                // Time

                s = e.Table[i].LocalTime.ToLongTimeString();
                // .............................................................................................

                // WGS 84
                if (initMapYaml.SysCoord == 0)
                {
                    if ((dchislo1 >= 0) && (dchislo2 >= 0))
                    {
                        DispatchIfNecessary(() =>
                        {
                            mapCtrl.Txt9.Text = "GNSS:   N " + Convert.ToString(dchislo1) + '\xB0' + "   " + "E " + Convert.ToString(dchislo2) + '\xB0' +
                                              "   " + Convert.ToString(e.Table[i].NumberOfSatellites) + "   " + s;
                        });
                    }
                    else if ((dchislo1 >= 0) && (dchislo2 < 0))
                    {
                        DispatchIfNecessary(() =>
                        {
                            mapCtrl.Txt9.Text = "GNSS:   N " + Convert.ToString(dchislo1) + '\xB0' + "   " + "W " + Convert.ToString(dchislo2) + '\xB0' +
                                            "   " + Convert.ToString(e.Table[i].NumberOfSatellites) + "   " + s;
                        });
                    }
                    else if ((dchislo1 < 0) && (dchislo2 >= 0))
                    {
                        DispatchIfNecessary(() =>
                        {
                            mapCtrl.Txt9.Text = "GNSS:   S " + Convert.ToString(dchislo1) + '\xB0' + "   " + "E " + Convert.ToString(dchislo2) + '\xB0' +
                                           "   " + Convert.ToString(e.Table[i].NumberOfSatellites) + "   " + s;
                        });
                    }
                    else
                    {
                        DispatchIfNecessary(() =>
                        {
                            mapCtrl.Txt9.Text = "GNSS:   S " + Convert.ToString(dchislo1) + '\xB0' + "   " + "W " + Convert.ToString(dchislo2) + '\xB0' +
                                           "   " + Convert.ToString(e.Table[i].NumberOfSatellites) + "   " + s;
                        });
                    }

                } // WGS 84

                // SK42,m
                else if (initMapYaml.SysCoord == 2)
                {
                    double xxx = 0;
                    double yyy = 0;

                    ClassGeoCalculator.f_WGS84_Krug(e.Table[i].Location.Latitude, -e.Table[i].Location.Longitude, ref xxx, ref yyy);

                    DispatchIfNecessary(() =>
                    {
                        mapCtrl.Txt9.Text = "GNSS:   X " + Convert.ToString((int)xxx) + " m" + "   " + "Y " + Convert.ToString((int)yyy) + " m" +
                                        "   " + Convert.ToString(e.Table[i].NumberOfSatellites) + "   " + s;
                    });

                } // SK42,m

                // SK42grad
                else if (initMapYaml.SysCoord == 1)
                {
                    double xxx1 = 0;
                    double yyy1 = 0;

                    ClassGeoCalculator.f_WGS84_SK42_BL(e.Table[i].Location.Latitude, -e.Table[i].Location.Longitude, ref xxx1, ref yyy1);

                    // Latitude
                    ichislo1 = (long)(xxx1 * 1000000);
                    dchislo1 = ((double)ichislo1) / 1000000;
                    // Longitude
                    ichislo2 = (long)(yyy1 * 1000000);
                    dchislo2 = ((double)ichislo2) / 1000000;

                    if ((dchislo1 >= 0) && (dchislo2 >= 0))
                    {
                        DispatchIfNecessary(() =>
                        {
                            mapCtrl.Txt9.Text = "GNSS:   N " + Convert.ToString(dchislo1) + '\xB0' + "   " + "E " + Convert.ToString(dchislo2) + '\xB0' +
                                           "   " + Convert.ToString(e.Table[i].NumberOfSatellites) + "   " + s;
                        });

                    }
                    else if ((dchislo1 >= 0) && (dchislo2 < 0))
                    {
                        DispatchIfNecessary(() =>
                        {
                            mapCtrl.Txt9.Text = "GNSS:   N " + Convert.ToString(dchislo1) + '\xB0' + "   " + "W " + Convert.ToString(dchislo2) + '\xB0' +
                                            "   " + Convert.ToString(e.Table[i].NumberOfSatellites) + "   " + s;
                        });

                    }
                    else if ((dchislo1 < 0) && (dchislo2 >= 0))
                    {
                        DispatchIfNecessary(() =>
                        {
                            mapCtrl.Txt9.Text = "GNSS:   S " + Convert.ToString(dchislo1) + '\xB0' + "   " + "E " + Convert.ToString(dchislo2) + '\xB0' +
                                           "   " + Convert.ToString(e.Table[i].NumberOfSatellites) + "   " + s;
                        });

                    }
                    else
                    {
                        DispatchIfNecessary(() =>
                        {
                            mapCtrl.Txt9.Text = "GNSS:   S " + Convert.ToString(dchislo1) + '\xB0' + "   " + "W " + Convert.ToString(dchislo2) + '\xB0' +
                                           "   " + Convert.ToString(e.Table[i].NumberOfSatellites) + "   " + s;
                        });

                    }


                } //SK42grad

                // Mercator,m
                else if (initMapYaml.SysCoord == 3)
                {
                    double xxx2 = 0;
                    double yyy2 = 0;

                    ClassGeoCalculator.f_WGS84_Mercator(e.Table[i].Location.Latitude, -e.Table[i].Location.Longitude, ref xxx2, ref yyy2);

                    DispatchIfNecessary(() =>
                    {
                        mapCtrl.Txt9.Text = "GNSS:   X " + Convert.ToString((int)xxx2) + " m" + "   " + "Y " + Convert.ToString((int)yyy2) + " m" +
                                         "   " + Convert.ToString(e.Table[i].NumberOfSatellites) + "   " + s;
                    });

                } // Mercator,m

                // .............................................................................................

            } // FOR
            // -------------------------------------------------------------------------------------------------
            if (e.Table.Count > 0)
            {
                GlobalVarMapMain.GNSS_Lat = e.Table[e.Table.Count - 1].Location.Latitude;
                GlobalVarMapMain.GNSS_Long = -e.Table[e.Table.Count - 1].Location.Longitude;
                GlobalVarMapMain.GNSS_time = s;
                GlobalVarMapMain.GNSS_Numb_Sat = e.Table[e.Table.Count - 1].NumberOfSatellites;
            }
            // -------------------------------------------------------------------------------------------------

        }
        // ************************************************************************************************** GPS

        // ASP *************************************************************************************************
        // Updated table of ASP

        private void OnTableAspUp(object sender, TableEventArs<TableASP> e)
        {

            // .................................................................................................
            if (GlobalVarMapMain.fltbl == false)
                return;
            // .................................................................................................

            List<TableASP> lstASP = new List<TableASP>(e.Table);
            // .................................................................................................
            // Убрать без координат

            if (lstASP.Count > 0)
            {
                for (int iijjs = (lstASP.Count - 1); iijjs >= 0; iijjs--)
                {
                    if (
                        (lstASP[iijjs].Coordinates.Latitude == -1) ||
                        (lstASP[iijjs].Coordinates.Longitude == -1) ||
                        (lstASP[iijjs].Coordinates.Latitude == 0) ||
                        (lstASP[iijjs].Coordinates.Longitude == 0)
                        )
                    {
                        try
                        {
                            lstASP.Remove(lstASP[iijjs]);
                        }
                        catch
                        { }

                    }
                }
            }

            if (lstASP.Count > 0)
            {
                for (int iijjss = (lstASP.Count - 1); iijjss >= 0; iijjss--)
                {
                    try
                    {
                        lstASP[iijjss].Coordinates.Latitude = lstASP[iijjss].Coordinates.Latitude < 0 ? lstASP[iijjss].Coordinates.Latitude * -1 : lstASP[iijjss].Coordinates.Latitude;
                        lstASP[iijjss].Coordinates.Longitude = lstASP[iijjss].Coordinates.Longitude < 0 ? lstASP[iijjss].Coordinates.Longitude * -1 : lstASP[iijjss].Coordinates.Longitude;
                    }
                    catch
                    { }
                }
            }
            // .................................................................................................
            // Map -> Clear all

            if ((lstASP.Count == 0) && (mapCtrl.objClassInterfaceMap.List_JS.Count > 0))
            {
                objClassFunctionsMain.ClearListJS();


                // IRIFRCH ------------------------------------------------------------------------
                // Удаляем SPj из внутреннего списка ИРИi

                int ipl1 = 0;

                // Лист ИРИ ФРЧ с пеленгами
                for (ipl1 = 0; ipl1 < mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count; ipl1++)
                {
                    // Удаляем внутренние списки в ИРИi
                    if (mapCtrl.objClassInterfaceMap.List_SRW_FRF[ipl1].list_JSBearing.Count > 0)
                    {
                        for (int i = mapCtrl.objClassInterfaceMap.List_SRW_FRF[ipl1].list_JSBearing.Count - 1; i >= 0; i--)
                        {
                            try
                            {
                                mapCtrl.objClassInterfaceMap.List_SRW_FRF[ipl1].list_JSBearing.RemoveAt(i);
                            }
                            catch
                            { }
                        }
                    }

                } // FOR IRIFRCH
                // ------------------------------------------------------------------------ IRIFRCH

                // Clear and redraw
                Dispatcher.Invoke(() =>
                {
                    mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                });
                return;
            }
            // .................................................................................................
            // UpdateASP

            bool f = false;

            if (
                (lstASP.Count > 0)
                //&& (mapCtrl.objClassInterfaceMap.ShowJS == true)
                )
            {
                Dispatcher.Invoke(() =>
                {
                    f=objClassFunctionsMain.UpdateASP(lstASP);
                });
                Dispatcher.Invoke(() =>
                {
                    if(f==true)
                      mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                });

            }
            // .................................................................................................

        }
        // ************************************************************************************************* ASP

        // ИРИ ФРЧ ********************************************************************************************
        // Updated table of ИРИ ФРЧ

        private void OnTempFWSUp(object sender, TableEventArs<TempFWS> e)
        {

            if (GlobalVarMapMain.fltbl == false)
                return;

            // .................................................................................................
            // Map -> Clear all

            if ((e.Table.Count == 0) && (mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count > 0))
            {
                objClassFunctionsMain.ClearListFRH();

                // Clear and redraw
                Dispatcher.Invoke(() =>
                {
                    mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                });
                return;
            }
            // .................................................................................................
            bool f = false;

            Dispatcher.Invoke(() =>
            {
                f=objClassFunctionsMain.UpdateIRIFRCH(e.Table);
            });

            Dispatcher.Invoke(() =>
            {
                if (f == true)
                    mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
            });

            // .................................................................................................

        }
        // ******************************************************************************************** ИРИ ФРЧ

        // ИРИ ФРЧ1 *******************************************************************************************
        // Updated table of ИРИ ФРЧ

        private void OnTempFWSAddRange(object sender, TableEventArs<TempFWS> e)
        {
            if (GlobalVarMapMain.fltbl == false)
                return;

            // .................................................................................................
            // Map -> Clear all

            if ((e.Table.Count == 0) && (mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count > 0))
            {
                objClassFunctionsMain.ClearListFRH();

                // Clear and redraw
                Dispatcher.Invoke(() =>
                {
                    mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                });
                return;
            }
            // .................................................................................................

            bool f = false;
            Dispatcher.Invoke(() =>
            {
                f=objClassFunctionsMain.UpdateIRIFRCH(e.Table);
            });

            Dispatcher.Invoke(() =>
            {
                if(f==true)
                   mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
            });

        }
        // ******************************************************************************************* ИРИ ФРЧ1

        // ИРИ ФРЧ ЦР ******************************************************************************************
        // Updated table of ИРИ ФРЧ ЦР

        private void OnTableReconFWSUp(object sender, TableEventArs<TableReconFWS> e)
        {
            if (GlobalVarMapMain.fltbl == false)
                return;

            // .................................................................................................
            // Map -> Clear all

                if ((e.Table.Count == 0) && (mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.Count > 0))
                {
                    objClassFunctionsMain.ClearListFRHCR();

                    // Clear and redraw
                    Dispatcher.Invoke(() =>
                    {
                        mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                    });
                    return;
                }
            // .................................................................................................
            bool f = false;

            Dispatcher.Invoke(() =>
                {
                    f=objClassFunctionsMain.UpdateIRIFRCH_CR(e.Table);
                });

            Dispatcher.Invoke(() =>
            {
                if (f == true)
                    mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
            });
            // .................................................................................................

        }
        // ****************************************************************************************** ИРИ ФРЧ ЦР

        // ИРИ ФРЧЦР1 *******************************************************************************************
        // Updated table of ИРИ ФРЧЦР1
        // !!! Не надо


        /*
                private void OnTableReconFWSAddRange(object sender, TableEventArs<TableReconFWS> e)
                {
                    if (GlobalVarMapMain.fltbl == false)
                        return;

                    // .................................................................................................
                    // Map -> Clear all

                    try
                    {

                        if ((e.Table.Count == 0) && (mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.Count > 0))
                        {
                            objClassFunctionsMain.ClearListFRHCR();

                            // Clear and redraw
                            Dispatcher.Invoke(() =>
                            {
                                mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                            });
                            return;
                        }
                        // .................................................................................................
                        Dispatcher.Invoke(() =>
                        {
                            objClassFunctionsMain.UpdateIRIFRCH_CR(e.Table);
                        });
                        // .................................................................................................

                    }
                    catch 
                    {
                        int f = 7;
                    }
                }
        */
        // ******************************************************************************************* ИРИ ФРЧ1

        // ИРИ ФРЧ РП *****************************************************************************************
        // Updated table of ИРИ ФРЧ РП

        private void OnTableSuppressFWSUp(object sender, TableEventArs<TableSuppressFWS> e)
        {
            if (GlobalVarMapMain.fltbl == false)
                return;

            // .................................................................................................
            // Map -> Clear all

            if ((e.Table.Count == 0) && (mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.Count > 0))
            {
                objClassFunctionsMain.ClearListFRHRP();

                // Clear and redraw
                Dispatcher.Invoke(() =>
                {
                    mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                });
                return;
            }
            // .................................................................................................
            bool f = false;

            Dispatcher.Invoke(() =>
            {
                f=objClassFunctionsMain.UpdateIRIFRCH_RP(e.Table);
            });

            Dispatcher.Invoke(() =>
            {
                if (f == true)
                    mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
            });
            // .................................................................................................

        }
        // ***************************************************************************************** ИРИ ФРЧ РП

        // ИРИ ППРЧ РП ****************************************************************************************
        // Updated table of ИРИ ППРЧ РП
        // !!!  Не рисуем

        private void OnTableSuppressFHSSUp(object sender, TableEventArs<TableSuppressFHSS> e)
        {
            if (GlobalVarMapMain.fltbl == false)
                return;

        }
        // **************************************************************************************** ИРИ ППРЧ РП

        // ИРИ ППРЧ *******************************************************************************************
        // Updated table of ИРИ ППРЧ
        // !!! Для очистки TableReconFHSS

        private void OnTableReconFHSSUp(object sender, TableEventArs<TableReconFHSS> e)
        {

            if (GlobalVarMapMain.fltbl == false)
                return;

            // .................................................................................................
            // Map -> Clear TableReconFHSS

            if ((e.Table.Count == 0) && (mapCtrl.objClassInterfaceMap.List_SRW_STRF_Recon.Count > 0))
            {
                objClassFunctionsMain.ClearListPPRH2();

                return;
            }
            // .................................................................................................

        }
        // ******************************************************************************************* ИРИ ППРЧ

        // ИРИ ППРЧ Координаты ********************************************************************************
        // Координаты для ИРИ ППРЧ
        // !!! Для очистки TableSourceFHSS и основного списка

        private void OnTableSourceFHSSUp(object sender, TableEventArs<TableSourceFHSS> e)
        {
            if (GlobalVarMapMain.fltbl == false)
                return;

            // .................................................................................................
            // Map -> Clear TableSourceFHSS

            if ((e.Table.Count == 0) && (mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord.Count > 0))
            {
                objClassFunctionsMain.ClearListPPRH1();
            }
            // .................................................................................................
            if ((e.Table.Count == 0) && (mapCtrl.objClassInterfaceMap.List_SRW_STRF.Count > 0))
            {
                objClassFunctionsMain.ClearListPPRH();

                // Clear and redraw
                Dispatcher.Invoke(() =>
                {
                    mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                });
                return;
            }
            // .................................................................................................

        }
        // ******************************************************************************** ИРИ ППРЧ Координаты

        // ИРИ ППРЧ1 *******************************************************************************************
        // TableReconFHSS

        private void OnTableReconFHSSAddRange(object sender, TableEventArs<TableReconFHSS> e)
        {
            if (GlobalVarMapMain.fltbl == false)
                return;

            // .................................................................................................
            // Map -> Clear TableReconFHSS

            if ((e.Table.Count == 0) && (mapCtrl.objClassInterfaceMap.List_SRW_STRF_Recon.Count > 0))
            {
                objClassFunctionsMain.ClearListPPRH2();

                return;
            }
            // .................................................................................................
            mapCtrl.objClassInterfaceMap.List_SRW_STRF_Recon = e.Table;
            // .................................................................................................

        }
        // ******************************************************************************************* ИРИ ППРЧ1

        // ИРИ ППРЧ2 *******************************************************************************************
        // TableSourceFHSS-> Заполнение основного списка ИРИППРЧ

        private void OnTableSourceFHSSAddRange(object sender, TableEventArs<TableSourceFHSS> e)
        {
            if (GlobalVarMapMain.fltbl == false)
                return;

            // .................................................................................................
            // Map -> Clear TableSourceFHSS

            if ((e.Table.Count == 0) && (mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord.Count > 0))
            {
                objClassFunctionsMain.ClearListPPRH1();
            }
            // .................................................................................................
            if ((e.Table.Count == 0) && (mapCtrl.objClassInterfaceMap.List_SRW_STRF.Count > 0))
            {
                objClassFunctionsMain.ClearListPPRH();

                // Clear and redraw
                Dispatcher.Invoke(() =>
                {
                    mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                });
                return;
            }
            // .................................................................................................
            Dispatcher.Invoke(() =>
            {
                objClassFunctionsMain.UpdateIRIPPRCH(mapCtrl.objClassInterfaceMap.List_SRW_STRF_Recon, e.Table);
            });
            // .................................................................................................

        }
        // ******************************************************************************************* ИРИ ППРЧ2

        // AIRPLANES ******************************************************************************************
        // Updated table of Airplanes
        // Этого обработчика нету
        // !!!OLD

        private void OnTempADSB(object sender, TableEventArs<TempADSB> e)
        {
            if (GlobalVarMapMain.fltbl == false)
                return;

        } // Airplanes
          // ------------------------------------------------------------------------------------------------------
          // NEW Airplanes

        // ......................................................................................................
        // Приходит с пустым листом для очистки всех самолетов или если лист не пустой -> для удаления одного самолета
        // Тавлица -> кнопка Delete

        private void HandlerUpdateTempADSB(object sender, TableEventArs<TempADSB> e)
        {
            try
            {
                if (GlobalVarMapMain.fltbl == false)
                    return;
                // -----------------------------------------------------------------------------------------
                // Удаление всех самолетов

                // Map -> Clear all
                if (e.Table.Count == 0)
                {
                    if (mapCtrl.objClassInterfaceMap.List_AP.Count > 0)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            try
                            {
                                aDSBControl.DEL_TAB();
                            }
                            catch
                            {
                                //MessageBox.Show("Error6");
                            }
                        });

                        objClassFunctionsMain.ClearListAP();

                        // Clear and redraw
                        Dispatcher.Invoke(() =>
                        {
                            if(GlobalVarMapMain.FlagLanguage==false)
                             mapCtrl.Txt10.Text = "Number of aircraft:" + " " + "0";
                            else
                             mapCtrl.Txt10.Text = "Количество ВО:" + " " + "0";

                            mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                        });
                    }
                } // e.Table.Count==0
                  // -----------------------------------------------------------------------------------------
                  // Удаление одного самолета
                  // NEW1

                else
                {
                    List<TempADSB> listTableADSB = new List<TempADSB>(e.Table);

                    Dispatcher.Invoke(() =>
                    {
                        objClassFunctionsMain.GetADSB_DB(listTableADSB, 1);
                    });

                    // ForArsen
                    Dispatcher.Invoke(() =>
                    {
                        try
                        {
                            aDSBControl.DelAirMain(e.Table);
                        }
                        catch
                        {
                            //MessageBox.Show("Error7");
                        }
                    });

                    DispatchIfNecessary(() =>
                    {
                        if (GlobalVarMapMain.FlagLanguage == false)
                            mapCtrl.Txt10.Text = "Number of aircraft:" + " " + Convert.ToString(mapCtrl.objClassInterfaceMap.List_AP.Count);
                        else
                            mapCtrl.Txt10.Text = "Количество ВО:" + " " + Convert.ToString(mapCtrl.objClassInterfaceMap.List_AP.Count);

                    });

                    Dispatcher.Invoke(() =>
                    {
                        mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                    });

                }
            }
            catch
            {
                int y = 6;
            }
            // -----------------------------------------------------------------------------------------

        }

        private void ADSBCleanerEvent_OnCleanADSBTable(object sender, EventArgs e)
        {
            if (GlobalVarMapMain.fltbl == false)
                return;

            try
            {
                if (clientDB != null)
                {
                    try
                    {
                        clientDB.Tables[NameTable.TempADSB].CLearAsync();
                    }
                    catch
                    {
                        //MessageBox.Show("Error9");
                    }

                }

            }
            catch (Exception)
            { }
        }
        // ......................................................................................................
        // Приходит по одной записи

        // NEW1
        private void HandlerAddRecordTempADSB(object sender, TempADSB e)
        {
            try
            {
                if (GlobalVarMapMain.fltbl == false)
                    return;

                Dispatcher.Invoke(() =>
                {
                    // Arsen_Tab
                    try
                    {
                        aDSBControl.AddRange(new List<TempADSB>() { e });
                    }
                    catch
                    {
                        //MessageBox.Show("Error10");
                    }
                });

                List<TempADSB> listTableADSB = new List<TempADSB>();

                if ((e.Coordinates.Latitude != -1) &&
                (e.Coordinates.Longitude != -1) &&
                (e.Coordinates.Latitude != 0) &&
                (e.Coordinates.Longitude != 0)
                )
                {
                    listTableADSB.Add(e);
                }

                Dispatcher.Invoke(() =>
                {
                    if (listTableADSB.Count > 0)
                    {
                        try
                        {
                            objClassFunctionsMain.GetADSB_DB(listTableADSB, 0);
                        }
                        catch
                        {
                            //MessageBox.Show("Error12");
                        }
                    }
                });

                DispatchIfNecessary(() =>
                {
                    if (GlobalVarMapMain.FlagLanguage == false)
                        mapCtrl.Txt10.Text = "Number of aircraft:" + " " + Convert.ToString(mapCtrl.objClassInterfaceMap.List_AP.Count);
                    else
                        mapCtrl.Txt10.Text = "Количество ВО:" + " " + Convert.ToString(mapCtrl.objClassInterfaceMap.List_AP.Count);


                });
            }

            catch
            {
                int g = 6;
            }

        }
        // ......................................................................................................


        // ****************************************************************************************** AIRPLANES


        // ********************************************************************************************* DATA_BASE

    } // Class
} // Namespace
