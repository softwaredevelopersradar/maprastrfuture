﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

//using WpfTasksControl;

namespace WpfMapRastr
{
    /// <summary>
    /// Interaction logic for WindowAzimuth.xaml
    /// </summary>
    public partial class WindowAzimuth : Window
    {
        public WindowAzimuth()
        {
            InitializeComponent();

            this.Closing += WndAz_Closing;

        }

        // Closing ************************************************************************
        public void WndAz_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
/*
            GlobalVarMapMain.flAzimuth = false;

            // Hide (спрятать) window, but NOT delete
            e.Cancel = true;
            Hide();
*/
        }
        // ************************************************************************ Closing

        // Minimaze window down ***********************************************************

        private void WndAz_Deactivated(object sender, EventArgs e)
        {
        }
        // *********************************************************** Minimaze window down

        // Activated window ***************************************************************
        private void WndAz_Activated(object sender, EventArgs e)
        {
           //GlobalVarMapMain.flAzimuth = true;
        }
        // *************************************************************** Activated window

        // State_Changed ******************************************************************
        private void WndAz_StateChanged(object sender, EventArgs e)
        {
            //if(this.WindowState==WindowState.Minimized)
            //    GlobalVarMapMain.flAzimuth = false;
        }
        // ****************************************************************** State_Changed

    } // Class
} // Namespace
