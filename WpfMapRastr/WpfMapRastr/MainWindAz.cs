﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ClientDataBase;

using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

using System.Windows.Threading;
using System.Threading.Tasks;

// Otl
// Для YAML реализации
using YamlDotNet.Serialization;

using WpfMapRastrControl;
using WpfAzimuthControl;
using ModelsTablesDBLib;
using Bearing;
using GeoCalculator;
using InheritorsEventArgs;
using ClassLibraryIniFiles;
using ControlSettingsMap;
using ADSBControl;
using AzimutTask;

using System.ComponentModel;
using System.Runtime.CompilerServices;

// Semen
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;

namespace WpfMapRastr
{
    public partial class MainWindow
    {


        // AZIMUTH ************************************************************************************
        // Azimuth*
        // BLOCK FOR AZIMUTH

        // ShowAzimutChange ***************************************************************************
        // Обработка изменения состояния пртички отображения азимута около станций

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

            //mapCtrl.objClassInterfaceMap.CheckShowAzimuth = AzimutTaskCntr.IsShowValue;
            if (AzimutTaskCntr.IsShowValue == true)
                mapCtrl.objClassInterfaceMap.CheckShowAzimuth = false;
            else
                mapCtrl.objClassInterfaceMap.CheckShowAzimuth = true;

            // !!! Расчет идет в перерисовке азимута
            mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
        }
        // *************************************************************************** ShowAzimutChange

        // ChangeItemInTabControl *********************************************************************
        // Переход на вкладку в NabControl задач

        private void OnTabItemChanged(object sender, SelectionChangedEventArgs e)
        {
            TabControl tabControl = sender as TabControl; // e.Source could have been used instead of sender as well
            TabItem item = tabControl.SelectedValue as TabItem;
            if (item.Name == "TabCtrlTasksIt1")
            {
                mapCtrl.objClassInterfaceMap.flAzimuth = true;
            }
            else
            {
                mapCtrl.objClassInterfaceMap.flAzimuth = false;
            }

        }
        // *********************************************************************** ChangeItemInTabControl

        // !!! См. еще клик мыши на карте

        // ButtonClear **********************************************************************************

        private void ButtonOnClear(object sender, EventArgs e)
        {
            if ((mapCtrl.objClassInterfaceMap.flAzimuth == true) && (mapCtrl.objClassInterfaceMap.ShowJS == true))
            {
                objClassFunctionsMain.ClearAzimuth();
            }
        }
        // *********************************************************************************** ButtonClear

        // ButtonExecute *********************************************************************************
        // Выполнить, если координаты источника ввели вручную

        private void ButtonOnExecute(object sender, AzimutTask.Models.ICoord e)
        {

            if ((mapCtrl.objClassInterfaceMap.flAzimuth == true) && (mapCtrl.objClassInterfaceMap.ShowJS == true))
            {
                // !!! Здесь нужно занести координаты источника из окошек Таниного контрола
                // ?????????????????????
                //mapCtrl.objClassInterfaceMap.LatSource_Azimuth=
                //mapCtrl.objClassInterfaceMap.LongSource_Azimuth =
                //mapCtrl.objClassInterfaceMap.HSource_Azimuth =
                //int yy = (int)(e as AzimutTask.Models.CoordXY).X;
                //AzimutTask.Models mmm = new AzimutTask.Models();
                //var pp = e as AzimutTask.Models.CoordXY;

                //objClassFunctionsMain.ReadCurrentCoordinatesAzimuth();

                AzimutTask.Models.CoordXY modelXY = new AzimutTask.Models.CoordXY();
                AzimutTask.Models.CoordDegree modelDegree = new AzimutTask.Models.CoordDegree();
                AzimutTask.Models.CoordDdMmSs modelDdMmSs = new AzimutTask.Models.CoordDdMmSs();

                if (e.GetType() == typeof(AzimutTask.Models.CoordXY))
                {
                    //AzimutTask.Models.CoordXY modelXY = e as AzimutTask.Models.CoordXY;
                    modelXY = e as AzimutTask.Models.CoordXY;
                }
                else if (e.GetType() == typeof(AzimutTask.Models.CoordDegree))
                {
                    //AzimutTask.Models.CoordDegree modelDegree = e as AzimutTask.Models.CoordDegree;
                    modelDegree = e as AzimutTask.Models.CoordDegree;
                }
                else if (e.GetType() == typeof(AzimutTask.Models.CoordDdMmSs))
                {
                    //AzimutTask.Models.CoordDdMmSs modelDdMmSs = e as AzimutTask.Models.CoordDdMmSs;
                    modelDdMmSs = e as AzimutTask.Models.CoordDdMmSs;

                }

                objClassFunctionsMain.ReadCurrentCoordinatesAzimuth(modelXY, modelDegree, modelDdMmSs);

                // ----------------------------------------------------------------------------------------
                mapCtrl.objClassInterfaceMap.CheckShowAzimuth = AzimutTaskCntr.IsShowValue;

                // !!! Расчет идет в перерисовке азимута
                mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                // ----------------------------------------------------------------------------------------

            } // if ((mapCtrl.objClassInterfaceMap.flAzimuth == true) && (mapCtrl.objClassInterfaceMap.ShowJS == true))
        }
        // ******************************************************************************* ButtonExecute


        // ************************************************************************************ AZIMUTH

    } // Class
} // Namespace
