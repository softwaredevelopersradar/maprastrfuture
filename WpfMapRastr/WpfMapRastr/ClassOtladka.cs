﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfMapRastr
{
    class ClassOtladka
    {
        // Отладочные куски


        // MainWindow.xaml.cs *************************************************************

        // UpdateSettings ----------------------------------------------------------------

        // ..............................................................................
        // Read from PropertyGrid

        //GlobalVarMapMain.IP_DB = SettingsMap.Settings.IP_DB;
        //GlobalVarMapMain.NumbPort_DB = SettingsMap.Settings.NumPort;
        //GlobalVarMapMain.SysCoord = (byte)SettingsMap.Settings.SysCoord;
        // !!!Otkommentir
        ////GlobalVarMapMain.FormatCoord = (byte)SettingsMap.Settings.FormCoord;
        // ..............................................................................
        // Change SystemCoordinates On the Map (Отображение внизу карты)

        /*
                              // WGS 84
                               if (initMapYaml.SysCoord == 0)
                               {
                                // Latitude
                                ichislo1 = (long)(GlobalVarMap.LAT_Rastr * 1000000);
                                dchislo1 = ((double)ichislo1) / 1000000;
                                // Longitude
                                ichislo2 = (long)(GlobalVarMap.LONG_Rastr * 1000000);
                                dchislo2 = ((double)ichislo2) / 1000000;

                                mapCtrl.Txt1.Text = "N " + Convert.ToString(dchislo1) + '\xB0';
                                mapCtrl.Txt3.Text = "W " + Convert.ToString(dchislo2) + '\xB0';

                               } // WGS84

                               // SK42,m
                               else if (initMapYaml.SysCoord == 2)
                                {
                                ClassGeoCalculator.f_WGS84_Krug(GlobalVarMap.LAT_Rastr,
                                                               GlobalVarMap.LONG_Rastr,
                                                               ref xxx, ref yyy);

                                mapCtrl.Txt1.Text = "X " + Convert.ToString((int)xxx) + " m";
                                mapCtrl.Txt3.Text = "Y " + Convert.ToString((int)yyy) + " m";

                                } // SK42,m
        */
        // ..............................................................................

        // ---------------------------------------------------------------- UpdateSettings

        // MapCtrl_OnMouseCl -------------------------------------------------------------

        // ..............................................................................

        // !!! Проверка функции расстояния между точками с учетом кривизны Земли
        /*
                            double d1 = 0;
                            double d2 = 0;
                            double d3 = 0;
                            double x1 = 0;
                            double x2 = 0;
                            double y1 = 0;
                            double y2 = 0;
                            double dx = 0;
                            double dy = 0;

                            d1 = ClassBearing.f_D_2Points(
                                                          GlobalVarMapMain.LatPel1,
                                                          GlobalVarMapMain.LonPel1,
                                                          GlobalVarMapMain.LatPel2,
                                                          GlobalVarMapMain.LonPel2,
                                                          1 // WGS84
                                                         );

                            ClassGeoCalculator.f_WGS84_Krug(
                                                          GlobalVarMapMain.LatPel1,
                                                          GlobalVarMapMain.LonPel1,
                                                          ref x1,
                                                          ref y1
                                                           );

                            ClassGeoCalculator.f_WGS84_Krug(
                                                          GlobalVarMapMain.LatPel2,
                                                          GlobalVarMapMain.LonPel2,
                                                          ref x2,
                                                          ref y2
                                                           );

                            dx = x1 - x2;
                            dy = y1 - y2;
                            d2 = Math.Sqrt(dx*dx+dy*dy);

                            ClassGeoCalculator.f_WGS84_Mercator(
                                                          GlobalVarMapMain.LatPel1,
                                                          GlobalVarMapMain.LonPel1,
                                                          ref x1,
                                                          ref y1
                                                           );

                            ClassGeoCalculator.f_WGS84_Mercator(
                                                          GlobalVarMapMain.LatPel2,
                                                          GlobalVarMapMain.LonPel2,
                                                          ref x2,
                                                          ref y2
                                                           );

                            dx = x1 - x2;
                            dy = y1 - y2;
                            d3 = Math.Sqrt(dx * dx + dy * dy);
                            d3 = 1;
        */
        // ..............................................................................

        // ------------------------------------------------------------- MapCtrl_OnMouseCl

        // ButtonAz ----------------------------------------------------------------------

        /*         
        // ..............................................................................
                 // Sector (Circle)
                 double lat = 0;
                 double lon = 0;
                 lat = 23.181;
                 lon = 52.908;
                 Mapsui.Geometries.Point pointPel = new Mapsui.Geometries.Point(lon, lat);

                 string s = mapCtrl.DrawSectorLatLong(
                                                     // Degree
                                                     pointPel,
                                                     // Color
                                                     100,
                                                     0,
                                                     255,
                                                     0,
                                                     10000,
                                                     // degree
                                                     0,
                                                    20
                                                    );

                 if (s != "")
                    MessageBox.Show(s);
        // ..............................................................................
        */

        // ..............................................................................
        /*
                    TableASP obj;
                    obj = new TableASP();
                    obj.Coordinates.Latitude = 53.889034;
                    obj.Coordinates.Longitude = 27.514555;
                    GlobalVarMapMain.listJS.Add(obj);

                    obj = new TableASP();
                    obj.Coordinates.Latitude = 53.912895;
                    obj.Coordinates.Longitude = 27.538245;
                    GlobalVarMapMain.listJS.Add(obj);

                    obj = new TableASP();
                    obj.Coordinates.Latitude = 53.936069;
                    obj.Coordinates.Longitude = 27.568285;
                    GlobalVarMapMain.listJS.Add(obj);

                    obj = new TableASP();
                    obj.Coordinates.Latitude = 53.932464;
                    obj.Coordinates.Longitude = 27.618926;
                    GlobalVarMapMain.listJS.Add(obj);
        */

        /*
                    // Get current dir
                    string dir = "";
                    dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                    for (int i=0;i< GlobalVarMapMain.listJS.Count;i++)
                    {

        string s = mapCtrl.DrawImage(
                                                 GlobalVarMapMain.listJS[i].Coordinates.Latitude,
                                                 GlobalVarMapMain.listJS[i].Coordinates.Longitude,
                                                 // Path to image
                                                 dir + "\\Images\\Jammer\\" + "1.png",
                                                 // Text
                                                 "",
                                                 // Scale
                                                 2
                                      );

                    }
        */
        // ..............................................................................

        // ---------------------------------------------------------------------- ButtonAz

        // ButtonCalcTriang -------------------------------------------------------------

        // ..............................................................................

        /*
                    List<Mapsui.Geometries.Point> pointPel = new List<Mapsui.Geometries.Point>();
                    pointPel.Add(new Mapsui.Geometries.Point(27.514555, 53.889034));
                    pointPel.Add(new Mapsui.Geometries.Point(27.538245, 53.912895));
                    pointPel.Add(new Mapsui.Geometries.Point(27.568285, 53.936069));
                    pointPel.Add(new Mapsui.Geometries.Point(27.618926, 53.932464));

                    string s = mapCtrl.DrawLinesLatLong(
                                                        // Degree
                                                        pointPel,
                                                        // Color
                                                        100,
                                                        255,
                                                        0,
                                                        0
                                                       );
        */
        // ..............................................................................
        // Triangulation1

        /*
                    int f = 0;

                    f = ClassBearing.f_Triangulation
                        (
                         GlobalVarMapMain.distance,
                         GlobalVarMapMain.numberofdots,
                         GlobalVarMapMain.Pel1Grad,
                         GlobalVarMapMain.Pel2Grad,
                         GlobalVarMapMain.LatPel1,
                         GlobalVarMapMain.LonPel1,
                         GlobalVarMapMain.LatPel2,
                         GlobalVarMapMain.LonPel2,
                         1, // WGS84
                         GlobalVarMapMain.distance_pel,

                        ref GlobalVarMapMain.LatTriang,
                        ref GlobalVarMapMain.LonTriang

                        );

                    if (f == -1)
                    {
                        MessageBox.Show("Несовместимые пеленги");
                        return;
                    }
                    else if (f == -2)
                    {
                        MessageBox.Show("Недопустимые параметры");
                        return;
                    }

                    else
                    {
                        // Get current dir
                        string dir = "";
                        dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

                        string s3 = mapCtrl.DrawImage(
                                                     GlobalVarMapMain.LatTriang,
                                                     GlobalVarMapMain.LonTriang,
                                                     // Path to image
                                                     dir + "\\Images\\OtherObject\\" + "tr10.png",
                                                     // Text
                                                     "",
                                                     // Scale
                                                     0.6
                                          );

                    }
        */
        // ..............................................................................

        // ------------------------------------------------------------- ButtonCalcTriang

        // OnTempGNSSUp -----------------------------------------------------------------

        // 19.07
        /*
                    TempGNSS obj = new TempGNSS();
                    obj.NumberOfSatellites = 35;
                    obj.Location = new Coord();
                    obj.Location.Latitude = 53.88085;
                    obj.Location.Longitude = 27.60605;

                    DateTime tm = new DateTime(2019,6,19,17,44,32);

                    obj.LocalTime = tm;

                    e.Table.Add(obj);
        */

        // ----------------------------------------------------------------- OnTempGNSSUp

        // OnTableAspUp -----------------------------------------------------------------

        // 19.07
        /*
                                TableASP obj;

                                obj = new TableASP();
                                obj.Coordinates.Latitude = 53.88085;     // ангар завода
                                obj.Coordinates.Longitude = 27.60605;
                                //obj.Coordinates.Latitude = 53.883079;  // верхний левый угол завода
                                 // obj.Coordinates.Longitude = 27.604591;
                                e.Table.Add(obj);

                                obj = new TableASP();
                                obj.Coordinates.Latitude = 53.895943;
                                obj.Coordinates.Longitude = 27.502021;
                                e.Table.Add(obj);

                                obj = new TableASP();
                                obj.Coordinates.Latitude = 53.896458;
                                obj.Coordinates.Longitude = 27.527856;
                                e.Table.Add(obj);

                                obj = new TableASP();
                                obj.Coordinates.Latitude = 53.880837;
                                obj.Coordinates.Longitude = 27.525367;
                                e.Table.Add(obj);
        */
        // ----------------------------------------------------------------- OnTableAspUp

        // OnTempFWSUp -----------------------------------------------------------------
        /*
                    TempFWS obj;

                    Random rand = new Random();
                    Random rand1 = new Random();
                    int j = 0;
                    int j1 = 0;
                    int f = 50;
                    double lt_temp = 0;
                    double ln_temp = 0;

                    for (j1 = 0; j1 < 10; j1++)
                    {
                        for (j = 0; j < 5; j++)
                        {
                            lt_temp = Convert.ToDouble(rand.Next(GlobalVarMapMain.lat1_temp1, GlobalVarMapMain.lat2_temp1)) / 100000;
                            ln_temp = Convert.ToDouble(rand1.Next(GlobalVarMapMain.lon1_temp1, GlobalVarMapMain.lon2_temp1)) / 100000;
                            obj = new TempFWS();
                            obj.Coordinates = new Coord();
                            obj.Coordinates.Latitude = lt_temp;
                            obj.Coordinates.Longitude = ln_temp;
                            obj.FreqKHz = f;
                            e.Table.Add(obj);
                        }
                        if (j1 < 4)
                            f += 50;
                        else
                            f += 250;
                    }
        */
        // ----------------------------------------------------------------- OnTempFWSUp

        // OnTableReconFWSUp -----------------------------------------------------------
        /*
                TableReconFWS obj;

                Random rand = new Random();
                Random rand1 = new Random();
                int j = 0;
                int j1 = 0;
                int f = 50;
                double lt_temp = 0;
                double ln_temp = 0;

                    for (j1 = 0; j1< 10; j1++)
                    {
                        for (j = 0; j< 5; j++)
                        {
                            lt_temp = Convert.ToDouble(rand.Next(GlobalVarMapMain.lat1_temp1, GlobalVarMapMain.lat2_temp1)) / 100000;
                            ln_temp = Convert.ToDouble(rand1.Next(GlobalVarMapMain.lon1_temp1, GlobalVarMapMain.lon2_temp1)) / 100000;
                            obj = new TableReconFWS();
                obj.Coordinates = new Coord();
                obj.Coordinates.Latitude = lt_temp;
                            obj.Coordinates.Longitude = ln_temp;
                            obj.FreqKHz = f;
                            e.Table.Add(obj);
                        }
                        if (j1< 4)
                            f += 50;
                        else
                            f += 250;
                    }
        */
        // ----------------------------------------------------------- OnTableReconFWSUp

        // OnTableSuppressFWSUp --------------------------------------------------------
        /*
                TableSuppressFWS obj;

                Random rand = new Random();
                Random rand1 = new Random();
                int j = 0;
                int j1 = 0;
                int f = 50;
                double lt_temp = 0;
                double ln_temp = 0;

                    for (j1 = 0; j1< 10; j1++)
                    {
                        for (j = 0; j< 5; j++)
                        {
                            lt_temp = Convert.ToDouble(rand.Next(GlobalVarMapMain.lat1_temp1, GlobalVarMapMain.lat2_temp1)) / 100000;
                            ln_temp = Convert.ToDouble(rand1.Next(GlobalVarMapMain.lon1_temp1, GlobalVarMapMain.lon2_temp1)) / 100000;
                            obj = new TableSuppressFWS();
                obj.Coordinates = new Coord();
                obj.Coordinates.Latitude = lt_temp;
                            obj.Coordinates.Longitude = ln_temp;
                            obj.FreqKHz = f;
                            e.Table.Add(obj);
                        }
                        if (j1< 4)
                            f += 50;
                        else
                            f += 250;
                    }
        */
        // -------------------------------------------------------- OnTableSuppressFWSUp

        // OnTableSuppressFHSSUp -------------------------------------------------------
        /*
                TableSuppressFHSS obj;

                Random rand = new Random();
                Random rand1 = new Random();
                int j = 0;
                int j1 = 0;
                int f = 50;
                double lt_temp = 0;
                double ln_temp = 0;

                    for (j1 = 0; j1< 10; j1++)
                    {
                        for (j = 0; j< 5; j++)
                        {
                            lt_temp = Convert.ToDouble(rand.Next(GlobalVarMapMain.lat1_temp1, GlobalVarMapMain.lat2_temp1)) / 100000;
                            ln_temp = Convert.ToDouble(rand1.Next(GlobalVarMapMain.lon1_temp1, GlobalVarMapMain.lon2_temp1)) / 100000;
                            obj = new TableSuppressFHSS();
                obj.Coordinates = new Coord();
                obj.Coordinates.Latitude = lt_temp;
                            obj.Coordinates.Longitude = ln_temp;
                            obj.FreqKHz = f;
                            e.Table.Add(obj);
                        }
                        if (j1< 4)
                            f += 50;
                        else
                            f += 250;
                    }
        */
        // ------------------------------------------------------- OnTableSuppressFHSSUp

        // OnTempADSB -------------------------------------------------------------------

        // ..............................................................................
        /*
                TempADSB_AP obj2;
                List<TempADSB_AP> list_AP = new List<TempADSB_AP>();
                List<TempADSB_AP> lstap = new List<TempADSB_AP>();

                                            if (GlobalVarMapMain.sh1 == 0)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.78746;
                                                obj2.Coordinates.Longitude = 25.71507;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 1)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.78622;
                                                obj2.Coordinates.Longitude = 25.71123;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }

                                            else if (GlobalVarMapMain.sh1 == 2)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.78572;
                                                obj2.Coordinates.Longitude = 25.70958;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 3)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.78511;
                                                obj2.Coordinates.Longitude = 25.70768;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 4)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.7846;
                                                obj2.Coordinates.Longitude = 25.70607;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 5)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.78362;
                                                obj2.Coordinates.Longitude = 25.70308;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 6)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.78307;
                                                obj2.Coordinates.Longitude = 25.70149;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 7)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                 obj2.Coordinates.Latitude = 53.78247;
                                                obj2.Coordinates.Longitude = 25.69961;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 8)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.78133;
                                                obj2.Coordinates.Longitude = 25.69608;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 9)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.78082;
                                                obj2.Coordinates.Longitude = 25.69443;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 10)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.78027;
                                                obj2.Coordinates.Longitude = 25.69278;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 11)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.77971;
                                                obj2.Coordinates.Longitude = 25.69088;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 12)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.77908;
                                                obj2.Coordinates.Longitude = 25.68902;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 13)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.77803;
                                                obj2.Coordinates.Longitude = 25.68579;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 14)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.77752;
                                                obj2.Coordinates.Longitude = 25.6841;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 15)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.77696;
                                                obj2.Coordinates.Longitude = 25.68248;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 16)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.77645;
                                                obj2.Coordinates.Longitude = 25.68086;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 17)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.77603;
                                                obj2.Coordinates.Longitude = 25.67949;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 18)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.77496;
                                                obj2.Coordinates.Longitude = 25.67626;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 19)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.77435;
                                                obj2.Coordinates.Longitude = 25.6744;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 20)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.77272;
                                                obj2.Coordinates.Longitude = 25.66924;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 21)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.76086;
                                                obj2.Coordinates.Longitude = 25.6326;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 22)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.75983;
                                                obj2.Coordinates.Longitude = 25.62941;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 23)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.75876;
                                                obj2.Coordinates.Longitude = 25.62609;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 24)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.7575;
                                                obj2.Coordinates.Longitude = 25.6223;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 25)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.75629;
                                                obj2.Coordinates.Longitude = 25.61855;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 26)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.75514;
                                                obj2.Coordinates.Longitude = 25.61502;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 27)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.754;
                                                obj2.Coordinates.Longitude = 25.61148;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 28)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.7528;
                                                obj2.Coordinates.Longitude = 25.60768;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 29)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.75189;
                                                obj2.Coordinates.Longitude = 25.60497;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 30)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.75084;
                                                obj2.Coordinates.Longitude = 25.6017;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 31)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.74968;
                                                obj2.Coordinates.Longitude = 25.59822;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }
                                            else if (GlobalVarMapMain.sh1 == 32)
                                            {
                                                obj2 = new TempADSB_AP();
                                                obj2.Coordinates = new Coord();
                                                obj2.Coordinates.Latitude = 53.74768;
                                                obj2.Coordinates.Longitude = 25.592;
                                                obj2.Id = 1;
                                                obj2.ICAO = Convert.ToString(GlobalVarMapMain.sh1);
                                                double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                        (
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LatitudePrev,
                                                         mapCtrl.objClassInterfaceMap.List_AP[0].LongitudePrev,
                                                         obj2.Coordinates.Latitude,
                                                         obj2.Coordinates.Longitude
                                                        );
                                                obj2.Azimuth = az;
                                                obj2.LatitudePrev = obj2.Coordinates.Latitude;
                                                obj2.LongitudePrev = obj2.Coordinates.Longitude;
                                                lstap.Add(obj2);
                                                mapCtrl.objClassInterfaceMap.List_AP = lstap;
                                                GlobalVarMapMain.sh1 += 1;
                                            }


                                            else
                                            {
                                            }

                                            mapCtrl.objClassInterfaceMap.Show_AP = true;
                                            mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                    */

        // ..............................................................................
        // Azimuth
        // Old -> in P/P

        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>        
        /*
                List<TempADSB_AP> Lstap = new List<TempADSB_AP>();
                                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>        
                                            // 1-й сеанс приема

                                              if(mapCtrl.objClassInterfaceMap.List_AP.Count==0)
                                              {
                                                for (int i = 0; i<e.Table.Count; i++)
                                                {
                                                    TempADSB_AP obj = new TempADSB_AP();
                                                    obj.Coordinates = new Coord();
                                                    obj.Id = e.Table[i].Id;
                                                    obj.ICAO = e.Table[i].ICAO;
                                                    obj.Coordinates.Latitude = e.Table[i].Coordinates.Latitude;
                                                    obj.Coordinates.Longitude = e.Table[i].Coordinates.Longitude;
                                                    obj.LatitudePrev = e.Table[i].Coordinates.Latitude;
                                                    obj.LongitudePrev = e.Table[i].Coordinates.Longitude;
                                                    obj.Azimuth = 0;
                                                    obj.AzimuthPrev = 0;
                                                    obj.fdraw = true;
                                                    mapCtrl.objClassInterfaceMap.List_AP.Add(obj);
                                                } // For
                                                  // --------------------------------------------------------------
                                                  // Draw

                                                if (
                                                    (mapCtrl.objClassInterfaceMap.List_AP.Count != 0)
                                                    && (mapCtrl.objClassInterfaceMap.Show_AP == true)
                                                    )
                                                    mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                                                // --------------------------------------------------------------

                                                return;
                                              }
        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>        

        // FOR_e >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                                            // e.Table (i1)
                                            // FOR1
                                            int flagredraw = 0;
                                            for (int i1 = 0; i1<e.Table.Count; i1++)
                                            {

                                                // List_AP (j)
                                                // FOR2
                                              int flag = 0;
                                              for(int j=0;j<mapCtrl.objClassInterfaceMap.List_AP.Count;j++)
                                              {

                                               TempADSB_AP obj1 = new TempADSB_AP();
                                               obj1.Coordinates = new Coord();

                                                    // ..........................................................
                                                    // IP==

                                                    if (e.Table[i1].Id == mapCtrl.objClassInterfaceMap.List_AP[j].Id)
                                                    {
                                                        flag = 1;

                                                        if ((mapCtrl.objClassInterfaceMap.List_AP[j].LatitudePrev == -1) && (mapCtrl.objClassInterfaceMap.List_AP[j].LongitudePrev == -1))
                                                        {
                                                            obj1.Id = e.Table[i1].Id;
                                                            obj1.ICAO = e.Table[i1].ICAO;
                                                            obj1.Coordinates.Latitude = e.Table[i1].Coordinates.Latitude;
                                                            obj1.Coordinates.Longitude = e.Table[i1].Coordinates.Longitude;
                                                            obj1.LatitudePrev = e.Table[i1].Coordinates.Latitude;
                                                            obj1.LongitudePrev = e.Table[i1].Coordinates.Longitude;
                                                            obj1.Azimuth = 0;
                                                            obj1.AzimuthPrev = 0;
                                                            obj1.fdraw = true;
                                                            //lstap.Add(obj1);

                                                            flagredraw = 1;

                                                         } // LatitudePrev,LongitudePrev==-1

                                                        // Azimuth
                                                        else
                                                        {
                                                            double az = mapCtrl.objClassInterfaceMap.CalcAzimuth
                                                                    (
                                                                     mapCtrl.objClassInterfaceMap.List_AP[j].LatitudePrev,
                                                                     mapCtrl.objClassInterfaceMap.List_AP[j].LongitudePrev,
                                                                     e.Table[i1].Coordinates.Latitude,
                                                                     e.Table[i1].Coordinates.Longitude
                                                                    );

                                                            obj1.Id = e.Table[i1].Id;
                                                            obj1.ICAO = e.Table[i1].ICAO;
                                                            obj1.Coordinates.Latitude = e.Table[i1].Coordinates.Latitude;
                                                            obj1.Coordinates.Longitude = e.Table[i1].Coordinates.Longitude;
                                                            obj1.LatitudePrev = e.Table[i1].Coordinates.Latitude;
                                                            obj1.LongitudePrev = e.Table[i1].Coordinates.Longitude;
                                                            obj1.Azimuth = az;
                                                            if (az != 0)
                                                            {
                                                              obj1.fdraw = true;
                                                              obj1.AzimuthPrev = az;
                                                            }
                                                           else
                                                           {
                                                            obj1.fdraw = false;
                                                            obj1.AzimuthPrev = mapCtrl.objClassInterfaceMap.List_AP[j].AzimuthPrev;
                                                            }
                                                            //lstap.Add(obj1);

                                                            if (az != 0)
                                                                flagredraw = 1;

                                                        } // Azimuth

                                                        // Выход из внутреннего цикла
                                                        j = mapCtrl.objClassInterfaceMap.List_AP.Count + 1;

                                                    } // IP==
                                                    // ..........................................................
                                                    // IP!= -> идем дальше

                                                    else
                                                    {
                                                    } //IP!=
                                                    // ..........................................................

                                                } // for2 (j)

                                               // не нашли ID -> новый
                                                if(flag==0)
                                                {
                                                    TempADSB_AP Obj2 = new TempADSB_AP();
                                                    Obj2.Coordinates = new Coord();
                                                    Obj2.Id = e.Table[i1].Id;
                                                    Obj2.ICAO = e.Table[i1].ICAO;
                                                    Obj2.Coordinates.Latitude = e.Table[i1].Coordinates.Latitude;
                                                    Obj2.Coordinates.Longitude = e.Table[i1].Coordinates.Longitude;
                                                    Obj2.LatitudePrev = e.Table[i1].Coordinates.Latitude;
                                                    Obj2.LongitudePrev = e.Table[i1].Coordinates.Longitude;
                                                    Obj2.Azimuth = 0;
                                                    Obj2.Azimuth = 0;
                                                    Obj2.AzimuthPrev = 0;
                                                    Obj2.fdraw = true;
                                                    //lstap.Add(Obj2);

                                                    flagredraw = 1;
                                                }


                                            } // For1 (i1)
                                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> FOR_e

                                            objClassFunctionsMain.ClearListAP();
                                           // mapCtrl.objClassInterfaceMap.List_AP = lstap;


                                            // Draw

                                            if (
                                                (mapCtrl.objClassInterfaceMap.List_AP.Count != 0)
                                                && (mapCtrl.objClassInterfaceMap.Show_AP == true)&&
                                                (flagredraw==1)
                                                )
                                                mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
          */
        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>        

        // ..............................................................................

        // ------------------------------------------------------------------- OnTempADSB


        // ************************************************************* MainWindow.xaml.cs

        // UserControl1.xaml.cs ***********************************************************

        // MapControl_MapMouseMoveEvent ---------------------------------------------------

        // Mapping

        /*
                            // ...................................................................................................
                            // 19.07 GNSS

                            long ichislo1 = 0;
                            double dchislo1 = 0;
                            long ichislo2 = 0;
                            double dchislo2 = 0;
                            long ichislo3 = 0;
                            double dchislo3 = 0;

                            // Latitude
                            ichislo1 = (long)(GlobalVarMap.LAT_Rastr * 1000000);
                            dchislo1 = ((double)ichislo1) / 1000000;
                            // Longitude
                            ichislo2 = (long)(GlobalVarMap.LONG_Rastr * 1000000);
                            dchislo2 = ((double)ichislo2) / 1000000;
                            // Altitude
                            ichislo3 = (long)(GlobalVarMap.H_Rastr * 1000000);
                            dchislo3 = ((double)ichislo3) / 1000000;
                            Txt5.Text = Convert.ToString(dchislo3) + " m";
                            // ...................................................................................................
                            // WGS 84
                            if (initMapYaml.SysCoord == 0)
                            {
                                Txt1.Text = "N " + Convert.ToString(dchislo1) + '\xB0';
                                Txt3.Text = "W " + Convert.ToString(dchislo2) + '\xB0';

                            } // WGS84
                              // SK42,m
                            else if (initMapYaml.SysCoord == 2)
                            {
                                double xxx = 0;
                                double yyy = 0;
                                ClassGeoCalculator.f_WGS84_Krug(GlobalVarMap.LAT_Rastr,
                                                                GlobalVarMap.LONG_Rastr,
                                                                ref xxx, ref yyy);
                                Txt1.Text = "X " + Convert.ToString((int)xxx) + " m";
                                Txt3.Text = "Y " + Convert.ToString((int)yyy) + " m";
                            } // SK42,m
                            // ...................................................................................................

                            // -------------------------------------------------------------------------------------------------
                            try
                            {
                                GlobalVarMap.Str_Rastr = ClassGeoCalculator.f_WGS84_MGRS(GlobalVarMap.LAT_Rastr, GlobalVarMap.LONG_Rastr);
                                Txt7.Text = "MGRS: " + GlobalVarMap.Str_Rastr;
                            }
                            catch
                            {
                                MessageBox.Show("Invalid coordinates");
                                return;
                            }
                            // -------------------------------------------------------------------------------------------------
        */

        // ...................................................................................................
        // 19.07 GNSS
        /*
                            long ichislo1_1 = 0;
                            double dchislo1_1 = 0;
                            long ichislo2_1 = 0;
                            double dchislo2_1 = 0;
                            long ichislo3_1 = 0;
                            double dchislo3_1 = 0;

                            // Latitude
                            ichislo1_1 = (long)(GlobalVarMap.LAT_Rastr * 1000000);
                            dchislo1_1 = ((double)ichislo1_1) / 1000000;
                            // Longitude
                            ichislo2_1 = (long)(GlobalVarMap.LONG_Rastr * 1000000);
                            dchislo2_1 = ((double)ichislo2_1) / 1000000;
                            // Altitude
                            ichislo3_1 = (long)(GlobalVarMap.H_Rastr * 1000000);
                            dchislo3_1 = ((double)ichislo3_1) / 1000000;
                            Txt5.Text = Convert.ToString(dchislo3_1) + " m";
                            // ...................................................................................................
                            // WGS 84
                            if (initMapYaml.SysCoord == 0)
                            {
                                Txt1.Text = "N " + Convert.ToString(dchislo1_1) + '\xB0';
                                Txt3.Text = "W " + Convert.ToString(dchislo2_1) + '\xB0';

                            } // WGS84
                              // SK42,m
                            else if (initMapYaml.SysCoord == 2)
                            {
                                double xxx1 = 0;
                                double yyy1 = 0;
                                ClassGeoCalculator.f_WGS84_Krug(GlobalVarMap.LAT_Rastr,
                                                                GlobalVarMap.LONG_Rastr,
                                                                ref xxx1, ref yyy1);
                                Txt1.Text = "X " + Convert.ToString((int)xxx1) + " m";
                                Txt3.Text = "Y " + Convert.ToString((int)yyy1) + " m";
                            } // SK42,m
                            // ...................................................................................................

                            // -------------------------------------------------------------------------------------------------
                            GlobalVarMap.Str_Rastr = (string)e.ToMgrs().ToLongString();
                            Txt7.Text = string.Format("    MGRS: {0}", e.ToMgrs().ToLongString());
                            // -------------------------------------------------------------------------------------------------
                            */


        // --------------------------------------------------- MapControl_MapMouseMoveEvent

        // checkBoxJS_Checked -------------------------------------------------------------

        // Otl ---------------------------------------------------------------------------------------------
        //TableASP obj;
        //List<TableASP> listJS = new List<TableASP>();

        /* 
         // emirates
         obj = new TableASP();
         obj.Coordinates.Latitude = 23.181;
         obj.Coordinates.Longitude = 52.908;
         listJS.Add(obj);

         obj = new TableASP();
         obj.Coordinates.Latitude = 23.21;
         obj.Coordinates.Longitude = 52.939;
         listJS.Add(obj);

         obj = new TableASP();
         obj.Coordinates.Latitude = 23.139;
         obj.Coordinates.Longitude = 52.854;
         listJS.Add(obj);

         obj = new TableASP();
         obj.Coordinates.Latitude = 23.11;
         obj.Coordinates.Longitude = 52.857;
         listJS.Add(obj);
         */

        /*
        // Belarus
        obj = new TableASP();
        obj.Coordinates.Latitude = 53.889034;
        obj.Coordinates.Longitude = 27.514555;
        listJS.Add(obj);

        obj = new TableASP();
        obj.Coordinates.Latitude = 53.912895;
        obj.Coordinates.Longitude = 27.538245;
        listJS.Add(obj);

        obj = new TableASP();
        obj.Coordinates.Latitude = 53.936069;
        obj.Coordinates.Longitude = 27.568285;
        listJS.Add(obj);

        obj = new TableASP();
        obj.Coordinates.Latitude = 53.932464;
        obj.Coordinates.Longitude = 27.618926;
        listJS.Add(obj);
*/
        // objClassInterfaceMap.List_JS = listJS;
        // ---------------------------------------------------------------------------------------------- Otl

        // Otl ---------------------------------------------------------------------------------------------
        // Polygon
        /*
                    List<Mapsui.Geometries.Point> pointPel = new List<Mapsui.Geometries.Point>();

                    double lat = 0;
                    double lon = 0;

                    lat = 23.181;
                    lon = 52.908;
                    pointPel.Add(new Mapsui.Geometries.Point(lon, lat));

                    lat = 23.21;
                    lon = 52.939;
                    pointPel.Add(new Mapsui.Geometries.Point(lon, lat));

                    lat = 23.139;
                    lon = 52.854;
                    pointPel.Add(new Mapsui.Geometries.Point(lon, lat));

                    lat = 23.11;
                    lon = 52.857;
                    pointPel.Add(new Mapsui.Geometries.Point(lon, lat));


                    string s=objClassInterfaceMap.DrawPolygonLatLong(pointPel, 100, 0, 255, 0);
                    if (s != "")
                        MessageBox.Show(s);
        */
        // ---------------------------------------------------------------------------------------------- Otl

        // Otl ---------------------------------------------------------------------------------------------
        // Sector (Circle)
        /*            
                                double lat = 0;
                                double lon = 0;
                                lat = 23.181;
                                lon = 52.908;
                                Mapsui.Geometries.Point pointPel = new Mapsui.Geometries.Point(lon, lat);

                    string s = objClassInterfaceMap.DrawSectorLatLong(
                                               // Degree
                                               pointPel,
                                               // Color
                                               100,
                                               0,
                                               255,
                                               0,
                                               10000,
                                               // degree
                                               0,
                                               20
                                               );

                                if (s != "")
                                    MessageBox.Show(s);
        */
        // ---------------------------------------------------------------------------------------------- Otl

        // ------------------------------------------------------------- checkBoxJS_Checked

        // checkBoxObj1_Checked ------------------------------------------------------------

        // Otl ---------------------------------------------------------------------------------------------
        /*
                    TempFWS obj;
                    List<TempFWS> list_SRW_FRF = new List<TempFWS>();

                    Random rand = new Random();
                    Random rand1 = new Random();
                    int j = 0;
                    int j1 = 0;
                    int f = 50;
                    double lt_temp =0;
                    double ln_temp = 0;

                    for (j1 = 0; j1 < 10; j1++)
                    {
                        for (j = 0; j < 5; j++)
                        {
                            lt_temp = Convert.ToDouble(rand.Next(GlobalVarMap.lat1_temp, GlobalVarMap.lat2_temp)) / 100000;
                            ln_temp = Convert.ToDouble(rand1.Next(GlobalVarMap.lon1_temp, GlobalVarMap.lon2_temp)) / 100000;
                            obj = new TempFWS();
                            obj.Coordinates.Latitude = lt_temp;
                            obj.Coordinates.Longitude = ln_temp;
                            obj.FreqKHz = f;
                            list_SRW_FRF.Add(obj);
                        }
                        if (j1 < 4)
                            f += 50;
                        else
                            f += 250;
                    }

                    objClassInterfaceMap.List_SRW_FRF = list_SRW_FRF;
        */
        // ---------------------------------------------------------------------------------------------- Otl

        // ------------------------------------------------------------ checkBoxObj1_Checked

        // checkBoxObj2_Checked ------------------------------------------------------------

        // Otl ---------------------------------------------------------------------------------------------
        /*
                    TableReconFWS obj;
                    List<TableReconFWS> list_SRW_FRF_TD = new List<TableReconFWS>();

                    Random rand = new Random();
                    Random rand1 = new Random();
                    int j = 0;
                    int j1 = 0;
                    int f = 50;
                    double lt_temp = 0;
                    double ln_temp = 0;

                    for (j1 = 0; j1 < 10; j1++)
                    {
                        for (j = 0; j < 5; j++)
                        {
                            lt_temp = Convert.ToDouble(rand.Next(GlobalVarMap.lat1_temp, GlobalVarMap.lat2_temp)) / 100000;
                            ln_temp = Convert.ToDouble(rand1.Next(GlobalVarMap.lon1_temp, GlobalVarMap.lon2_temp)) / 100000;
                            obj = new TableReconFWS();
                            obj.Coordinates.Latitude = lt_temp;
                            obj.Coordinates.Longitude = ln_temp;
                            obj.FreqKHz = f;
                            list_SRW_FRF_TD.Add(obj);
                        }
                        if (j1 < 4)
                            f += 50;
                        else
                            f += 250;
                    }

                    objClassInterfaceMap.List_SRW_FRF_TD = list_SRW_FRF_TD;
        */
        // ---------------------------------------------------------------------------------------------- Otl


        // ------------------------------------------------------------ checkBoxObj2_Checked

        // checkBoxObj3_Checked ------------------------------------------------------------

        // Otl ---------------------------------------------------------------------------------------------
        /*
                    TableSuppressFWS obj;
                    List<TableSuppressFWS> list_SRW_FRF_RS = new List<TableSuppressFWS>();

                    Random rand = new Random();
                    Random rand1 = new Random();
                    int j = 0;
                    int j1 = 0;
                    int f = 50;
                    double lt_temp = 0;
                    double ln_temp = 0;

                    for (j1 = 0; j1 < 10; j1++)
                    {
                        for (j = 0; j < 5; j++)
                        {
                            lt_temp = Convert.ToDouble(rand.Next(GlobalVarMap.lat1_temp, GlobalVarMap.lat2_temp)) / 100000;
                            ln_temp = Convert.ToDouble(rand1.Next(GlobalVarMap.lon1_temp, GlobalVarMap.lon2_temp)) / 100000;
                            obj = new TableSuppressFWS();
                            obj.Coordinates.Latitude = lt_temp;
                            obj.Coordinates.Longitude = ln_temp;
                            obj.FreqKHz = f;
                            list_SRW_FRF_RS.Add(obj);
                        }
                        if (j1 < 4)
                            f += 50;
                        else
                            f += 250;
                    }

                    objClassInterfaceMap.List_SRW_FRF_RS = list_SRW_FRF_RS;
        */
        // ---------------------------------------------------------------------------------------------- Otl


        // ------------------------------------------------------------ checkBoxObj3_Checked

        // checkBoxObj4_Checked ----------------------------------------------------------------------------

        // Otl ---------------------------------------------------------------------------------------------
        // Другой тип вписать

        /*
                TableReconFHSS obj;
                List<TableReconFHSS> list_SRW_STRF = new List<TableReconFHSS>();

                Random rand = new Random();
                Random rand1 = new Random();
                int j = 0;
                int j1 = 0;
                int f = 50;
                double lt_temp = 0;
                double ln_temp = 0;

                    for (j1 = 0; j1< 10; j1++)
                    {
                        for (j = 0; j< 5; j++)
                        {
                            lt_temp = Convert.ToDouble(rand.Next(GlobalVarMap.lat1_temp, GlobalVarMap.lat2_temp)) / 100000;
                            ln_temp = Convert.ToDouble(rand1.Next(GlobalVarMap.lon1_temp, GlobalVarMap.lon2_temp)) / 100000;
                            obj.Coordinates = new Coord();
                            obj.Coordinates.Latitude = lt_temp;
                            obj.Coordinates.Longitude = ln_temp;
                            obj.FreqKHz = f;
                            list_SRW_STRF.Add(obj);
                        }
                        if (j1< 4)
                            f += 50;
                        else
                            f += 250;
                    }

                  objClassInterfaceMap.List_SRW_STRF = list_SRW_STRF;
        */
        // ---------------------------------------------------------------------------------------------- Otl

        // ---------------------------------------------------------------------------- checkBoxObj4_Checked

        // checkBoxAirplanes_Checked -----------------------------------------------------------------------
        // Otl ---------------------------------------------------------------------------------------------
        /*
                                TempADSB_AP obj;
                                List<TempADSB_AP> list_AP = new List<TempADSB_AP>();

                                obj = new TempADSB_AP();
                                obj.Coordinates = new Coord();
                                obj.Coordinates = new Coord();
                                obj.Coordinates.Latitude = 53.889034;
                                obj.Coordinates.Longitude = 27.514555;
                                list_AP.Add(obj);


                                obj = new TempADSB_AP();
                                obj.Coordinates = new Coord();
                                obj.Coordinates.Latitude = 53.889034;
                                obj.Coordinates.Longitude = 27.514555;
                                list_AP.Add(obj);

                                obj = new TempADSB_AP();
                                obj.Coordinates = new Coord();
                                obj.Coordinates.Latitude = 53.912895;
                                obj.Coordinates.Longitude = 27.538245;
                                list_AP.Add(obj);

                                obj = new TempADSB_AP();
                                obj.Coordinates = new Coord();
                                obj.Coordinates.Latitude = 53.936069;
                                obj.Coordinates.Longitude = 27.568285;
                                list_AP.Add(obj);

                                obj = new TempADSB_AP();
                                obj.Coordinates = new Coord();
                                 obj.Coordinates.Latitude = 53.932464;
                                obj.Coordinates.Longitude = 27.618926;
                                list_AP.Add(obj);

                                objClassInterfaceMap.List_AP = list_AP;
        */
        // ---------------------------------------------------------------------------------------------- Otl

        // ----------------------------------------------------------------------- checkBoxAirplanes_Checked



        // *********************************************************** UserControl1.xaml.cs

    }
}
