﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WpfMapRastrControl;
using ModelsTablesDBLib;
using ClientDataBase;
using Bearing;
using GeoCalculator;

// Для YAML реализации
using YamlDotNet.Serialization;
using ClassLibraryIniFiles;

using AzimutTask;


using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

using System.Windows.Threading;
using System.Threading.Tasks;



namespace WpfMapRastr
{
    public class ClassFunctionsMain
    {
        // ClearLists ********************************************************************************************

        public void ClearLists()
        {
            int i = 0;

            ClearListJS();
            ClearListAP();
            ClearListFRH();
            ClearListFRHCR();
            ClearListFRHRP();
            ClearListPPRH();
            ClearListPPRH1();
            ClearListPPRH2();
            ClearListPPRHRP();

        } // ClearLists

        // ---------------------------------------------------------------------------------------------------------
        public void ClearListJS()
        {
            int i = 0;
            // JS
            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS.Count > 0)
            {
                for (i = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS.Count - 1; i >= 0; i--)
                {
                    try
                    {
                        GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS.RemoveAt(i);
                    }
                    catch { }
                }
            }
        } // ClearListJS
        // ---------------------------------------------------------------------------------------------------------

        // 11_13
        public void ClearListAP()
        {
            int i = 0;
            // AirPlanes
            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Count > 0)
            {
                for (i = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Count - 1; i >= 0; i--)
                {   try
                    {
                        GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.RemoveAt(i);
                    }
                    catch {
                       // MessageBox.Show("Error26");
                    }
                }
            }
            GlobalVarMapMain.fl_AirPlane = 0;

            GlobalVarMapMain.flairone = false;
            GlobalVarMapMain.ICAO_LAST = "";

        } // ClearListAP
        // ---------------------------------------------------------------------------------------------------------

        public void ClearListFRH()
        {
            int i = 0;
            // ИРИ ФРЧ
            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count>0)
            {
                for (i = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count - 1; i >= 0; i--)
                {
                    try
                    {
                        GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.RemoveAt(i);
                    }
                    catch { }
                }
            }

        } // ClearListFRH
        // ---------------------------------------------------------------------------------------------------------

        public void ClearListFRHCR()
        {
            int i = 0;
            // ИРИ ФРЧ ЦР
            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.Count > 0)
            {
                for (i = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.Count - 1; i >= 0; i--)
                {
                    try
                    {
                        GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.RemoveAt(i);
                    }
                    catch { }
                }
            }

        } // ClearListFRHCRCR
        // ---------------------------------------------------------------------------------------------------------

        public void ClearListFRHRP()
        {
            int i = 0;
            // ИРИ ФРЧ РП
            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.Count > 0)
            {
                for (i = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.Count - 1; i >= 0; i--)
                {
                    try
                    {
                        GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.RemoveAt(i);
                    }
                    catch { }
                }
            }

        } // ClearListFRHRP
        // ---------------------------------------------------------------------------------------------------------

        public void ClearListPPRH()
        {
            int i = 0;
            int i1 = 0;
            // ИРИ ППРЧ
            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF.Count > 0)
            {
                for (i = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF.Count - 1; i >= 0; i--)
                {
                    try
                    {
                        GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF.RemoveAt(i);
                    }
                    catch { }
                }
            }
        } // ClearListPPRH

        public void ClearListPPRH1()
        {
            int i1 = 0;
            
            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord.Count > 0)
             {
              for (i1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord.Count - 1; i1 >= 0; i1--)
                 {
                    try
                    {
                        GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord.RemoveAt(i1);
                    }
                    catch { }
                 }
             }
            
        } // ClearListPPRH1

        public void ClearListPPRH2()
        {
            int i1 = 0;

            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF_Recon.Count > 0)
            {
                for (i1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF_Recon.Count - 1; i1 >= 0; i1--)
                {
                    try
                    {
                        GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF_Recon.RemoveAt(i1);
                    }
                    catch { }
                }
            }

        } // ClearListPPRH2

        // ---------------------------------------------------------------------------------------------------------

        public void ClearListPPRHRP()
        {
            int i = 0;
            // ИРИ ППРЧ РП
            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF_RS.Count > 0)
            {
                for (i = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF_RS.Count - 1; i >= 0; i--)
                {
                    try
                    {
                        GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF_RS.RemoveAt(i);
                    }
                    catch { }
                }
            }

        } // ClearListPPRHRP
        // ---------------------------------------------------------------------------------------------------------

        // ******************************************************************************************** ClearLists

        // ClearAzimuth *****************************************************************************************
        // Azimuth*

        public void ClearAzimuth()
        {
            // -------------------------------------------------------------------------------------------------
            // INI

            string s = "";
            InitMapYaml initMapYaml = new InitMapYaml();
            s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
            // -------------------------------------------------------------------------------------------------
            // Очистка окошек в контроле

            // mm
            if ((initMapYaml.SysCoord == 2) || (initMapYaml.SysCoord == 3))
            {
                AzimutTask.Models.CoordXY coordXY = new AzimutTask.Models.CoordXY();
                coordXY.Altitude = 0;
                coordXY.X = 0;
                coordXY.Y = 0;
                GlobalVarMapMain.objMainWindowG.AzimutTaskCntr.SetModel(coordXY);
            }
            else
            {
              if(initMapYaml.FormatCoord==0)
               {
                    AzimutTask.Models.CoordDegree coordDegree = new AzimutTask.Models.CoordDegree();
                    coordDegree.Altitude = 0;
                    coordDegree.Latitude = 0;
                    coordDegree.Longitude = 0;
                    GlobalVarMapMain.objMainWindowG.AzimutTaskCntr.SetModel(coordDegree);
                }
                else
                {
                    AzimutTask.Models.CoordDdMmSs coordDdMmSs = new AzimutTask.Models.CoordDdMmSs();
                    coordDdMmSs.Altitude = 0;
                    coordDdMmSs.Latitude.Degree = 0;
                    coordDdMmSs.Latitude.Minutes = 0;
                    coordDdMmSs.Latitude.Seconds = 0;
                    coordDdMmSs.Longitude.Degree = 0;
                    coordDdMmSs.Longitude.Minutes = 0;
                    coordDdMmSs.Longitude.Seconds = 0;
                    GlobalVarMapMain.objMainWindowG.AzimutTaskCntr.SetModel(coordDdMmSs);
                }

            } // grad

            GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LatSource_Azimuth = -1;
            GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LongSource_Azimuth = -1;
            GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.HSource_Azimuth = -1;
            // -------------------------------------------------------------------------------------------------
            // FOR JS

            for (int i = 0; i < GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS.Count; i++)
            {
                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[i].Azimuth= -1;
            }
           // -------------------------------------------------------------------------------------------------

          // Clear and draw other objects
         GlobalVarMapMain.objMainWindowG.mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();

        }
        // ***************************************************************************************** ClearAzimuth

        // DrawCurrentCoordinatesAzimuth ************************************************************************
        // Azimuth*
        // Нарисовать текущие координаты источника в зависимости от установленной в ini файле СК и 
        // формата координат в окошках Таниного контрола вкладки азимута

        public void DrawCurrentCoordinatesAzimuth()
        {
            // -------------------------------------------------------------------------------------------------
            long ichislo1 = 0;
            double dchislo1 = 0;
            long ichislo2 = 0;
            double dchislo2 = 0;
            long ichislo3 = 0;
            double dchislo3 = 0;

            int ilatgrad = 0;
            double dlatmin = 0;
            int ilatmin = 0;
            long Llatmin = 0;
            double dlatsec = 0;
            int ilatsec = 0;
            long Llatsec = 0;

            int ilongrad = 0;
            double dlonmin = 0;
            int ilonmin = 0;
            long Llonmin = 0;
            double dlonsec = 0;
            int ilonsec = 0;
            long Llonsec = 0;

            // -------------------------------------------------------------------------------------------------
            // INI

            string s = "";
            InitMapYaml initMapYaml = new InitMapYaml();
            s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
            // -------------------------------------------------------------------------------------------------
            // WGS 84

            // Latitude
            ichislo1 = (long)(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LatSource_Azimuth * 1000000);
            dchislo1 = ((double)ichislo1) / 1000000;
            // Longitude
            ichislo2 = (long)(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LongSource_Azimuth * 1000000);
            dchislo2 = ((double)ichislo2) / 1000000;
            // Altitude
            ichislo3 = (long)(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.HSource_Azimuth * 1000000);
            dchislo3 = ((double)ichislo3) / 1000000;


            if (dchislo3 == -1) dchislo3 = 0;
            if (dchislo1 == -1) dchislo1 = 0;
            if (dchislo2 == -1) dchislo2 = 0;


            // !!! Вписать в окошко высоту
            // ??????????????????????????????
            //Txt5.Text = Convert.ToString(dchislo3) + " m";
            // ...................................................................................................
            // WGS 84

            if (initMapYaml.SysCoord == 0)
            {
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD.dddddd

                if (initMapYaml.FormatCoord == 0)
                {
                    // !!! Вписать в окошки
                    AzimutTask.Models.CoordDegree coordDegree = new AzimutTask.Models.CoordDegree();
                    coordDegree.Altitude = dchislo3;
                    coordDegree.Latitude = dchislo1;
                    coordDegree.Longitude = dchislo2;
                    GlobalVarMapMain.objMainWindowG.AzimutTaskCntr.SetModel(coordDegree);
                }
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD MM.mmmm

                else if (initMapYaml.FormatCoord == 1)
                {
                    // Lat
                    ClassGeoCalculator.f_Grad_GM(
                                                  dchislo1,
                                                  ref ilatgrad,
                                                  ref dlatmin
                                                );
                    Llatmin = (long)(dlatmin * 10000);
                    dlatmin = ((double)Llatmin) / 10000;

                    // Long
                    ClassGeoCalculator.f_Grad_GM(
                                                  dchislo2,
                                                  ref ilongrad,
                                                  ref dlonmin
                                                );
                    Llonmin = (long)(dlonmin * 10000);
                    dlonmin = ((double)Llonmin) / 10000;

                    // !!! Вписать в окошки
                    AzimutTask.Models.CoordDdMmSs coordDdMmSs = new AzimutTask.Models.CoordDdMmSs();
                    coordDdMmSs.Altitude = dchislo3;
                    coordDdMmSs.Latitude.Degree = ilatgrad;
                    coordDdMmSs.Latitude.Minutes = dlatmin;
                    coordDdMmSs.Latitude.Seconds = 0;
                    coordDdMmSs.Longitude.Degree = ilongrad;
                    coordDdMmSs.Longitude.Minutes = ilonmin;
                    coordDdMmSs.Longitude.Seconds = 0;
                    GlobalVarMapMain.objMainWindowG.AzimutTaskCntr.SetModel(coordDdMmSs);

                } // DD MM.mmmm
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD MM SS.ss

                else
                {
                    // Lat
                    ClassGeoCalculator.f_Grad_GMS(
                                                  dchislo1,
                                                  ref ilatgrad,
                                                  ref ilatmin,
                                                  ref dlatsec
                                                );
                    Llatsec = (long)(dlatsec * 100);
                    dlatsec = ((double)Llatsec) / 100;

                    // Long
                    ClassGeoCalculator.f_Grad_GMS(
                                                  dchislo2,
                                                  ref ilongrad,
                                                  ref ilonmin,
                                                  ref dlonsec
                                                );
                    Llonsec = (long)(dlonsec * 100);
                    dlonsec = ((double)Llonsec) / 100;

                    // !!! Вписать в окошки
                    AzimutTask.Models.CoordDdMmSs coordDdMmSs = new AzimutTask.Models.CoordDdMmSs();
                    coordDdMmSs.Altitude = dchislo3;
                    coordDdMmSs.Latitude.Degree = ilatgrad;
                    coordDdMmSs.Latitude.Minutes = dlatmin;
                    coordDdMmSs.Latitude.Seconds = dlatsec;
                    coordDdMmSs.Longitude.Degree = ilongrad;
                    coordDdMmSs.Longitude.Minutes = ilonmin;
                    coordDdMmSs.Longitude.Seconds = dlonsec;
                    GlobalVarMapMain.objMainWindowG.AzimutTaskCntr.SetModel(coordDdMmSs);

                } // DD MM SS.ss
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            } // WGS84
              // ...................................................................................................
              // SK42,m

            else if (initMapYaml.SysCoord == 2)
            {
                double xxx = 0;
                double yyy = 0;
                ClassGeoCalculator.f_WGS84_Krug(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LatSource_Azimuth,
                                                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LongSource_Azimuth,
                                                ref xxx, ref yyy);
                // !!! Вписать в окошки
                AzimutTask.Models.CoordXY coordXY = new AzimutTask.Models.CoordXY();
                coordXY.Altitude = dchislo3;

                if((GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LatSource_Azimuth==-1) ||(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LongSource_Azimuth==-1))
                {
                    xxx = 0;
                    yyy = 0;
                }

                coordXY.X = xxx;
                coordXY.Y = yyy;
                GlobalVarMapMain.objMainWindowG.AzimutTaskCntr.SetModel(coordXY);

            } // SK42,m
              // ...................................................................................................
              // SK42,grad

            else if (initMapYaml.SysCoord == 1)
            {
                double xxx = 0;
                double yyy = 0;
                ClassGeoCalculator.f_WGS84_SK42_BL(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LatSource_Azimuth,
                                                   GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LongSource_Azimuth,
                                                   ref xxx, ref yyy);

                if ((GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LatSource_Azimuth == -1) || (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LongSource_Azimuth == -1))
                {
                    xxx = 0;
                    yyy = 0;
                }

                // Latitude
                ichislo1 = (long)(xxx * 1000000);
                dchislo1 = ((double)ichislo1) / 1000000;
                // Longitude
                ichislo2 = (long)(yyy * 1000000);
                dchislo2 = ((double)ichislo2) / 1000000;

                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD.dddddd

                if (initMapYaml.FormatCoord == 0)
                {
                    // !!! Вписать в окошки
                    AzimutTask.Models.CoordDegree coordDegree = new AzimutTask.Models.CoordDegree();
                    coordDegree.Altitude = dchislo3;
                    coordDegree.Latitude = dchislo1;
                    coordDegree.Longitude = dchislo2;
                    GlobalVarMapMain.objMainWindowG.AzimutTaskCntr.SetModel(coordDegree);

                }
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD MM.mmmm

                else if (initMapYaml.FormatCoord == 1)
                {
                    // Lat
                    ClassGeoCalculator.f_Grad_GM(
                                                  dchislo1,
                                                  ref ilatgrad,
                                                  ref dlatmin
                                                );
                    Llatmin = (long)(dlatmin * 10000);
                    dlatmin = ((double)Llatmin) / 10000;

                    // Long
                    ClassGeoCalculator.f_Grad_GM(
                                                  dchislo2,
                                                  ref ilongrad,
                                                  ref dlonmin
                                                );
                    Llonmin = (long)(dlonmin * 10000);
                    dlonmin = ((double)Llonmin) / 10000;

                    // !!! Вписать в окошки
                    AzimutTask.Models.CoordDdMmSs coordDdMmSs = new AzimutTask.Models.CoordDdMmSs();
                    coordDdMmSs.Altitude = dchislo3;
                    coordDdMmSs.Latitude.Degree = ilatgrad;
                    coordDdMmSs.Latitude.Minutes = dlatmin;
                    coordDdMmSs.Latitude.Seconds = 0;
                    coordDdMmSs.Longitude.Degree = ilongrad;
                    coordDdMmSs.Longitude.Minutes = ilonmin;
                    coordDdMmSs.Longitude.Seconds = 0;
                    GlobalVarMapMain.objMainWindowG.AzimutTaskCntr.SetModel(coordDdMmSs);

                } // DD MM.mmmm
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD MM SS.ss

                else
                {
                    // Lat
                    ClassGeoCalculator.f_Grad_GMS(
                                                  dchislo1,
                                                  ref ilatgrad,
                                                  ref ilatmin,
                                                  ref dlatsec
                                                );
                    Llatsec = (long)(dlatsec * 100);
                    dlatsec = ((double)Llatsec) / 100;

                    // Long
                    ClassGeoCalculator.f_Grad_GMS(
                                                  dchislo2,
                                                  ref ilongrad,
                                                  ref ilonmin,
                                                  ref dlonsec
                                                );
                    Llonsec = (long)(dlonsec * 100);
                    dlonsec = ((double)Llonsec) / 100;

                    // !!! Вписать в окошки
                    AzimutTask.Models.CoordDdMmSs coordDdMmSs = new AzimutTask.Models.CoordDdMmSs();
                    coordDdMmSs.Altitude = dchislo3;
                    coordDdMmSs.Latitude.Degree = ilatgrad;
                    coordDdMmSs.Latitude.Minutes = dlatmin;
                    coordDdMmSs.Latitude.Seconds = dlatsec;
                    coordDdMmSs.Longitude.Degree = ilongrad;
                    coordDdMmSs.Longitude.Minutes = ilonmin;
                    coordDdMmSs.Longitude.Seconds = dlonsec;
                    GlobalVarMapMain.objMainWindowG.AzimutTaskCntr.SetModel(coordDdMmSs);

                } // DD MM SS.ss
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            } // SK42,grad
              // ...................................................................................................
              // Mercator

            else if (initMapYaml.SysCoord == 3)
            {
                double xxx = 0;
                double yyy = 0;
                ClassGeoCalculator.f_WGS84_Mercator(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LatSource_Azimuth,
                                                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LongSource_Azimuth,
                                                    ref xxx, ref yyy);

                // !!! Вписать в окошки
                AzimutTask.Models.CoordXY coordXY = new AzimutTask.Models.CoordXY();
                coordXY.Altitude = dchislo3;

                if ((GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LatSource_Azimuth == -1) || (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LongSource_Azimuth == -1))
                {
                    xxx = 0;
                    yyy = 0;
                }

                coordXY.X = xxx;
                coordXY.Y = yyy;
                GlobalVarMapMain.objMainWindowG.AzimutTaskCntr.SetModel(coordXY);


            } // Mercator
            // ...................................................................................................

        }
        // ************************************************************************* DrawCurrentCoordinatesAzimuth

        // ReadCurrentCoordinatesAzimuth *************************************************************************
        // Azimuth*
        // Считать текущие координаты источника в зависимости от установленной в ini файле СК и 
        // формата координат из окошек Таниного контрола вкладки азимута

        // !!! Должны заполнить:
        //     GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LatSource_Azimuth
        //     GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LongSource_Azimuth
        //     GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.HSource_Azimuth

        public void ReadCurrentCoordinatesAzimuth(
                                                  AzimutTask.Models.CoordXY modelXY,
                                                  AzimutTask.Models.CoordDegree modelDegree,
                                                  AzimutTask.Models.CoordDdMmSs modelDdMmSs
                                                 )
        {
            // -------------------------------------------------------------------------------------------------
            long ichislo1 = 0;
            double dchislo1 = 0;
            long ichislo2 = 0;
            double dchislo2 = 0;
            long ichislo3 = 0;
            double dchislo3 = 0;

            int ilatgrad = 0;
            double dlatmin = 0;
            int ilatmin = 0;
            long Llatmin = 0;
            double dlatsec = 0;
            int ilatsec = 0;
            long Llatsec = 0;

            int ilongrad = 0;
            double dlonmin = 0;
            int ilonmin = 0;
            long Llonmin = 0;
            double dlonsec = 0;
            int ilonsec = 0;
            long Llonsec = 0;

            // -------------------------------------------------------------------------------------------------
            // INI

            string s = "";
            InitMapYaml initMapYaml = new InitMapYaml();
            s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
            // -------------------------------------------------------------------------------------------------

            // .................................................................................................
            // WGS 84

            if (initMapYaml.SysCoord == 0)
            {
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD.dddddd

                if (initMapYaml.FormatCoord == 0)
                {
                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LatSource_Azimuth = modelDegree.Latitude;
                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LongSource_Azimuth = modelDegree.Longitude;
                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.HSource_Azimuth = modelDegree.Altitude;
                }
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD MM.mmmm

                else if (initMapYaml.FormatCoord == 1)
                {
                    // H
                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.HSource_Azimuth = modelDdMmSs.Altitude;

                    // Lat
                    ilatgrad = (int)modelDdMmSs.Latitude.Degree;
                    dlatmin = modelDdMmSs.Latitude.Minutes;
                    double dlat = 0;  
                    ClassGeoCalculator.f_GM_Grad(
                                                 ilatgrad,
                                                 dlatmin,
                                                 ref dlat
                                                );
                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LatSource_Azimuth = dlat;

                    // Long
                    ilongrad = (int)modelDdMmSs.Longitude.Degree;
                    dlonmin = modelDdMmSs.Longitude.Minutes;
                    double dlon = 0;  
                    ClassGeoCalculator.f_GM_Grad(
                                                 ilongrad,
                                                 dlonmin,
                                                 ref dlon
                                                );
                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LongSource_Azimuth = dlon;

                } // DD MM.mmmm
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD MM SS.ss

                else
                {
                    // H
                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.HSource_Azimuth = modelDdMmSs.Altitude;

                    // Lat
                    ilatgrad = (int)modelDdMmSs.Latitude.Degree;
                    ilatmin = (int)modelDdMmSs.Latitude.Minutes;
                    dlatsec = modelDdMmSs.Latitude.Seconds;
                    double dlat1 = 0;
                    ClassGeoCalculator.f_GMS_Grad(
                                                 ilatgrad,
                                                 ilatmin,
                                                 dlatsec,
                                                 ref dlat1
                                                );
                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LatSource_Azimuth = dlat1;

                    // Long
                    ilongrad = (int)modelDdMmSs.Longitude.Degree;
                    ilonmin = (int)modelDdMmSs.Longitude.Minutes;
                    dlonsec = modelDdMmSs.Longitude.Seconds;
                    double dlon1 = 0;
                    ClassGeoCalculator.f_GMS_Grad(
                                                 ilongrad,
                                                 ilonmin,
                                                 dlonsec,
                                                 ref dlon1
                                                );
                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LongSource_Azimuth = dlon1;

                } // DD MM SS.ss
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            } // WGS84
              // ...................................................................................................
              // SK42,m

            else if (initMapYaml.SysCoord == 2)
            {
                double xxx = 0;
                double yyy = 0;
                double lat2 = 0;
                double lon2 = 0;

                // H
                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.HSource_Azimuth = modelXY.Altitude;
                xxx = modelXY.X;
                yyy = modelXY.Y;
                ClassGeoCalculator.f_Krug_WGS84(
                                                xxx,
                                                yyy,
                                                ref lat2,
                                                ref lon2
                                               );

                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LatSource_Azimuth = lat2;
                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LongSource_Azimuth = lon2;

            } // SK42,m
              // ...................................................................................................
              // SK42,grad

            else if (initMapYaml.SysCoord == 1)
            {
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD.dddddd

                if (initMapYaml.FormatCoord == 0)
                {
                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.HSource_Azimuth = modelDegree.Altitude;

                    double lat3 = 0;
                    double lon3 = 0;
                    double lat3_1 = 0;
                    double lon3_1 = 0;

                    lat3 = modelDegree.Latitude;
                    lon3 = modelDegree.Longitude;
                    ClassGeoCalculator.f_SK42_WGS84_BL(
                                                       lat3,
                                                       lon3,
                                                       ref lat3_1,
                                                       ref lon3_1
                                                      );

                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LatSource_Azimuth = lat3_1;
                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LongSource_Azimuth = lon3_1;
               }
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD MM.mmmm

                else if (initMapYaml.FormatCoord == 1)
                {

                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.HSource_Azimuth = modelDdMmSs.Altitude;

                    // Lat
                    ilatgrad = (int)modelDdMmSs.Latitude.Degree;
                    dlatmin = modelDdMmSs.Latitude.Minutes;
                    double dlat5 = 0;
                    ClassGeoCalculator.f_GM_Grad(
                                                 ilatgrad,
                                                 dlatmin,
                                                 ref dlat5
                                                );
                    // Long
                    ilongrad = (int)modelDdMmSs.Longitude.Degree;
                    dlonmin = modelDdMmSs.Longitude.Minutes;
                    double dlon5 = 0;
                    ClassGeoCalculator.f_GM_Grad(
                                                 ilongrad,
                                                 dlonmin,
                                                 ref dlon5
                                                );

                    double dlat5_1 = 0;
                    double dlon5_1 = 0;
                    ClassGeoCalculator.f_SK42_WGS84_BL(
                                                       dlat5,
                                                       dlon5,
                                                       ref dlat5_1,
                                                       ref dlon5_1
                                                      );

                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LatSource_Azimuth = dlat5_1;
                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LongSource_Azimuth = dlon5_1;

                } // DD MM.mmmm
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD MM SS.ss

                else
                {
                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.HSource_Azimuth = modelDdMmSs.Altitude;

                    // Lat
                    ilatgrad = (int)modelDdMmSs.Latitude.Degree;
                    ilatmin = (int)modelDdMmSs.Latitude.Minutes;
                    dlatsec = modelDdMmSs.Latitude.Seconds;
                    double dlat6 = 0;
                    ClassGeoCalculator.f_GMS_Grad(
                                                 ilatgrad,
                                                 ilatmin,
                                                 dlatsec,
                                                 ref dlat6
                                                );
                    // Long
                    ilongrad = (int)modelDdMmSs.Longitude.Degree;
                    ilonmin = (int)modelDdMmSs.Longitude.Minutes;
                    dlonsec = modelDdMmSs.Longitude.Seconds;
                    double dlon6 = 0;
                    ClassGeoCalculator.f_GMS_Grad(
                                                 ilongrad,
                                                 ilonmin,
                                                 dlonsec,
                                                 ref dlon6
                                                );

                    double dlat6_1 = 0;
                    double dlon6_1 = 0;
                    ClassGeoCalculator.f_SK42_WGS84_BL(
                                                       dlat6,
                                                       dlon6,
                                                       ref dlat6_1,
                                                       ref dlon6_1
                                                      );

                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LatSource_Azimuth = dlat6_1;
                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LongSource_Azimuth = dlon6_1;

                } // DD MM SS.ss
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            } // SK42,grad
              // ...................................................................................................
              // Mercator

            else if (initMapYaml.SysCoord == 3)
            {
                double xxx = 0;
                double yyy = 0;
                double lat7 = 0;
                double lon7 = 0;

                // H
                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.HSource_Azimuth = modelXY.Altitude;
                xxx = modelXY.X;
                yyy = modelXY.Y;
                ClassGeoCalculator.f_Mercator_WGS84(
                                                xxx,
                                                yyy,
                                                ref lat7,
                                                ref lon7
                                               );

                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LatSource_Azimuth = lat7;
                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.LongSource_Azimuth = lon7;

            } // Mercator
            // ...................................................................................................

        }
        // ************************************************************************* ReadCurrentCoordinatesAzimuth

        // GetListAirplanes_LoadTabDB ****************************************************************************
        // Получение листа самолетов при подключении к БД

        public void GetListAirPlanes_LoadDB(List<TempADSB> lstapin)
        {
            // -------------------------------------------------------------------------------------------------------
            // При подключении к БД лист самолетов д.б. очищен

            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Count != 0)
            {
                ClearListAP();
            }
            // -------------------------------------------------------------------------------------------------------
            // !!! Перерисовка - в функции загрузки таблиц

            for (int i = 0; i < lstapin.Count; i++)
            {
                TempADSB_AP obj = new TempADSB_AP();
                obj.Coordinates = new Coord();
                obj.list_air_traj = new List<Objects>();
                Objects obj1 = new Objects();

                obj.Id = lstapin[i].Id;
                obj.ICAO = lstapin[i].ICAO;
                obj.Coordinates.Latitude = lstapin[i].Coordinates.Latitude;
                obj.Coordinates.Longitude = lstapin[i].Coordinates.Longitude;
                obj.LatitudePrev = lstapin[i].Coordinates.Latitude;
                obj.LongitudePrev = lstapin[i].Coordinates.Longitude;
                obj.Azimuth = 0;
                obj.AzimuthPrev = 0;
                obj.fdraw = true;

                // List trajectories
                obj1.Latitude = obj.Coordinates.Latitude;
                obj1.Longitude = obj.Coordinates.Longitude;
                obj1.ds = 0;
                obj.list_air_traj.Add(obj1);

                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Add(obj);

            } // For
            // -------------------------------------------------------------------------------------------------------

        }
        // **************************************************************************** GetListAirplanes_LoadTabDB

        // GetListAirplanesWithRotateOnAzimuth ******************************************************************

            public void GetListAirPlanes(List<TempADSB> lstapin)
            {
                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                // Azimuth

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>        
                List<TempADSB_AP> lstap = new List<TempADSB_AP>();
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>        
                // 1-й сеанс приема

                if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Count == 0)
                {
                    for (int i = 0; i < lstapin.Count; i++)
                    {
                        TempADSB_AP obj = new TempADSB_AP();
                        obj.Coordinates = new Coord();
                        obj.Id = lstapin[i].Id;
                        obj.ICAO = lstapin[i].ICAO;
                        obj.Coordinates.Latitude = lstapin[i].Coordinates.Latitude;
                        obj.Coordinates.Longitude = lstapin[i].Coordinates.Longitude;
                        obj.LatitudePrev = lstapin[i].Coordinates.Latitude;
                        obj.LongitudePrev = lstapin[i].Coordinates.Longitude;
                        obj.Azimuth = 0;
                        obj.AzimuthPrev = 0;
                        obj.fdraw = true;
                        GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Add(obj);
                    } // For
                      // --------------------------------------------------------------
                      // Draw

                    if (
                        (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Count != 0)
                        && (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.Show_AP == true)
                        )
                        GlobalVarMapMain.objMainWindowG.mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                    // --------------------------------------------------------------

                    return;
                }
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>        

                // FOR_e >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                // e.Table (i1)
                // FOR1
                int flagredraw = 0;
                for (int i1 = 0; i1 < lstapin.Count; i1++)
                {

                    // List_AP (j)
                    // FOR2
                    int flag = 0;
                    for (int j = 0; j < GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Count; j++)
                    {

                        TempADSB_AP obj1 = new TempADSB_AP();
                        obj1.Coordinates = new Coord();

                        // ..........................................................
                        // IP==

                        if (lstapin[i1].Id == GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[j].Id)
                        {
                            flag = 1;

                            if ((GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[j].LatitudePrev == -1) &&
                                (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[j].LongitudePrev == -1))
                            {
                                obj1.Id = lstapin[i1].Id;
                                obj1.ICAO = lstapin[i1].ICAO;
                                obj1.Coordinates.Latitude = lstapin[i1].Coordinates.Latitude;
                                obj1.Coordinates.Longitude = lstapin[i1].Coordinates.Longitude;
                                obj1.LatitudePrev = lstapin[i1].Coordinates.Latitude;
                                obj1.LongitudePrev = lstapin[i1].Coordinates.Longitude;
                                obj1.Azimuth = 0;
                                obj1.AzimuthPrev = 0;
                                obj1.fdraw = true;
                                lstap.Add(obj1);

                                flagredraw = 1;

                            } // LatitudePrev,LongitudePrev==-1

                            // Azimuth
                            else
                            {
                                double az = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.CalcAzimuth
                                        (
                                         GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[j].LatitudePrev,
                                         GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[j].LongitudePrev,
                                         lstapin[i1].Coordinates.Latitude,
                                         lstapin[i1].Coordinates.Longitude
                                        );

                                obj1.Id = lstapin[i1].Id;
                                obj1.ICAO = lstapin[i1].ICAO;
                                obj1.Coordinates.Latitude = lstapin[i1].Coordinates.Latitude;
                                obj1.Coordinates.Longitude = lstapin[i1].Coordinates.Longitude;
                                obj1.LatitudePrev = lstapin[i1].Coordinates.Latitude;
                                obj1.LongitudePrev = lstapin[i1].Coordinates.Longitude;
                                obj1.Azimuth = az;
                                if (az != 0)
                                {
                                    obj1.fdraw = true;
                                    obj1.AzimuthPrev = az;
                                }
                                else
                                {
                                    obj1.fdraw = false;
                                    obj1.AzimuthPrev = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[j].AzimuthPrev;
                                }
                                lstap.Add(obj1);

                                if (az != 0)
                                    flagredraw = 1;

                            } // Azimuth

                            // Выход из внутреннего цикла
                            j = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Count + 1;

                        } // IP==
                          // ..........................................................
                          // IP!= -> идем дальше

                        else
                        {
                        } //IP!=
                          // ..........................................................

                    } // for2 (j)

                    // не нашли ID -> новый
                    if (flag == 0)
                    {
                        TempADSB_AP obj2 = new TempADSB_AP();
                        obj2.Coordinates = new Coord();
                        obj2.Id = lstapin[i1].Id;
                        obj2.ICAO = lstapin[i1].ICAO;
                        obj2.Coordinates.Latitude = lstapin[i1].Coordinates.Latitude;
                        obj2.Coordinates.Longitude = lstapin[i1].Coordinates.Longitude;
                        obj2.LatitudePrev = lstapin[i1].Coordinates.Latitude;
                        obj2.LongitudePrev = lstapin[i1].Coordinates.Longitude;
                        obj2.Azimuth = 0;
                        obj2.Azimuth = 0;
                        obj2.AzimuthPrev = 0;
                        obj2.fdraw = true;
                        lstap.Add(obj2);

                        flagredraw = 1;
                    }

                } // For1 (i1)
                  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> FOR_e

                ClearListAP();
                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP = lstap;

                // Draw
                if (
                    (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Count != 0)
                    && (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.Show_AP == true) &&
                    (flagredraw == 1)
                    )
                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();

                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            }

        // ****************************************************************** GetListAirplanesWithRotateOnAzimuth



        // GetListAirplanesWithRotateOnAzimuth ******************************************************************
        // Добавить одну запись

        /*
                public void GetListAirPlanes(TempADSB lstapin)
                {

                                //List<TempADSB_AP> lstap = new List<TempADSB_AP>();

                                // В листе еще ничего не было >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>        
                                // 1-й сеанс приема

                                if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Count == 0)
                                {
                                        TempADSB_AP obj = new TempADSB_AP();
                                        obj.Coordinates = new Coord();
                                        obj.list_air_traj = new List<Objects>();
                                        Objects obj1 = new Objects();

                                        obj.Id = lstapin.Id;
                                        obj.ICAO = lstapin.ICAO;
                                        obj.Coordinates.Latitude = lstapin.Coordinates.Latitude;
                                        obj.Coordinates.Longitude = lstapin.Coordinates.Longitude;
                                        obj.LatitudePrev = lstapin.Coordinates.Latitude;
                                        obj.LongitudePrev = lstapin.Coordinates.Longitude;
                                        obj.Azimuth = 0;
                                        obj.AzimuthPrev = 0;
                                        obj.fdraw = true;

                                        // List trajectories
                                        obj1.Latitude = obj.Coordinates.Latitude;
                                        obj1.Longitude = obj.Coordinates.Longitude;
                                        obj1.ds = 0;
                                        obj.list_air_traj.Add(obj1);

                                        GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Add(obj);
                                      // --------------------------------------------------------------
                                      // Draw

                                      GlobalVarMapMain.objMainWindowG.mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                                      // --------------------------------------------------------------

                                      return;

                                } // if(В листе еще ничего не было)
                                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> В листе еще ничего не было
        */
        /*

                    // FOR_e >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                    // e.Table (i1)
                    // FOR1
                    int flagredraw = 0;
                    for (int i1 = 0; i1 < lstapin.Count; i1++)
                    {

                        // List_AP (j)
                        // FOR2
                        int flag = 0;
                        for (int j = 0; j < GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Count; j++)
                        {

                            TempADSB_AP obj1 = new TempADSB_AP();
                            obj1.Coordinates = new Coord();

                            // ..........................................................
                            // IP==

                            if (lstapin[i1].Id == GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[j].Id)
                            {
                                flag = 1;

                                if ((GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[j].LatitudePrev == -1) &&
                                    (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[j].LongitudePrev == -1))
                                {
                                    obj1.Id = lstapin[i1].Id;
                                    obj1.ICAO = lstapin[i1].ICAO;
                                    obj1.Coordinates.Latitude = lstapin[i1].Coordinates.Latitude;
                                    obj1.Coordinates.Longitude = lstapin[i1].Coordinates.Longitude;
                                    obj1.LatitudePrev = lstapin[i1].Coordinates.Latitude;
                                    obj1.LongitudePrev = lstapin[i1].Coordinates.Longitude;
                                    obj1.Azimuth = 0;
                                    obj1.AzimuthPrev = 0;
                                    obj1.fdraw = true;
                                    lstap.Add(obj1);

                                    flagredraw = 1;

                                } // LatitudePrev,LongitudePrev==-1

                                // Azimuth
                                else
                                {
                                    double az = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.CalcAzimuth
                                            (
                                             GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[j].LatitudePrev,
                                             GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[j].LongitudePrev,
                                             lstapin[i1].Coordinates.Latitude,
                                             lstapin[i1].Coordinates.Longitude
                                            );

                                    obj1.Id = lstapin[i1].Id;
                                    obj1.ICAO = lstapin[i1].ICAO;
                                    obj1.Coordinates.Latitude = lstapin[i1].Coordinates.Latitude;
                                    obj1.Coordinates.Longitude = lstapin[i1].Coordinates.Longitude;
                                    obj1.LatitudePrev = lstapin[i1].Coordinates.Latitude;
                                    obj1.LongitudePrev = lstapin[i1].Coordinates.Longitude;
                                    obj1.Azimuth = az;
                                    if (az != 0)
                                    {
                                        obj1.fdraw = true;
                                        obj1.AzimuthPrev = az;
                                    }
                                    else
                                    {
                                        obj1.fdraw = false;
                                        obj1.AzimuthPrev = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[j].AzimuthPrev;
                                    }
                                    lstap.Add(obj1);

                                    if (az != 0)
                                        flagredraw = 1;

                                } // Azimuth

                                // Выход из внутреннего цикла
                                j = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Count + 1;

                            } // IP==
                              // ..........................................................
                              // IP!= -> идем дальше

                            else
                            {
                            } //IP!=
                              // ..........................................................

                        } // for2 (j)

                        // не нашли ID -> новый
                        if (flag == 0)
                        {
                            TempADSB_AP obj2 = new TempADSB_AP();
                            obj2.Coordinates = new Coord();
                            obj2.Id = lstapin[i1].Id;
                            obj2.ICAO = lstapin[i1].ICAO;
                            obj2.Coordinates.Latitude = lstapin[i1].Coordinates.Latitude;
                            obj2.Coordinates.Longitude = lstapin[i1].Coordinates.Longitude;
                            obj2.LatitudePrev = lstapin[i1].Coordinates.Latitude;
                            obj2.LongitudePrev = lstapin[i1].Coordinates.Longitude;
                            obj2.Azimuth = 0;
                            obj2.Azimuth = 0;
                            obj2.AzimuthPrev = 0;
                            obj2.fdraw = true;
                            lstap.Add(obj2);

                            flagredraw = 1;
                        }

                    } // For1 (i1)
                      // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> FOR_e

                    ClearListAP();
                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP = lstap;

                    // Draw
                    if (
                        (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Count != 0)
                        && (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.Show_AP == true) &&
                        (flagredraw == 1)
                        )
                        GlobalVarMapMain.objMainWindowG.mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();

                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        */
        //}
        // ****************************************************************** GetListAirplanesWithRotateOnAzimuth

        // GetListAirplanes ****************************************************************************
        // GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP
        // !!! new: flag=0-> для добавления самолетов, =1-> для удаления одного самолета

        // 11_13
        public void GetADSB_DB(
                               List<TempADSB> lstTableADSB,
                               int flag
                              )
        {
            // ---------------------------------------------------------------------
            int ii = 0;
            int ii1 = 0;
            int fi = 0;
            String s1 = "";
            String sICAO = "";
            double lat = 0;
            double lon = 0;

            bool fldraw = false;
            bool f = false;
            int index = 0;

            // otl
            // 1202
            int jjk = 0;
            // ---------------------------------------------------------------------


            // ###
            //List<TempADSB_AP> List_APDel = new List<TempADSB_AP>();
            //List_APDel = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP;

            // DelOneAirplane >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // Удаление одного самолета
            if (flag == 1)
            {
                GlobalVarMapMain.sICAO.Clear();

                // ###
                if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Count > 0)
                {
                    for (ii= (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Count-1);ii>=0;ii--)
                    //for (ii = (List_APDel.Count - 1); ii >= 0; ii--)
                    {
                        index = -1;
                        index = lstTableADSB.FindIndex(x => (x.ICAO == GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[ii].ICAO));
                        //index = lstTableADSB.FindIndex(x => (x.ICAO == List_APDel[ii].ICAO));

                        // В новом списке не нашли такой ICAO->в старом удаляем его
                        if (index < 0)
                        {
                            GlobalVarMapMain.sICAO.Add(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[ii].ICAO);
                            //GlobalVarMapMain.sICAO.Add(List_APDel[ii].ICAO);

                            // 1202
                            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Count>0)
                            //if (List_APDel.Count > 0)

                            {
                                try
                                {
                                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[ii]);
                                    //List_APDel.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[ii]);

                                }
                                catch
                                {
                                    //MessageBox.Show("Error20");
                                }
                            }
                        }

                    } // FOR
                }

                return;
            }  // flag==1
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> DelOneAirplane

            // Airplanes are existing >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            if (lstTableADSB.Count != 0)
            {

                // 1-й сеанс ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // 1-й сеанс получения данных

                // IF1
                if (GlobalVarMapMain.fl_AirPlane == 0)
                {

                    GlobalVarMapMain.fl_AirPlane = 1;

                    // 1-й раз идут для одного ICAO несколько записей, напримкер 
                    // ICAO1,coord1 ICAO1,coord2... ICAO2,coord1,...  

                    // WHILE1: Формируем исходный List самолетов
                    while (lstTableADSB.Count > 0)
                    {
                        // Опорный
                        sICAO = lstTableADSB[0].ICAO; // ICAO
                        fldraw = AddAirPlaneBD(
                                              lstTableADSB[0].Coordinates.Latitude,
                                              lstTableADSB[0].Coordinates.Longitude,
                                              lstTableADSB[0].ICAO,
                                              lstTableADSB[0].Id,
                                               0, // 1-й раз
                                               lstTableADSB[0]
                                              );

                        // 1202
                        // Убираем опорны
                        if (lstTableADSB.Count > 0)
                         {
                           try
                            {
                             lstTableADSB.Remove(lstTableADSB[0]);
                            }
                           catch
                           {
                                //MessageBox.Show("Error21");
                            }
                        }

                        index = 0;
                        // FOR1 входной List: Добавляем все с выбранным ICAO в новый лист
                        //for (ii = 0; ii < lstTableADSB.Count; ii++)
                        while(index>=0)
                        {
                            // Ищем все выбранные ICAO
                            index = lstTableADSB.FindIndex(x => (x.ICAO == sICAO));

                            // Ищем все выбранные ICAO
                            // IF2
                            if (
                              //String.Compare(lstTableADSB[ii].ICAO, sICAO) == 0
                              index>=0
                               )
                            {
                                fldraw = AddAirPlaneBD(
                                                      lstTableADSB[index].Coordinates.Latitude,
                                                      lstTableADSB[index].Coordinates.Longitude,
                                                      lstTableADSB[index].ICAO,
                                                      lstTableADSB[index].Id,
                                                      1, // НЕ 1-й раз
                                                      lstTableADSB[index]
                                                      );
                                // 1202
                                // Убрать из входного листа
                                if (lstTableADSB.Count > 0)
                                {
                                    try
                                    {
                                        lstTableADSB.Remove(lstTableADSB[index]);
                                    }
                                    catch
                                    {
                                        //MessageBox.Show("Error22");
                                    }
                                }

                            } // IF2 

                            //} // FOR1 входной List
                        }; // WHILE(index>=0)

                    }; // WHILE1: (Count входного листа !=0)

                } // IF1: fl_AirPlane == 0
                // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 1-й сеанс

                // НЕ 1-й сеанс +++++++++++++++++++++++++++++++++++++++++++++++++++++
                // ................................................................
                // НЕ 1-й сеанс получения данных

                // ELSE on IF1
                else
                {
                    // WHILE2: Добавляем новые координаты для ICAOi-ых самолетов, если они есть
                    while (lstTableADSB.Count > 0)
                    {
 
                        sICAO = lstTableADSB[0].ICAO; // ICAO
                        fldraw = AddAirPlaneBD(
                                              lstTableADSB[0].Coordinates.Latitude,
                                              lstTableADSB[0].Coordinates.Longitude,
                                              lstTableADSB[0].ICAO,
                                              lstTableADSB[0].Id,
                                              1, // НЕ 1-й раз
                                              lstTableADSB[0]
                                              );
                        // 1202
                        // Убираем 
                        if (lstTableADSB.Count > 0)
                        {
                            try
                            {
                                lstTableADSB.Remove(lstTableADSB[0]);
                            }
                            catch
                            {
                                //MessageBox.Show("Error23");
                            }
                        }

                        index = 0;
                        // FOR1_1 входной List: Добавляем все с выбранным ICAO в новый лист
                        //for (ii = 0; ii < lstTableADSB.Count; ii++)
                        while ((index >= 0)&&(lstTableADSB.Count!=0))
                        {
                            // Ищем все выбранные ICAO
                            index = lstTableADSB.FindIndex(x => (x.ICAO == sICAO));

                            // Ищем все выбранные ICAO
                            // IF2
                            if (
                              //String.Compare(lstTableADSB[ii].ICAO, sICAO) == 0
                              index >= 0
                               )
                            {
                                fldraw = AddAirPlaneBD(
                                                      lstTableADSB[index].Coordinates.Latitude,
                                                      lstTableADSB[index].Coordinates.Longitude,
                                                      lstTableADSB[index].ICAO,
                                                      lstTableADSB[index].Id,
                                                      1, // НЕ 1-й раз
                                                      lstTableADSB[index]
                                                      );

                                // 1202
                                // Убрать из входного листа
                                if (lstTableADSB.Count > 0)
                                {
                                    try
                                    {
                                        lstTableADSB.Remove(lstTableADSB[index]);
                                    }
                                    catch
                                    {
                                        //MessageBox.Show("Error25");
                                    }

                                }

                            } // IF2 

                            //} // FOR1_1 входной List
                        }; // WHILE(index>=0)

                    }; // WHILE2: (Count входного листа !=0)

                } // ELSE on IF1: fl_AirPlane == 1 -> НЕ 1-й сеанс получения данных
                // ................................................................

                // +++++++++++++++++++++++++++++++++++++++++++++++++++++ НЕ 1-й сеанс

            } // if(lstTableADSB.Count!=0)
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Airplanes are existing

            // NO Airplanes  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            else
            {
                if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Count != 0)
                {
                    DelAirPlanes();
                    fldraw = true;
                }

            } // if(lstTableADSB.Count==0)
              //  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> NO Airplanes

            // --------------------------------------------------------------------------   
            // !!! Перерисовка по Tick Timer1

                // Draw
                if (
                (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.Show_AP == true) &&
                (fldraw == true)
                )
                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();

        }
        // **************************************************************************** GetListAirplanes


        // DelAllAirPlanes *****************************************************************************

        // 11_13
        public void DelAirPlanes()
        {

            ClearListAP();

            //GlobalVarLn.Number_air = 0;
            GlobalVarMapMain.fl_AirPlane = 0;

        }
        // ***************************************************************************** DelAllAirPlanes

        // AddAirplane********************** ***********************************************************
        // Добавить самолет
        // ff_first=0 -> 1-й раз в лист
        // Возврат: true -> нужна перерисовка

        // 11_13
        public bool AddAirPlaneBD(
                                  double Lat_air,   // grad
                                  double Long_air,
                                  String sNum_air, // ICAO
                                  int ID,
                                  int ff_first,

                                  // ForArsen
                                  TempADSB objtempADSB
                                         )
        {
            // -------------------------------------------------------------------------------------
            double LatDX = 0;
            double LongDY = 0;
            double LatDX_m = 0;
            double LongDY_m = 0;
            int f = 0;
            double X1 = 0;
            double X2 = 0;
            double Y1 = 0;
            double Y2 = 0;
            double dX = 0;
            double dY = 0;
            double Az = 0;

            double Lt1 = 0;
            double Ln1 = 0;
            double Lt2 = 0;
            double Ln2 = 0;

            int iij = 0;

            double dchislo1 = 0;
            long ichislo1 = 0;
            double dchislo2 = 0;
            long ichislo2 = 0;
            double dchislo3 = 0;
            long ichislo3 = 0;
            double dchislo4 = 0;
            long ichislo4 = 0;

            int jjk = 0;
            // -------------------------------------------------------------------------------------

            // AirPlane (objAirPlane) >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // Заполняем общую структуру (объект класса Самолет)

            // -------------------------------------------------------------------------------------
            TempADSB_AP objAirPlane = new TempADSB_AP();
            objAirPlane.Coordinates = new Coord();
            objAirPlane.list_air_traj = new List<Objects>();
            Objects objAirPlane2 = new Objects();

            // -------------------------------------------------------------------------------------

            // Широта,долгота в градусах (текущие)
            objAirPlane.Coordinates.Latitude = Lat_air;
            objAirPlane.Coordinates.Longitude = Long_air;
            // -------------------------------------------------------------------------------------
            // текущие координаты

            // Для азимута
            Lt2 = Lat_air;
            Ln2 = Long_air;
            // -------------------------------------------------------------------------------------
            objAirPlane.ICAO = sNum_air; // ICAO
            objAirPlane.Id = ID;
            // -------------------------------------------------------------------------------------
            // Список координат (траектория): последний элемент - текущие
            // !!! На вход идет самолет ICAO c новой текущей записью (НЕ вся траектория) 

            objAirPlane2.Latitude = Lat_air;
            objAirPlane2.Longitude = Long_air;
            //objAirPlane.list_air2.Add(objAirPlane2);
            // -------------------------------------------------------------------------------------
            // ForArsen

            objAirPlane.Coordinates.Altitude = objtempADSB.Coordinates.Altitude;
            objAirPlane.Time = objtempADSB.Time;
            // -------------------------------------------------------------------------------------

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> AirPlane (objAirPlane)

            // list_ClassAirPlane(1-е занесение) >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // 1-ое занесение в лист

            // IF1
            if (ff_first == 0)
            {
                // Добавили самолет
                objAirPlane.Azimuth = 0;
                objAirPlane.AzimuthPrev = 0;
                objAirPlane.LatitudePrev = Lat_air;
                objAirPlane.LongitudePrev = Long_air;
                objAirPlane.fdraw = true;
                objAirPlane.list_air_traj.Add(objAirPlane2);
                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Add(objAirPlane);

                return true;

            } // IF1:ff_first == 0
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> list_ClassAirPlane(1-е занесение)

            // list_ClassAirPlane(НЕ 1-е занесение) >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // НЕ 1-ое занесение в лист

            // ELSE on IF1
            else
            {
                // FOR (old list) ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // Есть ли самолет с таким же номером

                f = 0;
                bool fb = false;
                int index = 0;

                while(index>=0)
                //for (int i = (GlobalVarLn.list_ClassAirPlane.Count - 1); i >= 0; i--)
                {
                    // ...............................................................................
                    // Есть самолет с таким же номером (ICAO)
                    index = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.FindIndex(x => (x.ICAO == sNum_air));

                    // IF2
                    if (
                      //String.Compare(GlobalVarLn.list_ClassAirPlane[i].sNum, objAirPlane.sNum) == 0
                       index>=0
                       )
                    {
                        f = 1;


                        //11_13new
                        int Color = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].flColor;


                        // Последние координаты в старом списке (до 5-го знака после запятой)
                        ichislo1 = (long)(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].Coordinates.Latitude * 100000);
                        dchislo1 = ((double)ichislo1) / 100000;
                        // Новые координаты
                        ichislo2 = (long)(objAirPlane.Coordinates.Latitude * 100000);
                        dchislo2 = ((double)ichislo2) / 100000;

                        ichislo3 = (long)(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].Coordinates.Longitude * 100000);
                        dchislo3 = ((double)ichislo3) / 100000;
                        ichislo4 = (long)(objAirPlane.Coordinates.Longitude * 100000);
                        dchislo4 = ((double)ichislo4) / 100000;

                        double sss = ClassBearing.f_D_2Points(dchislo1, dchislo3, dchislo2, dchislo4, 1);
                        objAirPlane2.ds = sss; // m

                        // ==============================================================
                        // Координаты повторились

                        // IF3
                        if (
                            (
                            (dchislo1 == dchislo2) &&
                            (dchislo3 == dchislo4)
                            ) ||
                            //(objAirPlane.Lat > GlobalVarLn.lat_max) ||
                            //(objAirPlane.Lat < GlobalVarLn.lat_min) ||
                            //(objAirPlane.Long > GlobalVarLn.lon_max) ||
                            //(objAirPlane.Long < GlobalVarLn.lon_min) ||
                            (objAirPlane.Coordinates.Latitude == 0) ||
                            (objAirPlane.Coordinates.Longitude == 0)
                           )
                        {
                            fb = false;
                            return false;

                        } // IF3
                        // ==============================================================
                        // координаты не повторились

                        // ELSE on IF3
                        else
                        {
                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            // обновляем лист координат

                            // Предыдущие координаты 
                            Lt1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].Coordinates.Latitude;
                            Ln1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].Coordinates.Longitude;

                            Az = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.CalcAzimuth
                                    (
                                     Lt1,
                                     Ln1,
                                     Lt2,
                                     Ln2
                                    );

                            objAirPlane.Azimuth = Az;
                            objAirPlane.AzimuthPrev = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].Azimuth;
                            objAirPlane.LatitudePrev = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].Coordinates.Latitude;
                            objAirPlane.LongitudePrev = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].Coordinates.Longitude;
                            objAirPlane.fdraw = true;

                            // @@@
                            objAirPlane.S_traj = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].S_traj;
                            objAirPlane.S_traj = objAirPlane.S_traj + objAirPlane2.ds;

                            // @@@
                            // Добавляем новые координаты
                            //if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].list_air_traj.Count >= GlobalVarMapMain.numbtr)
                            if (objAirPlane.S_traj >= GlobalVarMapMain.Smax_traj)
                            {
                                while ((objAirPlane.S_traj >= GlobalVarMapMain.Smax_traj) && (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].list_air_traj.Count != 0))
                                {
                                    objAirPlane.S_traj = objAirPlane.S_traj - GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].list_air_traj[0].ds;

                                    // 1202
                                    // Если список переполнился -> Удаляем 1-й
                                    if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].list_air_traj.Count > 0)
                                    {
                                        try
                                        {
                                            GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].list_air_traj.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].list_air_traj[0]);
                                        }
                                        catch
                                        {
                                            jjk = 5;
                                        }
                                    }
                                }

                                if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].list_air_traj.Count == 1)
                                {
                                    objAirPlane.S_traj = objAirPlane.S_traj - GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].list_air_traj[0].ds;
                                    Objects ooo = new Objects();
                                    ooo = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].list_air_traj[0];
                                    ooo.ds = 0;
                                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].list_air_traj[0] = ooo;
                                }
                                else if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].list_air_traj.Count == 0)
                                {
                                    objAirPlane.S_traj = 0;
                                    objAirPlane2.ds = 0;
                                }

                            }

                            // В списке траектории добавляем новые координаты
                            GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].list_air_traj.Add(objAirPlane2);
                            // Переписываем траекторию во вновь созданный объект
                            for (iij = 0; iij < GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].list_air_traj.Count; iij++)
                            {
                                Objects objtmp2 = new Objects();
                                objtmp2.Latitude = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].list_air_traj[iij].Latitude;
                                objtmp2.Longitude = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].list_air_traj[iij].Longitude;
                                objtmp2.ds = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index].list_air_traj[iij].ds;

                                // Добавляем в List траектории во вновь созданный объект класса-самолета
                                objAirPlane.list_air_traj.Add(objtmp2);
                            }

                            //11_13new
                            objAirPlane.flColor = Color;

                            // 1202
                            // Убрать старый самолет и добавить новый 
                            // Т.е. заменили старый новым
                            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Count > 0)
                            {
                                try
                                {
                                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP[index]);
                                }
                                catch
                                {
                                    jjk = 5;
                                }
                            }

                            GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Add(objAirPlane);
                            fb = true;
                            return true;

                            //} // IF4: счетчик вышел

                        } // ELSE on IF3 координаты не повторились
                        // ==============================================================

                    } // IF2 (есть такой же самолет)
                    // ..............................................................................

            // } // FOR
              }; // WHILE(index>=0)
            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ FOR (old list)

            // New AirPlane ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            // Такого самолета не нашлось при НЕ первом сеансе получения данных

            if (f == 0)
                {
                    // Добавили самолет
                    objAirPlane.Azimuth = 0;
                    objAirPlane.AzimuthPrev = 0;
                    objAirPlane.LatitudePrev = Lat_air;
                    objAirPlane.LongitudePrev = Long_air;
                    objAirPlane.fdraw = true;
                    objAirPlane.list_air_traj.Add(objAirPlane2);
                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP.Add(objAirPlane);

                    return true;
                }
                // ??? Вообще сюда недолжны выходить
                else
                    return fb;
                // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ New AirPlane

            } // ELSE on IF1:НЕ 1-ое занесение в лист
            // -------------------------------------------------------------------------------------

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> list_ClassAirPlane(НЕ 1-е занесение)

        }
        // ******************************************************************************** AddAirplane

        // AddOneJS ***********************************************************************************
        // Добавляем (вернее, переписываем необходимые параметры) в лист классов АСП (моих List<ClassJS>)  
        // один элемент из входного массива от БД типа TableASP
         
        public void AddOneJS(
                             TableASP objASP,
                             // Флаг отрисовки для пеленга:
                             // -1 - не требуется отрисовка для пеленга
                             // =1 -> требуется отрисовка для пеленга (например, 1-й раз при подключении к БД, изменении выделенной строки в таблице)
                             //        (но при этом проверять птички нажатия СП и Пеленг)
                             // =2 -> убрать пеленги (станция исчезла, птичку отжали)
                             int fl
                            )
        {
            ClassJS objClassJS = new ClassJS();
            objClassJS.Coordinates = new Coord();
            objClassJS.IsConnect = new Led();
            objClassJS.Role = new RoleStation();
            objClassJS.Sectors = new Sectors();
            objClassJS.TypeConnection = new TypeConnection();
            //objClassJS.Letters = new byte[objASP.];

            objClassJS.flRedrawPelengFromJS = fl;
            objClassJS.Coordinates.Latitude = objASP.Coordinates.Latitude;
            objClassJS.Coordinates.Longitude = objASP.Coordinates.Longitude;
            objClassJS.Id = objASP.Id;

            // ???????????   Нужны ли все
            objClassJS.AddressIP = objASP.AddressIP;
            objClassJS.AddressPort = objASP.AddressPort;
            objClassJS.AntHeightRec = objASP.AntHeightRec;
            objClassJS.AntHeightSup = objASP.AntHeightSup;
            objClassJS.CallSign = objASP.CallSign;
            objClassJS.Caption = objASP.Caption;
            objClassJS.IdMission = objASP.IdMission;
            objClassJS.Image = objASP.Image;
            objClassJS.IsConnect = objASP.IsConnect;
            objClassJS.ISOwn = objASP.ISOwn;
            objClassJS.LPA13 = objASP.LPA13;
            objClassJS.LPA24 = objASP.LPA24;
            objClassJS.LPA510 = objASP.LPA510;
            objClassJS.Mode = objASP.Mode;
            objClassJS.Role = objASP.Role;
            objClassJS.RRS1 = objASP.RRS1;
            objClassJS.RRS2 = objASP.RRS2;
            objClassJS.Sectors = objASP.Sectors;
            objClassJS.Type = objASP.Type;
            objClassJS.TypeConnection = objASP.TypeConnection;

            GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS.Add(objClassJS);
        }
        // *********************************************************************************** AddOneJS

        // UpdateASP **********************************************************************************
        // Для обработчика события по обновлению списка СП (с учетом флага для отрисовки пеленгов)

        public bool UpdateASP(
                              List<TableASP> lstTableASP
                             )
        {
            int index1 = -1;
            int i1 = 0;
            int i2 = 0;
            bool flRedraw = false; // Для вызова перерисовки
            int fPel = -1;
            double sss = 0;

            // GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS
            // index = lstTableADSB.FindIndex(x => (x.ICAO == sICAO));

            // OldList  *********************************************************************************
            // Проверка: если в новом списке нет какой-либо станции -> значит она исчезла -> удаляем из
            // старого списка и вызываем перерисовку (пеленги нужно удалить: он не отрисуется при проверке наличия СП)

            // FOR_MAIN (old): идем по старому с конца
            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS.Count != 0)
            {
                for (i1 = (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS.Count-1); i1 >=0; i1--)
                {
                    int numb1 = 0;

                    numb1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[i1].Id;

                    index1 = -1;
                    // Есть ли в новом списке такая СП
                    index1 = lstTableASP.FindIndex(x => (x.Id == GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[i1].Id));
                    // Такой нет -> исчезла
                    if(index1<0)
                    {
                        // 1202
                        if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS.Count > 0)
                        {
                            try
                            {
                                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[i1]);
                            }
                            catch
                            { }
                        }

                        flRedraw = true;

                        // IRIFRCH ------------------------------------------------------------------------
                        // Удаляем SPj из внутреннего списка ИРИi

                        int ipl1 = 0;
                        int indexp1 = -1;

                        // Лист ИРИ ФРЧ с пеленгами
                        for(ipl1=0; ipl1< GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count; ipl1++)
                        {
                            // Эта JS исчезла
                            indexp1 = -1;
                            indexp1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[ipl1].list_JSBearing.FindIndex(x => (x.NumberASP == numb1));
                            if(indexp1>=0)
                            {
                                // 1202
                                // Убираем ее из внутреннего списка этого ИРИ
                                if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[ipl1].list_JSBearing.Count > 0)
                                {
                                    try
                                    {
                                        GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[ipl1].list_JSBearing.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[ipl1].list_JSBearing[indexp1]);
                                    }
                                    catch
                                    { }
                                }
                            }

                        } // FOR IRIFRCH
                        // ------------------------------------------------------------------------ IRIFRCH


                    } // SP исчезла

                } // FOR_MAIN (old)
            } // Старый список не нулевой
            //  ********************************************************************************* OldList

            // NewList **********************************************************************************
            // Новые СП или старые 

            // FOR_MAIN1 (new) -> идем по новому списку с конца 
            if (lstTableASP.Count!=0)
            {
                for (i2 = (lstTableASP.Count - 1); i2 >= 0; i2--)
                {
                    fPel = -1;

                    index1 = -1;
                    // Есть ли в старом списке такая СП
                    index1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS.FindIndex(x => (x.Id == lstTableASP[i2].Id));

                    // NewSP ___________________________________________________________________________________
                    // Такой СП не было в старом списке

                    if (index1 < 0)
                    {
                        if ((GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.ShowJS == true) &&
                            (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.Show_SRW_STRF_RS == true)) // для отрисовки пеленгов
                        {
                            fPel = 1;
                        }

                        // Добавляем СП в старый список
                        AddOneJS(lstTableASP[i2], fPel);
                        // Удаляем из нового
                        lstTableASP.Remove(lstTableASP[i2]);
                        flRedraw = true;

                    } // Такой СП не было в старом списке
                      // ___________________________________________________________________________________ NewSP

                    // OldSP ___________________________________________________________________________________
                    // Такая СП была в старом списке

                    else
                    {
                        
                        // Оцениваем изменение местоположения
                        sss = ClassBearing.f_D_2Points(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[index1].Coordinates.Latitude,
                                                       GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[index1].Coordinates.Longitude,
                                                       lstTableASP[i2].Coordinates.Latitude,
                                                       lstTableASP[i2].Coordinates.Longitude,
                                                        1);
                        // ........................................................................................
                        // Это новое положение

                        if(

                            // 222
                            (sss>GlobalVarMapMain.deltaS)||
                            (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[index1].Caption!= lstTableASP[i2].Caption)||
                            (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[index1].LPA13!= lstTableASP[i2].LPA13) ||
                            (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[index1].LPA24 != lstTableASP[i2].LPA24) ||
                            (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[index1].LPA510 != lstTableASP[i2].LPA510) ||
                            (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[index1].RRS1 != lstTableASP[i2].RRS1) ||
                            (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[index1].RRS2 != lstTableASP[i2].RRS2) 
                          )
                        {
                            // Для ИРИ ФРЧ
                            int numb2 = 0;
                            double ltp2 = 0;
                            double lnp2 = 0;
                            numb2 = lstTableASP[i2].Id;
                            ltp2 = lstTableASP[i2].Coordinates.Latitude;
                            lnp2 = lstTableASP[i2].Coordinates.Longitude;

                            // 1202
                            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS.Count > 0)
                            {
                                try
                                {
                                    // Удаляем из старого
                                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[index1]);
                                }
                                catch
                                { }
                            }

                            if ((GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.ShowJS == true) &&
                                (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.Show_SRW_STRF_RS == true)) // для отрисовки пеленгов
                            {
                                fPel = 1;
                            }

                            // Добавляем СП в старый список
                            AddOneJS(lstTableASP[i2], fPel);

                            // 1202
                            if (lstTableASP.Count > 0)
                            {
                                try
                                {
                                    // Удаляем из нового
                                    lstTableASP.Remove(lstTableASP[i2]);
                                }
                                catch
                                {

                                }
                            }

                            flRedraw = true;

                            // IRIFRCH ------------------------------------------------------------------------
                            // Если пеленг остался тем же, то придет только событие по обновлению т-цы СП

                            int ipl2 = 0;
                            int ipl3 = 0;
                            int indexp2 = -1;

                            // Лист ИРИ ФРЧ с пеленгами
                            for (ipl2 = 0; ipl2 < GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count; ipl2++)
                            { 
                                // Ищем эту СП во внутреннем списке ИРИ
                                indexp2 = -1;
                                indexp2 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[ipl2].list_JSBearing.FindIndex(x => (x.NumberASP == numb2));
                                if (indexp2 >= 0)
                                {
                                    Class_IRIFRCH objClass_IRIFRCH = new Class_IRIFRCH();
                                    //objClass_IRIFRCH.list_JSBearing = new List<ClassBearingMap>();
                                    // Копируем старый ИРИ
                                    objClass_IRIFRCH=CopyIRIFRCH(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[ipl2]);
                                    // Меняем координаты станции
                                    objClass_IRIFRCH.list_JSBearing[indexp2].Latitude = ltp2;
                                    objClass_IRIFRCH.list_JSBearing[indexp2].Longitude = lnp2;
                                    // очистка и перерасчет пеленга
                                    RecalculationPeleng(objClass_IRIFRCH, indexp2);

                                    // 1202
                                    if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count > 0)
                                    {
                                        try
                                        {
                                            // Удаляем старый
                                            GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[ipl2]);
                                        }
                                        catch
                                        { }
                                    }

                                    // Добавляем новый
                                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Add(objClass_IRIFRCH);

                                } // if (indexp2 >= 0)

                            } // FOR IRIFRCH
                            // ------------------------------------------------------------------------ IRIFRCH


                        } // Это новое положение
                        // ........................................................................................
                        //  Это то же положение

                        else
                        {
                            // 1202
                            if (lstTableASP.Count > 0)
                            {
                                try
                                {
                                    // Удаляем из нового
                                    lstTableASP.Remove(lstTableASP[i2]);
                                }
                                catch
                                { }
                            }

                        }
                        // ........................................................................................


                    } // Такая СП была в старом списке
                      // ___________________________________________________________________________________ Old

                } // FOR_MAIN1 (new)
                  // ********************************************************************************** NewList
            } // Ненулевой новый список

            // Draw
            //if (
            //    (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.ShowJS == true) &&
            //    (flRedraw == true)
            //    )
            //    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();

            return flRedraw;

        }
        // ********************************************************************************** UpdateASP

        // AddOneFRCH *********************************************************************************
        // Добавляем (вернее, переписываем необходимые параметры) в лист классов ФРЧ (моих List<Class_IRIFRCH>)  
        // один элемент из входного массива от БД типа TempFWS

        public void AddOneIRIFRCH(
                             TempFWS objIRIFRCH
                            )
        {
            int i = 0;

            Class_IRIFRCH objClass_IRIFRCH = new Class_IRIFRCH();
            objClass_IRIFRCH.list_JSBearing = new List<ClassBearingMap>();
            objClass_IRIFRCH.Latitude = objIRIFRCH.Coordinates.Latitude;
            objClass_IRIFRCH.Longitude = objIRIFRCH.Coordinates.Longitude;
            objClass_IRIFRCH.FreqKHz = objIRIFRCH.FreqKHz;
            objClass_IRIFRCH.Id = objIRIFRCH.Id;

            if (objIRIFRCH.IsSelected.HasValue)
                objClass_IRIFRCH.IsSelected = objIRIFRCH.IsSelected.Value;

            // SP >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            if(objIRIFRCH.ListQ.Count>0)
            {
                for(i=0;i< objIRIFRCH.ListQ.Count;i++)
                {


                    //objIRIFRCH.ListQ[0].

                    // For SP[i]
                    if (objIRIFRCH.ListQ[i].Bearing!=-1)
                    {
                        // ---------------------------------------------------------------------------
                        double a1 = 0;
                        double b1 = 0;
                        double c1 = 0;
                        double d1 = 0;
                        double f1 = 0;
                        double g1 = 0;
                        // ---------------------------------------------------------------------------
                        ClassBearingMap objClassBearingMap = new ClassBearingMap();
                        objClassBearingMap.list_bearing = new List<Objects>();
                        // ---------------------------------------------------------------------------
                        double[] arr_Pel = new double[GlobalVarMapMain.numberofdots * 2 + 1];     // R,Широта,долгота
                        double[] arr_Pel_XYZ = new double[GlobalVarMapMain.numberofdots * 3 + 1];
                        // ---------------------------------------------------------------------------
                        objClassBearingMap.Bearing = objIRIFRCH.ListQ[i].Bearing;
                        objClassBearingMap.NumberASP = objIRIFRCH.ListQ[i].NumberASP;
                        // ---------------------------------------------------------------------------
                        // Ищем в основном списке SPi

                        int index = -1;
                        index = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS.FindIndex(x => (x.Id == objIRIFRCH.ListQ[i].NumberASP));
                        if(index>=0)
                        {
                            if(
                               (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[index].Coordinates.Latitude!=-1) &&
                               (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[index].Coordinates.Longitude!=-1)&&
                               (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[index].Coordinates.Latitude != 0) &&
                               (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[index].Coordinates.Longitude != 0) 
                              )
                            {
                                objClassBearingMap.Latitude = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[index].Coordinates.Latitude;
                                objClassBearingMap.Longitude = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[index].Coordinates.Longitude;

                                // Вычисление пеленга от SPi
                                ClassBearing.f_Bearing(
                                                       objClassBearingMap.Bearing,
                                                       GlobalVarMapMain.distance,
                                                       GlobalVarMapMain.numberofdots,
                                                       objClassBearingMap.Latitude,
                                                       objClassBearingMap.Longitude,
                                                       1,  // WGS84
                                                       ref a1, ref b1, ref c1,
                                                       ref d1, ref f1, ref g1,
                                                       ref arr_Pel,
                                                       ref arr_Pel_XYZ
                                                       );

                                // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                                // Занесение точек пеленга в лист

                                int i1 = 0;
                                for (i1 = 0; i1 < GlobalVarMapMain.numberofdots * 2; i1 += 2)
                                {
                                    Objects objObjects = new Objects();

                                    objObjects.Latitude = arr_Pel[i1];
                                    objObjects.Longitude = arr_Pel[i1 + 1];

                                    objClassBearingMap.list_bearing.Add(objObjects);

                                } // For(лист точек пеленга)
                                // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                                // Добавить SPi

                                objClass_IRIFRCH.list_JSBearing.Add(objClassBearingMap);
                                // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                                // Добавить ИРИ ФРЧ

                                //GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Add(objClass_IRIFRCH);
                                // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,


                            } // IF(SPi имеет координаты)

                        } // index>=0 (в основном списке есть SPi)
                        // ---------------------------------------------------------------------------


                    } // IF(Bearing!=-1)

                } // FOR (JS)

            } // Count JS!=0
              // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> SP

            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
            // Добавить ИРИ ФРЧ

            GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Add(objClass_IRIFRCH);
            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,


        }
        // ********************************************************************************* AddOneFRCH

        // CopyIRIFRCH *********************************************************************************
        // Скопировать один ИРИФРЧ из моего списка в другой (Для изменения координат СП): objIRIFRCH2 в objIRIFRCH1

        public Class_IRIFRCH CopyIRIFRCH(
                             //Class_IRIFRCH objIRIFRCH1,
                             Class_IRIFRCH objIRIFRCH2
                               )
        {
            int i = 0;

            Class_IRIFRCH objClass_IRIFRCH = new Class_IRIFRCH();
            objClass_IRIFRCH.list_JSBearing = new List<ClassBearingMap>();

            objClass_IRIFRCH.Latitude = objIRIFRCH2.Latitude;
            objClass_IRIFRCH.Longitude = objIRIFRCH2.Longitude;
            objClass_IRIFRCH.FreqKHz = objIRIFRCH2.FreqKHz;
            objClass_IRIFRCH.Id = objIRIFRCH2.Id;
            objClass_IRIFRCH.IsSelected = objIRIFRCH2.IsSelected;

            // SP >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            if (objIRIFRCH2.list_JSBearing.Count != 0)
            {
                for (i = 0; i < objIRIFRCH2.list_JSBearing.Count; i++)
                {
                        // ---------------------------------------------------------------------------
                        ClassBearingMap objClassBearingMap = new ClassBearingMap();
                        objClassBearingMap.list_bearing = new List<Objects>();
                        // ---------------------------------------------------------------------------
                        objClassBearingMap.Bearing = objIRIFRCH2.list_JSBearing[i].Bearing;
                        objClassBearingMap.NumberASP = objIRIFRCH2.list_JSBearing[i].NumberASP;
                        objClassBearingMap.Latitude = objIRIFRCH2.list_JSBearing[i].Latitude;
                        objClassBearingMap.Longitude = objIRIFRCH2.list_JSBearing[i].Longitude;
                        // ---------------------------------------------------------------------------
                        if(objIRIFRCH2.list_JSBearing[i].list_bearing.Count!=0)
                        {

                          // Занесение точек пеленга в лист
                          int i1 = 0;
                          for (i1 = 0; i1 < objIRIFRCH2.list_JSBearing[i].list_bearing.Count; i1 ++)
                          {
                            Objects objObjects = new Objects();
                            objObjects.Latitude = objIRIFRCH2.list_JSBearing[i].list_bearing[i1].Latitude;
                            objObjects.Longitude = objIRIFRCH2.list_JSBearing[i].list_bearing[i1].Longitude;

                            objClassBearingMap.list_bearing.Add(objObjects);

                          } // For(лист точек пеленга)

                        } // if(objIRIFRCH2.list_JSBearing[i].list_bearing.Count!=0)
                        // ---------------------------------------------------------------------------
                        // Добавить SPi

                         objClass_IRIFRCH.list_JSBearing.Add(objClassBearingMap);
                       // ---------------------------------------------------------------------------

                } // FOR (JS)

            } // Count JS!=0
              // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> SP

            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
            // Скопировать ИРИ ФРЧ

            //objIRIFRCH1 = objClass_IRIFRCH;
            return objClass_IRIFRCH;
            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

        }
        // ********************************************************************************* CopyIRIFRCH

        // RecalculationPeleng *************************************************************************
        // Пересчет пеленга при изменении положения СП[ind]

        public void RecalculationPeleng(
                             Class_IRIFRCH objIRIFRCH,
                             // индекс JSi во внутреннем списке ИРИФРЧ
                             int ind
                            )
        {
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // Очистка Пеленга
            int ip = 0;

            if (objIRIFRCH.list_JSBearing[ind].list_bearing.Count > 0)
            {
                // ИРИ ФРЧ
                for (ip = objIRIFRCH.list_JSBearing[ind].list_bearing.Count - 1; ip >= 0; ip--)
                {
                    try
                    {
                        objIRIFRCH.list_JSBearing[ind].list_bearing.RemoveAt(ip);
                    }
                    catch
                    { }
                }
            }
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // For SP[ind]

            if (objIRIFRCH.list_JSBearing[ind].Bearing != -1)
            {
                // ---------------------------------------------------------------------------
                double a1 = 0;
                double b1 = 0;
                double c1 = 0;
                double d1 = 0;
                double f1 = 0;
                double g1 = 0;
                // ---------------------------------------------------------------------------
                double[] arr_Pel = new double[GlobalVarMapMain.numberofdots * 2 + 1];     // R,Широта,долгота
                double[] arr_Pel_XYZ = new double[GlobalVarMapMain.numberofdots * 3 + 1];
                // ---------------------------------------------------------------------------
                // Вычисление пеленга от SPi

                ClassBearing.f_Bearing(
                                        objIRIFRCH.list_JSBearing[ind].Bearing,
                                        GlobalVarMapMain.distance,
                                        GlobalVarMapMain.numberofdots,
                                        objIRIFRCH.list_JSBearing[ind].Latitude,
                                        objIRIFRCH.list_JSBearing[ind].Longitude,
                                        1,  // WGS84
                                        ref a1, ref b1, ref c1,
                                        ref d1, ref f1, ref g1,
                                        ref arr_Pel,
                                        ref arr_Pel_XYZ
                                      );

                // ---------------------------------------------------------------------------
                // Занесение точек пеленга в лист

                        int i1 = 0;
                        for (i1 = 0; i1 < GlobalVarMapMain.numberofdots * 2; i1 += 2)
                        {
                            Objects objObjects = new Objects();

                            objObjects.Latitude = arr_Pel[i1];
                            objObjects.Longitude = arr_Pel[i1 + 1];

                            objIRIFRCH.list_JSBearing[ind].list_bearing.Add(objObjects);

                        } // For(лист точек пеленга)
                // ---------------------------------------------------------------------------

            } // IF(Bearing!=-1)
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        }
        // ************************************************************************* RecalculationPeleng

        // UpdateIRIFRCH ******************************************************************************
        // Для обработчика событий по обновлению списка ИРИ ФРЧ :
        // - OnUpTable
        // - OnAddRange

        // !!! См. еще куски в АСП

        public bool UpdateIRIFRCH(
                              List<TempFWS> lstIRIFRCH
                                 )
        {

            // ------------------------------------------------------------------------------------------
            bool flRedraw = false;

            int i1 = 0;
            int i2 = 0;

            int index = -1;
            int index1 = -1;
            int index2 = -1;

            int IDI1 = -1;
            int IDI2 = -1;
            // ------------------------------------------------------------------------------------------

            // OldList  *********************************************************************************
            // Проверка: если в новом списке нет какого-либо ИРИ -> значит он исчез -> удаляем из
            // старого списка и вызываем перерисовку 

            // FOR_MAIN (old): идем по старому с конца
            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count > 0)
            {
                for (i1 = (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count - 1); i1 >= 0; i1--)
                {
                    index = -1;
                    // Есть ли в новом списке такой IRI
                    index = lstIRIFRCH.FindIndex(x => (x.Id == GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[i1].Id));
                    // Такого нет -> исчез
                    if (index < 0)
                    {
                        // 1202
                        if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count > 0)
                        {
                            try
                            {
                                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[i1]);
                            }
                            catch
                            { }
                        }

                        flRedraw = true;

                    } // IRI исчез

                } // FOR_MAIN (old)
            } // Старый список не нулевой
            //  ********************************************************************************* OldList

            // SelectedString ***************************************************************************
            // Ищем выделенную строку в старом и новом списке

            index1 = -1;
            index2 = -1;

            // Ищем выделенную строку в старом списке
            index1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.FindIndex(x => (x.IsSelected == true));
            if (index1 >= 0)
                IDI1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].Id;

            // Ищем выделенную строку в новом списке
            index2 = lstIRIFRCH.FindIndex(x => ((x.IsSelected.HasValue)&&(x.IsSelected.Value == true)));
            if (index2 >= 0)
                IDI2 = lstIRIFRCH[index2].Id;

            // *************************************************************************** SelectedString

            // NewList **********************************************************************************
            // Ищем новые ИРИ в новом списке (их нет в старом)

            // FOR_MAIN1 (new) -> идем по новому списку с конца 
            if (lstIRIFRCH.Count > 0)
            {
                for (i2 = (lstIRIFRCH.Count - 1); i2 >= 0; i2--)
                {
                    index1 = -1;
                    // Есть ли в старом списке такой ИРИ
                    index1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.FindIndex(x => (x.Id == lstIRIFRCH[i2].Id));

                    // NewIRI ___________________________________________________________________________________
                    // Такого ИРИ не было в старом списке

                    if (index1 < 0)
                    {

                        // Добавляем СП в старый список
                        AddOneIRIFRCH(lstIRIFRCH[i2]);

                        // 1202
                        if (lstIRIFRCH.Count > 0)
                        {
                            try
                            {
                                // ***
                                // Удаляем из нового
                                lstIRIFRCH.Remove(lstIRIFRCH[i2]);
                                i2 -= 1;
                            }
                            catch
                            { }
                        }

                        flRedraw = true;

                    } // Такого ИРИ не было в старом списке
                      // ___________________________________________________________________________________ NewIRI


                } // FOR_MAIN1 (new)

            }  // IF(lstIRIFRCH.Count != 0)
            // ********************************************************************************** NewList

            // NewList1 *********************************************************************************
            // Анализируем старые ИРИ в новом списке (т.е. которые есть в старом списке)
            // !!! Здесь должны остаться только те ИРИ, которые есть в старом списке

            // FOR_MAIN2 (new) -> идем по новому списку с конца 
            if (lstIRIFRCH.Count >0)
            {
                for (i2 = (lstIRIFRCH.Count - 1); i2 >= 0; i2--)
                {
                    //if (lstIRIFRCH.Count > 0)
                    //{


                        index1 = -1;
                        // Ищем в старом списке такой ИРИ
                        index1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.FindIndex(x => (x.Id == lstIRIFRCH[i2].Id));

                        // NoIRI ___________________________________________________________________________________
                        // Такого ИРИ не было в старом списке -> такого вообще не м. б.

                        if (index1 < 0)
                        {
                            ;
                        } // Такого ИРИ не было в старом списке -> в принципе не м. б.
                          // ___________________________________________________________________________________ NoIRI

                        // Есть IRI _______________________________________________________________________________
                        // Нашли

                        else // index1>=0
                        {
                            double sss = 0;

                            // Оцениваем изменение местоположения
                            sss = ClassBearing.f_D_2Points(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].Latitude,
                                                           GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].Longitude,
                                                           lstIRIFRCH[i2].Coordinates.Latitude,
                                                           lstIRIFRCH[i2].Coordinates.Longitude,
                                                            1);

                            // dS > 250m >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // Местоположение ИРИ изменилось

                            if (
                                (sss > GlobalVarMapMain.deltaS)
                              )
                            {
                                // 1202
                                if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count > 0)
                                {
                                    try
                                    {
                                        // Удаляем из старого
                                        GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1]);
                                    }
                                    catch
                                    { }
                                }

                                // Добавляем IRI в старый список
                                AddOneIRIFRCH(lstIRIFRCH[i2]);

                                // ***
                                // Удаляем из нового
                                lstIRIFRCH.Remove(lstIRIFRCH[i2]);
                                i2 -= 1; ;

                                flRedraw = true;

                            }
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> dS > 250m

                            // dS <= 250m >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // Местоположение ИРИ НЕ изменилось

                            else
                            {
                                int j = 0;
                                int j1 = 0;
                                int j2 = 0;

                                // OldJSj """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
                                // Идем по старому внутреннему списку ИРИ и проверяем, не исчезли ли некоторые JS в старом внутреннем списке

                                if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].list_JSBearing.Count > 0)
                                {
                                    // FOR1 -> старый внутренний
                                    for (j = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].list_JSBearing.Count - 1; j >= 0; j--)
                                    {
                                        index2 = -1;

                                        // FOR2 новый
                                        for (j1 = 0; j1 < lstIRIFRCH[i2].ListQ.Count; j1++)
                                        {

                                            // Проверка: В новом списке такой ИРИ есть
                                            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].list_JSBearing[j].NumberASP ==
                                               lstIRIFRCH[i2].ListQ[j1].NumberASP)
                                            {
                                                index2 = j1;
                                                j1 = lstIRIFRCH[i2].ListQ.Count + 1; // Выход из For2
                                            }

                                        } // FOR2 новый

                                        // Такой JSj нет -> исчезла
                                        if (index2 < 0)
                                        {
                                            // 1202
                                            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].list_JSBearing.Count > 0)
                                            {
                                                try
                                                {
                                                    // Убираем из старого
                                                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].list_JSBearing.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].list_JSBearing[j]);
                                                }
                                                catch
                                                { }
                                            }

                                            flRedraw = true;

                                        } // Такой JSj нет -> исчезла


                                    } // FOR1 -> старый внутренний
                                }
                                // """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" OldJSj

                                // NewJSj """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
                                // Идем по новому внутреннему списку ИРИ и проверяем, не появились ли новые JS 

                                // FOR3 -> новый внутренний
                                if (lstIRIFRCH[i2].ListQ.Count > 0)
                                {
                                    for (j = lstIRIFRCH[i2].ListQ.Count - 1; j >= 0; j--)
                                    {
                                        // Ищем JSj из нового внутреннего списка во внутреннем списке старого ИРИ
                                        index2 = -1;

                                        try
                                        {
                                            index2 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].list_JSBearing.FindIndex(x => (x.NumberASP == lstIRIFRCH[i2].ListQ[j].NumberASP));
                                        }
                                        catch
                                        { }
                                        //catch (System.Exception e)
                                        //{
                                        //    MessageBox.Show(e.Message);
                                        //
                                        //}

                                        // ...................................................................................
                                        // Такой JSj нет -> просто заменяем этот ИРИ на новый
                                        if (index2 < 0)
                                        {
                                            // 1202
                                            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count > 0)
                                            {
                                                try
                                                {
                                                    // Удаляем из старого
                                                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1]);
                                                }
                                                catch
                                                {
                                                    //MessageBox.Show("Error22");

                                                }
                                            }


                                            try
                                            {
                                                // Добавляем IRI в старый список
                                                AddOneIRIFRCH(lstIRIFRCH[i2]);
                                            }
                                            catch
                                            {
                                                //MessageBox.Show("Error2");
                                            }

                                            // 1202
                                            if (lstIRIFRCH.Count > 0)
                                            {
                                                try
                                                {
                                                    // ***
                                                    // Удаляем из нового
                                                    lstIRIFRCH.Remove(lstIRIFRCH[i2]);
                                                    i2 -= 1;
                                                }
                                                catch
                                                {
                                                    //MessageBox.Show("Error3");
                                                }
                                            }

                                            flRedraw = true;

                                        } // Такой JSj нет -> просто заменяем этот ИРИ на новый
                                          // ...................................................................................
                                          // JSj есть -> проверяем пеленги

                                        else
                                        {

                                            // Это новый пеленг-> Просто заменяем ИРИ
                                            if (
                                               (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].list_JSBearing[index2].Bearing != -1) &&
                                               (lstIRIFRCH[i2].ListQ[j].Bearing != -1) &&
                                               (Math.Abs(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].list_JSBearing[index2].Bearing - lstIRIFRCH[i2].ListQ[j].Bearing) > 1)
                                              )
                                            {
                                                // 1202
                                                if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count > 0)
                                                {
                                                    try
                                                    {
                                                        // Удаляем из старого
                                                        GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1]);
                                                    }
                                                    catch
                                                    {
                                                        //MessageBox.Show("Error4");
                                                    }
                                                }

                                                try
                                                {
                                                    // Добавляем IRI в старый список
                                                    AddOneIRIFRCH(lstIRIFRCH[i2]);
                                                }
                                                catch
                                                {
                                                    //MessageBox.Show("Error5");

                                                }
                                                // 1202
                                                if (lstIRIFRCH.Count > 0)
                                                {
                                                    try
                                                    {
                                                     // ***
                                                     // Удаляем из нового
                                                     lstIRIFRCH.Remove(lstIRIFRCH[i2]);
                                                     i2 -= 1;
                                                    }
                                                    catch
                                                    {
                                                        //MessageBox.Show("Error6");
                                                    }
                                                }

                                                flRedraw = true;
                                            }

                                        }  // JSj есть в старом
                                           // ...................................................................................

                                    } // FOR3 -> новый внутренний
                                }
                                // """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" NewJSj

                            } // Местоположение ИРИ НЕ изменилось
                              // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> dS <= 250m

                        } // Нашли ИРИ в старом списке (index1>=0)
                          // _______________________________________________________________________________ Есть IRI


                    //} // IF(lstIRIFRCH.Count != 0)


                } // FOR_MAIN2 (new)

            }  // IF(lstIRIFRCH.Count != 0)
               // ********************************************************************************* NewList1

            // Установка активной строки *******************************************************************

            int j3 = 0;

            for (j3 = 0; j3 < GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count; j3++)
            {
                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[j3].IsSelected = false;
            }

            // ---------------------------------------------------------------------------------------------
            if((IDI1==-1)&&(IDI2!=-1))
            {
                index1 = -1;
                // Ищем в списке такой ИРИ
                index1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.FindIndex(x => (x.Id == IDI2));
                if(index1>=0)
                {
                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].IsSelected = true;
                    flRedraw = true;
                }

            } // if((IDI1==-1)&&(IDI2!=-1))
            // ---------------------------------------------------------------------------------------------
            else if((IDI1 != -1) && (IDI2 == -1))
            {
                index1 = -1;
                // Ищем в списке такой ИРИ
                index1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.FindIndex(x => (x.Id == IDI1));
                if (index1 >= 0)
                {
                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].IsSelected = false;
                    flRedraw = true;
                }

            } // if((IDI1 != -1) && (IDI2 == -1))
            // ---------------------------------------------------------------------------------------------
            else if((IDI1 != -1) && (IDI2 != -1))
            {
                // .........................................................................................
                // IDI1==IDI2

                if(IDI1==IDI2)
                {
                    index1 = -1;
                    // Ищем в списке такой ИРИ
                    index1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.FindIndex(x => (x.Id == IDI1));
                    if (index1 >= 0)
                    {
                        GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].IsSelected = true;
                    }

                } // IDI1==IDI2
                // .........................................................................................
                // IDI1!=IDI2

                else
                {
                    // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                    index1 = -1;
                    // Ищем в списке такой ИРИ
                    index1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.FindIndex(x => (x.Id == IDI1));
                    if (index1 >= 0)
                    {
                        GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].IsSelected = false;
                        flRedraw = true;
                    }
                    // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                    index1 = -1;
                    // Ищем в списке такой ИРИ
                    index1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.FindIndex(x => (x.Id == IDI2));
                    if (index1 >= 0)
                    {
                        GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].IsSelected = true;
                        flRedraw = true;
                    }
                    // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

                } // IDI1!=IDI2
                // .........................................................................................

            } // if((IDI1 != -1) && (IDI2 != -1))
              // ******************************************************************* Установка активной строки
              /*
                          // Draw
                          if (
                              (flRedraw == true)
                             )
                             GlobalVarMapMain.objMainWindowG.mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
              */

            return flRedraw;
        }
        // ****************************************************************************** UpdateIRIFRCH

        // UpdateIRIFRCH_CR ***************************************************************************

        public bool UpdateIRIFRCH_CR(
                                 List<TableReconFWS> lstIRI
                                    )
        {

            // ------------------------------------------------------------------------------------------
            bool flRedraw = false;

            int i1 = 0;
            int i2 = 0;

            int index = -1;
            int index1 = -1;
            int index2 = -1;

            int IDI1 = -1;
            int IDI2 = -1;
            // ------------------------------------------------------------------------------------------

            // OldList  *********************************************************************************
            // Проверка: если в новом списке нет какого-либо ИРИ -> значит он исчез -> удаляем из
            // старого списка и вызываем перерисовку 

            // FOR_MAIN (old): идем по старому с конца
            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.Count > 0)
            {
                for (i1 = (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.Count - 1); i1 >= 0; i1--)
                {
                    index = -1;
                    // Есть ли в новом списке такой IRI
                    index = lstIRI.FindIndex(x => (x.Id == GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD[i1].Id));
                    // Такого нет -> исчез
                    if (index < 0)
                    {
                        if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.Count > 0)
                        {
                            try
                            {
                                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD[i1]);
                            }
                            catch
                            { }
                        }

                        flRedraw = true;

                    } // IRI исчез

                } // FOR_MAIN (old)
            } // Старый список не нулевой
            //  ********************************************************************************* OldList

            // NewList **********************************************************************************
            // Ищем новые ИРИ в новом списке (их нет в старом)

            // FOR_MAIN1 (new) -> идем по новому списку с конца 
            if (lstIRI.Count > 0)
            {
                for (i2 = (lstIRI.Count - 1); i2 >= 0; i2--)
                {
                    index1 = -1;
                    // Есть ли в старом списке такой ИРИ
                    index1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.FindIndex(x => (x.Id == lstIRI[i2].Id));

                    // NewIRI ___________________________________________________________________________________
                    // Такого ИРИ не было в старом списке

                    if (index1 < 0)
                    {

                        // Добавляем IRI в старый список
                        GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.Add(lstIRI[i2]);

                        if (lstIRI.Count > 0)
                        {
                            try
                            {
                                // ***
                                // Удаляем из нового
                                lstIRI.Remove(lstIRI[i2]);
                                i2 -= 1;
                            }
                            catch
                            { }
                        }

                        flRedraw = true;

                    } // Такого ИРИ не было в старом списке
                      // ___________________________________________________________________________________ NewIRI


                } // FOR_MAIN1 (new)

            }  // IF(lstIRIFRCH.Count != 0)
            // ********************************************************************************** NewList

            // NewList1 *********************************************************************************
            // Анализируем старые ИРИ в новом списке (т.е. которые есть в старом списке)
            // !!! Здесь должны остаться только те ИРИ, которые есть в старом списке

            // FOR_MAIN2 (new) -> идем по новому списку с конца 
            if (lstIRI.Count > 0)
            {
                for (i2 = (lstIRI.Count - 1); i2 >= 0; i2--)
                {
                    index1 = -1;
                    // Ищем в старом списке такой ИРИ
                    index1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.FindIndex(x => (x.Id == lstIRI[i2].Id));

                    // NoIRI ___________________________________________________________________________________
                    // Такого ИРИ не было в старом списке -> такого вообще не м. б.

                    if (index1 < 0)
                    {
                        ;
                    } // Такого ИРИ не было в старом списке -> в принципе не м. б.
                    // ___________________________________________________________________________________ NoIRI

                    // Есть IRI _______________________________________________________________________________
                    // Нашли

                    else // index1>=0
                    {
                        double sss = 0;

                        // Оцениваем изменение местоположения
                        sss = ClassBearing.f_D_2Points(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD[index1].Coordinates.Latitude,
                                                       GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD[index1].Coordinates.Longitude,
                                                       lstIRI[i2].Coordinates.Latitude,
                                                       lstIRI[i2].Coordinates.Longitude,
                                                        1);

                        // dS > 250m >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        // Местоположение ИРИ изменилось

                        if (
                            (sss > GlobalVarMapMain.deltaS)
                          )
                        {
                            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.Count > 0)
                            {
                                try
                                {
                                    // Удаляем из старого
                                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD[index1]);
                                }
                                catch
                                { }
                            }

                            // Добавляем IRI в старый список
                            GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.Add(lstIRI[i2]);

                            if (lstIRI.Count > 0)
                            {
                                try
                                {
                                    // ***
                                    // Удаляем из нового
                                    lstIRI.Remove(lstIRI[i2]);
                                    i2 -= 1;
                                }
                                catch
                                { }
                            }

                            flRedraw = true;

                        }
                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> dS > 250m

                        // dS <= 250m >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        // Местоположение ИРИ НЕ изменилось

                        else
                        {
                            if (lstIRI.Count > 0)
                            {
                                try
                                {
                                    // ***
                                    // Удаляем из нового
                                    lstIRI.Remove(lstIRI[i2]);
                                    i2 -= 1;
                                }
                                catch
                                { }
                            }

                        } // Местоположение ИРИ НЕ изменилось
                          // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> dS <= 250m

                    } // Нашли ИРИ в старом списке (index1>=0)
                    // _______________________________________________________________________________ Есть IRI

                } // FOR_MAIN2 (new)

            }  // IF(lstIRIFRCH.Count != 0)
               // ********************************************************************************* NewList1

            /*
                        // Draw
                        if (
                            (flRedraw == true)
                           )
                           GlobalVarMapMain.objMainWindowG.mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
            */

            return flRedraw;

        }
        // *************************************************************************** UpdateIRIFRCH_CR

        // UpdateIRIFRCH_RP ***************************************************************************

        public bool UpdateIRIFRCH_RP(
                                 List<TableSuppressFWS> lstIRI
                                    )
        {

            // ------------------------------------------------------------------------------------------
            bool flRedraw = false;

            int i1 = 0;
            int i2 = 0;

            int index = -1;
            int index1 = -1;
            int index2 = -1;

            int IDI1 = -1;
            int IDI2 = -1;
            // ------------------------------------------------------------------------------------------

            // OldList  *********************************************************************************
            // Проверка: если в новом списке нет какого-либо ИРИ -> значит он исчез -> удаляем из
            // старого списка и вызываем перерисовку 

            // FOR_MAIN (old): идем по старому с конца
            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.Count > 0)
            {
                for (i1 = (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.Count - 1); i1 >= 0; i1--)
                {
                    index = -1;
                    // Есть ли в новом списке такой IRI
                    index = lstIRI.FindIndex(x => (x.Id == GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS[i1].Id));
                    // Такого нет -> исчез
                    if (index < 0)
                    {
                        if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.Count > 0)
                        {
                            try
                            {
                                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS[i1]);
                            }
                            catch
                            { }
                        }

                        flRedraw = true;

                    } // IRI исчез

                } // FOR_MAIN (old)
            } // Старый список не нулевой
            //  ********************************************************************************* OldList

            // NewList **********************************************************************************
            // Ищем новые ИРИ в новом списке (их нет в старом)

            // FOR_MAIN1 (new) -> идем по новому списку с конца 
            if (lstIRI.Count > 0)
            {
                for (i2 = (lstIRI.Count - 1); i2 >= 0; i2--)
                {
                    index1 = -1;
                    // Есть ли в старом списке такой ИРИ
                    index1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.FindIndex(x => (x.Id == lstIRI[i2].Id));

                    // NewIRI ___________________________________________________________________________________
                    // Такого ИРИ не было в старом списке

                    if (index1 < 0)
                    {

                        // Добавляем IRI в старый список
                        GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.Add(lstIRI[i2]);

                        if (lstIRI.Count > 0)
                        {
                            try
                            {
                                // ***
                                // Удаляем из нового
                                lstIRI.Remove(lstIRI[i2]);
                                i2 -= 1;
                            }
                            catch
                            { }
                        }

                        flRedraw = true;

                    } // Такого ИРИ не было в старом списке
                      // ___________________________________________________________________________________ NewIRI


                } // FOR_MAIN1 (new)

            }  // IF(lstIRIFRCH.Count != 0)
            // ********************************************************************************** NewList

            // NewList1 *********************************************************************************
            // Анализируем старые ИРИ в новом списке (т.е. которые есть в старом списке)
            // !!! Здесь должны остаться только те ИРИ, которые есть в старом списке

            // FOR_MAIN2 (new) -> идем по новому списку с конца 
            if (lstIRI.Count > 0)
            {
                for (i2 = (lstIRI.Count - 1); i2 >= 0; i2--)
                {
                    index1 = -1;
                    // Ищем в старом списке такой ИРИ
                    index1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.FindIndex(x => (x.Id == lstIRI[i2].Id));

                    // NoIRI ___________________________________________________________________________________
                    // Такого ИРИ не было в старом списке -> такого вообще не м. б.

                    if (index1 < 0)
                    {
                        ;
                    } // Такого ИРИ не было в старом списке -> в принципе не м. б.
                    // ___________________________________________________________________________________ NoIRI

                    // Есть IRI _______________________________________________________________________________
                    // Нашли

                    else // index1>=0
                    {
                        double sss = 0;

                        // Оцениваем изменение местоположения
                        sss = ClassBearing.f_D_2Points(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS[index1].Coordinates.Latitude,
                                                       GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS[index1].Coordinates.Longitude,
                                                       lstIRI[i2].Coordinates.Latitude,
                                                       lstIRI[i2].Coordinates.Longitude,
                                                        1);

                        // dS > 250m >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        // Местоположение ИРИ изменилось

                        if (
                            (sss > GlobalVarMapMain.deltaS)
                          )
                        {
                            // 1202
                            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.Count > 0)
                            {
                                try
                                {
                                    // Удаляем из старого
                                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS[index1]);
                                }
                                catch
                                { }
                            }

                            // Добавляем IRI в старый список
                            GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.Add(lstIRI[i2]);

                            if (lstIRI.Count > 0)
                            {
                                try
                                {
                                    // ***
                                    // Удаляем из нового
                                    lstIRI.Remove(lstIRI[i2]);
                                    i2 -= 1;
                                }
                                catch
                                { }
                            }

                            flRedraw = true;

                        }
                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> dS > 250m

                        // dS <= 250m >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        // Местоположение ИРИ НЕ изменилось

                        else
                        {
                            // 1202
                            if (lstIRI.Count>0)
                            {
                                try
                                {
                                    // ***
                                    // Удаляем из нового
                                    lstIRI.Remove(lstIRI[i2]);
                                    i2 -= 1;
                                }
                                catch
                                { }
                            }

                        } // Местоположение ИРИ НЕ изменилось
                          // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> dS <= 250m

                    } // Нашли ИРИ в старом списке (index1>=0)
                    // _______________________________________________________________________________ Есть IRI

                } // FOR_MAIN2 (new)

            }  // IF(lstIRIFRCH.Count != 0)
               // ********************************************************************************* NewList1

            /*
                        // Draw
                        if (
                            (flRedraw == true)
                           )
                           GlobalVarMapMain.objMainWindowG.mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
            */

            return flRedraw;

        }
        // *************************************************************************** UpdateIRIFRCH_RP

        // AddOnePPRCH **********************************************************************************
        // Добавляем (вернее, переписываем необходимые параметры) в лист классов ППРЧ (моих List<Class_IRIPPRCH>)  
        // один элемент из входного массива от БД типа TableReconFHSS,TableSourceFHSS

        public void AddOneIRIPPRCH(
                             TableSourceFHSS objIRIPPRCH,
                             List<TableReconFHSS> lstIRIPPRCH_Recon
                            )
        {
            /*
                        int i = 0;

                        Class_IRIFRCH objClass_IRIFRCH = new Class_IRIFRCH();
                        objClass_IRIFRCH.list_JSBearing = new List<ClassBearingMap>();
                        objClass_IRIFRCH.Latitude = objIRIFRCH.Coordinates.Latitude;
                        objClass_IRIFRCH.Longitude = objIRIFRCH.Coordinates.Longitude;
                        objClass_IRIFRCH.FreqKHz = objIRIFRCH.FreqKHz;
                        objClass_IRIFRCH.Id = objIRIFRCH.Id;

                        if (objIRIFRCH.IsSelected.HasValue)
                            objClass_IRIFRCH.IsSelected = objIRIFRCH.IsSelected.Value;

                        // SP >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        if (objIRIFRCH.ListQ.Count != 0)
                        {
                            for (i = 0; i < objIRIFRCH.ListQ.Count; i++)
                            {


                                //objIRIFRCH.ListQ[0].

                                // For SP[i]
                                if (objIRIFRCH.ListQ[i].Bearing != -1)
                                {
                                    // ---------------------------------------------------------------------------
                                    double a1 = 0;
                                    double b1 = 0;
                                    double c1 = 0;
                                    double d1 = 0;
                                    double f1 = 0;
                                    double g1 = 0;
                                    // ---------------------------------------------------------------------------
                                    ClassBearingMap objClassBearingMap = new ClassBearingMap();
                                    objClassBearingMap.list_bearing = new List<Objects>();
                                    // ---------------------------------------------------------------------------
                                    double[] arr_Pel = new double[GlobalVarMapMain.numberofdots * 2 + 1];     // R,Широта,долгота
                                    double[] arr_Pel_XYZ = new double[GlobalVarMapMain.numberofdots * 3 + 1];
                                    // ---------------------------------------------------------------------------
                                    objClassBearingMap.Bearing = objIRIFRCH.ListQ[i].Bearing;
                                    objClassBearingMap.NumberASP = objIRIFRCH.ListQ[i].NumberASP;
                                    // ---------------------------------------------------------------------------
                                    // Ищем в основном списке SPi

                                    int index = -1;
                                    index = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS.FindIndex(x => (x.Id == objIRIFRCH.ListQ[i].NumberASP));
                                    if (index >= 0)
                                    {
                                        if (
                                           (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[index].Coordinates.Latitude != -1) &&
                                           (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[index].Coordinates.Longitude != -1) &&
                                           (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[index].Coordinates.Latitude != 0) &&
                                           (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[index].Coordinates.Longitude != 0)
                                          )
                                        {
                                            objClassBearingMap.Latitude = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[index].Coordinates.Latitude;
                                            objClassBearingMap.Longitude = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_JS[index].Coordinates.Longitude;

                                            // Вычисление пеленга от SPi
                                            ClassBearing.f_Bearing(
                                                                   objClassBearingMap.Bearing,
                                                                   GlobalVarMapMain.distance,
                                                                   GlobalVarMapMain.numberofdots,
                                                                   objClassBearingMap.Latitude,
                                                                   objClassBearingMap.Longitude,
                                                                   1,  // WGS84
                                                                   ref a1, ref b1, ref c1,
                                                                   ref d1, ref f1, ref g1,
                                                                   ref arr_Pel,
                                                                   ref arr_Pel_XYZ
                                                                   );

                                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                                            // Занесение точек пеленга в лист

                                            int i1 = 0;
                                            for (i1 = 0; i1 < GlobalVarMapMain.numberofdots * 2; i1 += 2)
                                            {
                                                Objects objObjects = new Objects();

                                                objObjects.Latitude = arr_Pel[i1];
                                                objObjects.Longitude = arr_Pel[i1 + 1];

                                                objClassBearingMap.list_bearing.Add(objObjects);

                                            } // For(лист точек пеленга)
                                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                                            // Добавить SPi

                                            objClass_IRIFRCH.list_JSBearing.Add(objClassBearingMap);
                                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                                            // Добавить ИРИ ФРЧ

                                            //GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Add(objClass_IRIFRCH);
                                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,


                                        } // IF(SPi имеет координаты)

                                    } // index>=0 (в основном списке есть SPi)
                                    // ---------------------------------------------------------------------------


                                } // IF(Bearing!=-1)

                            } // FOR (JS)

                        } // Count JS!=0
                          // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> SP

                        // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                        // Добавить ИРИ ФРЧ

                        GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Add(objClass_IRIFRCH);
                        // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
            */

            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
            int i = 0;

            Class_IRIPPRCH objClass_IRIPPRCH = new Class_IRIPPRCH();
            objClass_IRIPPRCH.list_JSBearing = new List<ClassBearingMapPPRCH>();
            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

            objClass_IRIPPRCH.Latitude = objIRIPPRCH.Coordinates.Latitude;
            objClass_IRIPPRCH.Longitude = objIRIPPRCH.Coordinates.Longitude;

            objClass_IRIPPRCH.Id = objIRIPPRCH.IdFHSS;

            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
            // F

            if (lstIRIPPRCH_Recon.Count > 0)
            {
                int indx = -1;
                indx = lstIRIPPRCH_Recon.FindIndex(x => (x.Id == objIRIPPRCH.IdFHSS));
                if (indx >= 0)
                {
                    objClass_IRIPPRCH.FreqKHz = (lstIRIPPRCH_Recon[indx].FreqMinKHz + lstIRIPPRCH_Recon[indx].FreqMaxKHz) / 2;

                } // indx >= 0

                if (lstIRIPPRCH_Recon[indx].IsSelected.HasValue)
                    objClass_IRIPPRCH.IsSelected = lstIRIPPRCH_Recon[indx].IsSelected.Value;

            } // lstIRIPPRCH_Recon.Count!=0
            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,


            // ??????????????? Pelengs

            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
            // Добавить ИРИ ППРЧ

            GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF.Add(objClass_IRIPPRCH);
            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

        }
        // ******************************************************************************** AddOnePPRCH

        // UpdateIRIPPRCH *****************************************************************************
        // Для обработчика событий по обновлению списка ИРИ ППРЧ :
        // - OnUpTable (for TableReconFHSS, TableSourceFHSS)

        public void UpdateIRIPPRCH(
                              List<TableReconFHSS> lstIRIPPRCH,
                              List<TableSourceFHSS> lstIRIPPRCH_Coord
                                 )
        {
            /*
                        // ------------------------------------------------------------------------------------------
                        bool flRedraw = false;

                        int i1 = 0;
                        int i2 = 0;

                        int index = -1;
                        int index1 = -1;
                        int index2 = -1;

                        int IDI1 = -1;
                        int IDI2 = -1;
                        // ------------------------------------------------------------------------------------------

                        // OldList  *********************************************************************************
                        // Проверка: если в новом списке нет какого-либо ИРИ -> значит он исчез -> удаляем из
                        // старого списка и вызываем перерисовку 

                        // FOR_MAIN (old): идем по старому с конца
                        if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count != 0)
                        {
                            for (i1 = (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count - 1); i1 >= 0; i1--)
                            {
                                index = -1;
                                // Есть ли в новом списке такой IRI
                                index = lstIRIFRCH.FindIndex(x => (x.Id == GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[i1].Id));
                                // Такого нет -> исчез
                                if (index < 0)
                                {
                                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[i1]);
                                    flRedraw = true;

                                } // IRI исчез

                            } // FOR_MAIN (old)
                        } // Старый список не нулевой
                        //  ********************************************************************************* OldList

                        // SelectedString ***************************************************************************
                        // Ищем выделенную строку в старом и новом списке

                        index1 = -1;
                        index2 = -1;

                        // Ищем выделенную строку в старом списке
                        index1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.FindIndex(x => (x.IsSelected == true));
                        if (index1 >= 0)
                            IDI1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].Id;

                        // Ищем выделенную строку в новом списке
                        index2 = lstIRIFRCH.FindIndex(x => ((x.IsSelected.HasValue) && (x.IsSelected.Value == true)));
                        if (index2 >= 0)
                            IDI2 = lstIRIFRCH[index2].Id;

                        // *************************************************************************** SelectedString

                        // NewList **********************************************************************************
                        // Ищем новые ИРИ в новом списке (их нет в старом)

                        // FOR_MAIN1 (new) -> идем по новому списку с конца 
                        if (lstIRIFRCH.Count != 0)
                        {
                            for (i2 = (lstIRIFRCH.Count - 1); i2 >= 0; i2--)
                            {
                                index1 = -1;
                                // Есть ли в старом списке такой ИРИ
                                index1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.FindIndex(x => (x.Id == lstIRIFRCH[i2].Id));

                                // NewIRI ___________________________________________________________________________________
                                // Такого ИРИ не было в старом списке

                                if (index1 < 0)
                                {

                                    // Добавляем СП в старый список
                                    AddOneIRIFRCH(lstIRIFRCH[i2]);
                                    // Удаляем из нового
                                    lstIRIFRCH.Remove(lstIRIFRCH[i2]);
                                    flRedraw = true;

                                } // Такого ИРИ не было в старом списке
                                  // ___________________________________________________________________________________ NewIRI


                            } // FOR_MAIN1 (new)

                        }  // IF(lstIRIFRCH.Count != 0)
                        // ********************************************************************************** NewList

                        // NewList1 *********************************************************************************
                        // Анализируем старые ИРИ в новом списке (т.е. которые есть в старом списке)
                        // !!! Здесь должны остаться только те ИРИ, которые есть в старом списке

                        // FOR_MAIN2 (new) -> идем по новому списку с конца 
                        if (lstIRIFRCH.Count != 0)
                        {
                            for (i2 = (lstIRIFRCH.Count - 1); i2 >= 0; i2--)
                            {
                                index1 = -1;
                                // Ищем в старом списке такой ИРИ
                                index1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.FindIndex(x => (x.Id == lstIRIFRCH[i2].Id));

                                // NoIRI ___________________________________________________________________________________
                                // Такого ИРИ не было в старом списке -> такого вообще не м. б.

                                if (index1 < 0)
                                {
                                    ;
                                } // Такого ИРИ не было в старом списке -> в принципе не м. б.
                                // ___________________________________________________________________________________ NoIRI

                                // Есть IRI _______________________________________________________________________________
                                // Нашли

                                else // index1>=0
                                {
                                    double sss = 0;

                                    // Оцениваем изменение местоположения
                                    sss = ClassBearing.f_D_2Points(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].Latitude,
                                                                   GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].Longitude,
                                                                   lstIRIFRCH[i2].Coordinates.Latitude,
                                                                   lstIRIFRCH[i2].Coordinates.Longitude,
                                                                    1);

                                    // dS > 250m >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                    // Местоположение ИРИ изменилось

                                    if (
                                        (sss > GlobalVarMapMain.deltaS)
                                      )
                                    {
                                        // Удаляем из старого
                                        GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1]);
                                        // Добавляем IRI в старый список
                                        AddOneIRIFRCH(lstIRIFRCH[i2]);
                                        // Удаляем из нового
                                        lstIRIFRCH.Remove(lstIRIFRCH[i2]);
                                        flRedraw = true;

                                    }
                                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> dS > 250m

                                    // dS <= 250m >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                    // Местоположение ИРИ НЕ изменилось

                                    else
                                    {
                                        int j = 0;
                                        int j1 = 0;
                                        int j2 = 0;

                                        // OldJSj """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
                                        // Идем по старому внутреннему списку ИРИ и проверяем, не исчезли ли некоторые JS в старом внутреннем списке

                                        // FOR1 -> старый внутренний
                                        for (j = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].list_JSBearing.Count - 1; j >= 0; j--)
                                        {
                                            index2 = -1;

                                            // FOR2 новый
                                            for (j1 = 0; j1 < lstIRIFRCH[i2].ListQ.Count; j1++)
                                            {

                                                // Проверка: В новом списке такой ИРИ есть
                                                if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].list_JSBearing[j].NumberASP ==
                                                   lstIRIFRCH[i2].ListQ[j1].NumberASP)
                                                {
                                                    index2 = j1;
                                                    j1 = lstIRIFRCH[i2].ListQ.Count + 1; // Выход из For2
                                                }

                                            } // FOR2 новый

                                            // Такой JSj нет -> исчезла
                                            if (index2 < 0)
                                            {
                                                // Убираем из старого
                                                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].list_JSBearing.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].list_JSBearing[j]);
                                                flRedraw = true;

                                            } // Такой JSj нет -> исчезла


                                        } // FOR1 -> старый внутренний
                                        // """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" OldJSj

                                        // NewJSj """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
                                        // Идем по новому внутреннему списку ИРИ и проверяем, не появились ли новые JS 

                                        // FOR3 -> новый внутренний
                                        for (j = lstIRIFRCH[i2].ListQ.Count - 1; j >= 0; j--)
                                        {
                                            // Ищем JSj из нового внутреннего списка во внутреннем списке старого ИРИ
                                            index2 = -1;
                                            index2 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].list_JSBearing.FindIndex(x => (x.NumberASP == lstIRIFRCH[i2].ListQ[j].NumberASP));

                                            // ...................................................................................
                                            // Такой JSj нет -> просто заменяем этот ИРИ на новый
                                            if (index2 < 0)
                                            {
                                                // Удаляем из старого
                                                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1]);
                                                // Добавляем IRI в старый список
                                                AddOneIRIFRCH(lstIRIFRCH[i2]);
                                                // Удаляем из нового
                                                lstIRIFRCH.Remove(lstIRIFRCH[i2]);
                                                flRedraw = true;

                                            } // Такой JSj нет -> просто заменяем этот ИРИ на новый
                                            // ...................................................................................
                                            // JSj есть -> проверяем пеленги

                                            else
                                            {

                                                // Это новый пеленг-> Просто заменяем ИРИ
                                                if (
                                                   (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].list_JSBearing[index2].Bearing != -1) &&
                                                   (lstIRIFRCH[i2].ListQ[j].Bearing != -1) &&
                                                   (Math.Abs(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].list_JSBearing[index2].Bearing - lstIRIFRCH[i2].ListQ[j].Bearing) > 1)
                                                  )
                                                {
                                                    // Удаляем из старого
                                                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1]);
                                                    // Добавляем IRI в старый список
                                                    AddOneIRIFRCH(lstIRIFRCH[i2]);
                                                    // Удаляем из нового
                                                    lstIRIFRCH.Remove(lstIRIFRCH[i2]);
                                                    flRedraw = true;
                                                }

                                            }  // JSj есть в старом
                                            // ...................................................................................

                                        } // FOR3 -> новый внутренний
                                        // """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" NewJSj

                                    } // Местоположение ИРИ НЕ изменилось
                                      // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> dS <= 250m

                                } // Нашли ИРИ в старом списке (index1>=0)
                                // _______________________________________________________________________________ Есть IRI

                            } // FOR_MAIN2 (new)

                        }  // IF(lstIRIFRCH.Count != 0)
                           // ********************************************************************************* NewList1

                        // Установка активной строки *******************************************************************

                        int j3 = 0;

                        for (j3 = 0; j3 < GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count; j3++)
                        {
                            GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[j3].IsSelected = false;
                        }

                        // ---------------------------------------------------------------------------------------------
                        if ((IDI1 == -1) && (IDI2 != -1))
                        {
                            index1 = -1;
                            // Ищем в списке такой ИРИ
                            index1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.FindIndex(x => (x.Id == IDI2));
                            if (index1 >= 0)
                            {
                                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].IsSelected = true;
                                flRedraw = true;
                            }

                        } // if((IDI1==-1)&&(IDI2!=-1))
                        // ---------------------------------------------------------------------------------------------
                        else if ((IDI1 != -1) && (IDI2 == -1))
                        {
                            index1 = -1;
                            // Ищем в списке такой ИРИ
                            index1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.FindIndex(x => (x.Id == IDI1));
                            if (index1 >= 0)
                            {
                                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].IsSelected = false;
                                flRedraw = true;
                            }

                        } // if((IDI1 != -1) && (IDI2 == -1))
                        // ---------------------------------------------------------------------------------------------
                        else if ((IDI1 != -1) && (IDI2 != -1))
                        {
                            // .........................................................................................
                            // IDI1==IDI2

                            if (IDI1 == IDI2)
                            {
                                index1 = -1;
                                // Ищем в списке такой ИРИ
                                index1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.FindIndex(x => (x.Id == IDI1));
                                if (index1 >= 0)
                                {
                                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].IsSelected = true;
                                }

                            } // IDI1==IDI2
                            // .........................................................................................
                            // IDI1!=IDI2

                            else
                            {
                                // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                                index1 = -1;
                                // Ищем в списке такой ИРИ
                                index1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.FindIndex(x => (x.Id == IDI1));
                                if (index1 >= 0)
                                {
                                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].IsSelected = false;
                                    flRedraw = true;
                                }
                                // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                                index1 = -1;
                                // Ищем в списке такой ИРИ
                                index1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.FindIndex(x => (x.Id == IDI2));
                                if (index1 >= 0)
                                {
                                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF[index1].IsSelected = true;
                                    flRedraw = true;
                                }
                                // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

                            } // IDI1!=IDI2
                            // .........................................................................................

                        } // if((IDI1 != -1) && (IDI2 != -1))
                        // ******************************************************************* Установка активной строки

                        // Draw
                        if (
                            (flRedraw == true)
                           )
                            GlobalVarMapMain.objMainWindowG.mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
            */


            // ------------------------------------------------------------------------------------------
            bool flRedraw = false;

            int i1 = 0;
            int i2 = 0;

            int index = -1;
            int index1 = -1;
            int index2 = -1;

            int IDI1 = -1;
            int IDI2 = -1;
            // ------------------------------------------------------------------------------------------

            // OldList  *********************************************************************************
            // Проверка: если в новом списке нет какого-либо ИРИ -> значит он исчез -> удаляем из
            // старого списка и вызываем перерисовку 

            // FOR_MAIN (old): идем по старому с конца
            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count > 0)
            {
                for (i1 = (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF.Count - 1); i1 >= 0; i1--)
                {
                    index = -1;
                    // Есть ли в новом списке такой IRI
                    index = lstIRIPPRCH_Coord.FindIndex(x => (x.IdFHSS == GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF[i1].Id));
                    // Такого нет -> исчез
                    if (index < 0)
                    {
                        // 1202
                        if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF.Count > 0)
                        {
                            try
                            {
                                // Удаляем из старого
                                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF[i1]);
                            }
                            catch
                            { }
                        }

                        flRedraw = true;

                    } // IRI исчез

                } // FOR_MAIN (old)
            } // Старый список не нулевой
              //  ********************************************************************************* OldList

            // SelectedString ***************************************************************************
            // Ищем выделенную строку в старом и новом списке

            index1 = -1;
            index2 = -1;

            // *************************************************************************** SelectedString

            // NewList **********************************************************************************
            // Ищем новые ИРИ в новом списке (их нет в старом)

            // FOR_MAIN1 (new) -> идем по новому списку с конца 
            if (lstIRIPPRCH_Coord.Count > 0)
            {
                for (i2 = (lstIRIPPRCH_Coord.Count - 1); i2 >= 0; i2--)
                {
                    index1 = -1;
                    // Есть ли в старом списке такой ИРИ
                    index1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF.FindIndex(x => (x.Id == lstIRIPPRCH_Coord[i2].IdFHSS));

                    // NewIRI ___________________________________________________________________________________
                    // Такого ИРИ не было в старом списке

                    if (index1 < 0)
                    {
                        // Добавляем СП в старый список
                        AddOneIRIPPRCH( lstIRIPPRCH_Coord[i2], lstIRIPPRCH);

                        // 1202
                        if (lstIRIPPRCH_Coord.Count > 0)
                        {
                            try
                            {
                                // Удаляем из нового
                                lstIRIPPRCH_Coord.Remove(lstIRIPPRCH_Coord[i2]);
                            }
                            catch
                            { }
                        }

                        flRedraw = true;

                    } // Такого ИРИ не было в старом списке
                      // ___________________________________________________________________________________ NewIRI

                } // FOR_MAIN1 (new)

            }  // IF(lstIRIPPRCH_Coord.Count != 0)
               // ********************************************************************************** NewList

            // NewList1 *********************************************************************************
            // Анализируем старые ИРИ в новом списке (т.е. которые есть в старом списке)
            // !!! Здесь должны остаться только те ИРИ, которые есть в старом списке

            // FOR_MAIN2 (new) -> идем по новому списку с конца 
            if (lstIRIPPRCH_Coord.Count != 0)
            {
                for (i2 = (lstIRIPPRCH_Coord.Count - 1); i2 >= 0; i2--)
                {
                    index1 = -1;
                    // Ищем в старом списке такой ИРИ
                    index1 = GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF.FindIndex(x => (x.Id == lstIRIPPRCH_Coord[i2].IdFHSS));

                    // NoIRI ___________________________________________________________________________________
                    // Такого ИРИ не было в старом списке -> такого вообще не м. б.

                    if (index1 < 0)
                    {
                        ;
                    } // Такого ИРИ не было в старом списке -> в принципе не м. б.
                      // ___________________________________________________________________________________ NoIRI

                    // Есть IRI _______________________________________________________________________________
                    // Нашли

                    else // index1>=0
                    {
                        double sss = 0;

                        // Оцениваем изменение местоположения
                        sss = ClassBearing.f_D_2Points(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF[index1].Latitude,
                                                       GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF[index1].Longitude,
                                                       lstIRIPPRCH_Coord[i2].Coordinates.Latitude,
                                                       lstIRIPPRCH_Coord[i2].Coordinates.Longitude,
                                                        1);

                        // dS > 250m >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        // Местоположение ИРИ изменилось

                        if (
                            (sss > GlobalVarMapMain.deltaS)
                          )
                        {
                            // 1202
                            if (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF.Count > 0)
                            {
                                try
                                {
                                    // Удаляем из старого
                                    GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF.Remove(GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SRW_STRF[index1]);
                                }
                                catch
                                { }
                            }

                            // Добавляем IRI в старый список
                            AddOneIRIPPRCH(lstIRIPPRCH_Coord[i2], lstIRIPPRCH);

                            // 1202
                            if (lstIRIPPRCH_Coord.Count > 0)
                            {
                                try
                                {
                                    // Удаляем из нового
                                    lstIRIPPRCH_Coord.Remove(lstIRIPPRCH_Coord[i2]);
                                }
                                catch
                                { }
                            }

                            flRedraw = true;

                        }
                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> dS > 250m

                        // dS <= 250m >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        // Местоположение ИРИ НЕ изменилось

                        else
                        {

                        } // Местоположение ИРИ НЕ изменилось
                          // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> dS <= 250m

                    } // Нашли ИРИ в старом списке (index1>=0)
                      // _______________________________________________________________________________ Есть IRI

                } // FOR_MAIN2 (new)

            }  // IF(lstIRIPPRCH_Coord.Count != 0)
               // ********************************************************************************* NewList1

            // Установка активной строки *******************************************************************

              // ******************************************************************* Установка активной строки

            // Draw
            if (
                (flRedraw == true)
               )
                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();


        }
        // ****************************************************************************** UpdateIRIPPRCH


    } // Class
} // NameSpace
