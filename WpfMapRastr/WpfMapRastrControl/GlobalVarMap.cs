﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using ModelsTablesDBLib;


using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;

using Bitmap = System.Drawing.Bitmap;
using Point = System.Drawing.Point;

// ???
using Image = System.Drawing.Image;
//using Image = System.Drawing.Image;
using Icon = System.Drawing.Icon;

// Для YAML реализации
using YamlDotNet.Serialization;
using ClassLibraryIniFiles;
using GeoCalculator;


namespace WpfMapRastrControl
{
    // Structures ****************************************************************************** 

    // Point Coordinates Mercator (Lat,Long=-1)/
    // degree (X,Y=-1)
    public struct Point_XY_LatLong
    {
        // -----------------------------------------------------------------------------------------
        // Merkator
        public double X;
        public double Y;

        public double Lat;
        public double Long;

    }
    // -----------------------------------------------------------------------------------------
    // Structure JS/Objects

    public struct Objects
    {
        // Merkator m
        public double X;
        public double Y;

        // Degree
        public double Latitude;
        public double Longitude;

        // m
        public double Altitude;

        // For airplanes
        public double ds;
    }
    // -----------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------

    // ****************************************************************************** Structures

    public class GlobalVarMap
    //class GlobalVarMap
    {

        // MAP *********************************************************************************
        //--------------------------------------------------------------------------------------
        public static bool MapOpened = false;
        public static bool MatrixOpened = false;
        public static double Scale = -1;
        //--------------------------------------------------------------------------------------
        // !!! Сюда заносятся координаты клика мыши на карте (или при движении мыши)

        // Mercator
        public static double X_Rastr = 0;
        public static double Y_Rastr = 0;
        public static string Str_Rastr = "";

        public static double LAT_Rastr = 0;
        public static double LONG_Rastr = 0;
        public static double H_Rastr = 0;
        //--------------------------------------------------------------------------------------
        public static bool MousePress = false;
        //--------------------------------------------------------------------------------------

        // ********************************************************************************* MAP

        // JS *********************************************************************************     
        // Jammer stations


        // ********************************************************************************* JS

        // Obj1 *******************************************************************************
        // Objects1 

        // ******************************************************************************* Obj1

        // Obj2 *******************************************************************************
        // Objects2 

        // ******************************************************************************* Obj2

        // Frequency **************************************************************************

        public static double fCl1 = 30;
        public static double fCl1_2 = 88;

        public static double fCl2 = 88;
        public static double fCl2_2 = 108;

        public static double fCl3 = 108;
        public static double fCl3_2 = 170;

        public static double fCl4 = 170;
        public static double fCl4_2 = 220;

        public static double fCl5 = 220;
        public static double fCl5_2 = 400;

        public static double fCl6 = 400;
        public static double fCl6_2 = 512;

        public static double fCl7 = 512;
        public static double fCl7_2 = 860;

        public static double fCl8 = 860;
        public static double fCl8_2 = 1215;

        public static double fCl9 = 1215;
        public static double fCl9_2 = 1575;

        public static double fCl10 = 1575;
        public static double fCl10_2 = 3000;

        /*
                public static int lat1_temp= 23006;
                public static int lat2_temp = 23498;
                public static int lon1_temp = 52530;
                public static int lon2_temp = 52995;
        */

        public static int lat1_temp = 5386500;
        public static int lat2_temp = 5397332;
        public static int lon1_temp = 2747061;
        public static int lon2_temp = 2768947;


        // ************************************************************************** Frequency

        // Antennas ***************************************************************************

        // Сектора антенн (grad)
        public static double sect_RRS1 = 7;
        public static double sect_RRS2 = 7;
        public static double sect_LPA13 = 120;
        public static double sect_LPA24 = 120;
        public static double sect_LPA510 = 120;

        // Colors
        // LPA13
        public static int col1_1 = 255;
        public static int col1_2 = 0;
        public static int col1_3 = 0;
        // LPA24
        public static int col2_1 = 0;
        public static int col2_2 = 255;
        public static int col2_3 = 0;
        // LPA510
        public static int col3_1 = 0;
        public static int col3_2 = 0;
        public static int col3_3 = 255;
        // RRS1
        public static int col4_1 = 100;
        public static int col4_2 = 0;
        public static int col4_3 = 0;
        // RRS2
        public static int col5_1 = 0;
        public static int col5_2 = 100;
        public static int col5_3 = 0;

        //R
        public static int rant = 10000;
        // *************************************************************************** Antennas

        // Flags ********************************************************************************
        // AAA

        // Флаг языка: по умолчанию английский (false)
        // true  -> русский
        public static bool FlagLanguageMap = false;

        // ******************************************************************************** Flags

        //public static IMapObject objectGrozaS1_L;



    } // Class
} // Namespace
