﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

// Semen
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;

using System.Drawing;
using System.Runtime.InteropServices;
using System.IO;

using WpfMapControl;
using System.Data;
using System.Reflection;
using System.ServiceModel;
using System.Diagnostics;
using System.Threading;
using System.Globalization;
using Microsoft.Win32;

// Otl
// Для YAML реализации
using YamlDotNet.Serialization;
// Для JSON реализации
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
// Для реализации XML
using System.Xml;
using System.Xml.Linq;

using Bitmap = System.Drawing.Bitmap;
using Point = System.Drawing.Point;

// ???
using Image = System.Drawing.Image;

using ClassLibraryIniFiles;
using ModelsTablesDBLib;

using GeoCalculator;
// 333
//using WpfTasksControl;


namespace WpfMapRastrControl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class UserControl1 : UserControl
    {
        //private bool flag;
        // Functions of map interface
        public ClassInterfaceMap objClassInterfaceMap { get; set; }
        // Functions of map interface for Control call
        private ClassMapRastrMenu objClassMapRastrMenu;

        // 777
        // Functions of map redraw
        //private ClassMapRastrReDraw objClassMapRastrReDraw;
        public ClassMapRastrReDraw objClassMapRastrReDraw;

        // 666
        public event EventHandler<ClassArg> OnMouseCl = (object sender, ClassArg data) => { };

        // *****************************************************************************************
        public UserControl1()
        {
            InitializeComponent();

            //flag = false;
            objClassInterfaceMap = new ClassInterfaceMap(MapControl);
            objClassMapRastrMenu = new ClassMapRastrMenu(MapControl);
            objClassMapRastrReDraw = new ClassMapRastrReDraw(MapControl, objClassInterfaceMap);

            string s = "";
            double scale = -1;

            // OpenMap -------------------------------------------------------------------------

            // Get path to map from .yaml file
            s = objClassMapRastrMenu.PathMapFile();

            if (s == "")
            {
                if (GlobalVarMap.FlagLanguageMap == false)
                    MessageBox.Show("Can't open .yaml file");
                else
                    MessageBox.Show("Невозможно открыть .yaml файл");

                return;
            }

            objClassInterfaceMap.OpenMap(s);

            GlobalVarMap.MapOpened = objClassInterfaceMap.MapOpened;

            if(GlobalVarMap.MapOpened==false)
            {
                if (GlobalVarMap.FlagLanguageMap == false)
                    MessageBox.Show("Can't load map");
                else
                    MessageBox.Show("Невозможно загрузить карту");

                return;
            }
            // ------------------------------------------------------------------------- OpenMap

            // OpenMatrix ----------------------------------------------------------------------

            // Get path to height matrix from .yaml file
            s = objClassMapRastrMenu.PathMatrixFile();

            if (s == "")
            {
                if (GlobalVarMap.FlagLanguageMap == false)
                    MessageBox.Show("Can't open .yaml file");
                else
                    MessageBox.Show("Невозможно открыть .yaml файл");

                return;
            }

            objClassInterfaceMap.OpenMatrix(s);

            GlobalVarMap.MatrixOpened = objClassInterfaceMap.MatrixOpened;

            if (GlobalVarMap.MatrixOpened == false)
            {
                if (GlobalVarMap.FlagLanguageMap == false)
                    MessageBox.Show("Can't load the height matrix");
                else
                    MessageBox.Show("Невозможно загрузить матрицу высот");

                //return;
            }
            // ---------------------------------------------------------------------- OpenMatrix

            // Scale ---------------------------------------------------------------------------

            // Get scale from .yaml file
            scale = objClassMapRastrMenu.ReadScaleFile();

            if (scale == -1)
            {
                if (GlobalVarMap.FlagLanguageMap == false)
                    MessageBox.Show("Can't open .yaml file");
                else
                    MessageBox.Show("Невозможно открыть .yaml файл");

                return;
            }

            objClassInterfaceMap.SetScale(scale);

            GlobalVarMap.Scale = objClassInterfaceMap.Scale;

            if (GlobalVarMap.Scale == -1)
            {
                if (GlobalVarMap.FlagLanguageMap == false)
                    MessageBox.Show("Can't set scale");
                else
                    MessageBox.Show("Невозможно установить масштаб");

                return;
            }

            // --------------------------------------------------------------------------- Scale

        }
        // ***************************************************************************************** CONSTRUCTOR

        // ButtonPlus ******************************************************************************************
        // Increase scale

        private void ButtonPlus_Click(object sender, RoutedEventArgs e)
        {
            bool bvar = false;

            bvar = objClassInterfaceMap.IncreaseScale();

            // The scale has been increased
            if(bvar==true)
            {
                GlobalVarMap.Scale = objClassInterfaceMap.Scale;

            } // The scale has been increased

            // The scale hasn't been increased
            else
            {
                if (GlobalVarMap.FlagLanguageMap == false)
                    MessageBox.Show("Impossible to increase the scale of map");
                else
                    MessageBox.Show("Невозможно увеличить масштаб карты");

                return;

            } // The scale hasn't been increased

            // Scale >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // Save current scale to . yaml file

            string s = objClassMapRastrMenu.WriteScaleFile();

            if (s != "")
                MessageBox.Show(s);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Scale

        } // ButtonPlus
        // ****************************************************************************************** ButtonPlus

        // ButtonMinus *****************************************************************************************
        // Decrease scale

        private void ButtonMinus_Click(object sender, RoutedEventArgs e)
        {
            bool bvar = false;

            bvar = objClassInterfaceMap.DecreaseScale();

            // The scale has been decreased
            if (bvar == true)
            {
                GlobalVarMap.Scale = objClassInterfaceMap.Scale;

            } // The scale has been decreased

            // The scale hasn't been decreased
            else
            {
                if (GlobalVarMap.FlagLanguageMap == false)
                    MessageBox.Show("Impossible to decrease the scale of map");
                else
                    MessageBox.Show("Невозможно уменьшить масштаб карты");

                return;

            } // The scale hasn't been decreased

            // Scale >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // Save current scale to . yaml file

            string s = objClassMapRastrMenu.WriteScaleFile();

            if (s != "")
                MessageBox.Show(s);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Scale

        } // ButtonMinus
        // ***************************************************************************************** ButtonMinus

        // ButtonBase ******************************************************************************************
        // Base scale

        private void ButtonBase_Click(object sender, RoutedEventArgs e)
        {

            bool bvar = false;

            bvar = objClassInterfaceMap.BaseScale();

            // The base scale has been set
            if (bvar == true)
            {
                GlobalVarMap.Scale = objClassInterfaceMap.Scale;

            } // The base scale has been set

            // The base scale hasn't been set
            else
            {
                if (GlobalVarMap.FlagLanguageMap == false)
                    MessageBox.Show("Impossible to set the base scale");
                else
                    MessageBox.Show("Невозможно установить базовый масштаб");

                return;

            } // The base scale hasn't been set

            // Scale >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // Save current scale to . yaml file

            string s = objClassMapRastrMenu.WriteScaleFile();

            if (s != "")
                MessageBox.Show(s);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Scale

        } // ButtonBase
        // ****************************************************************************************** ButtonBase

        // ButtonSenter ****************************************************************************************
        // Центрирование карты по координатам базовой станции
        // Map centring on base station coordinates
        // 999

        private void ButtonSenter_Click(object sender, RoutedEventArgs e)
        {
            // -------------------------------------------------------------------------------------------------
            bool bvar = false;
            double x = 0;
            double y = 0;
            double lat = 0;
            double lon = 0;
            int i = 0;
            // -------------------------------------------------------------------------------------------------
            // Otl
            // Для отладки здесь пока зададим конкретные координаты (из прошлого проекта)
            // Mercator/degree

            // Otl
            //lat = 53.78746;
            //lon = 25.71507;
            // 19.07
            //objClassInterfaceMap.List_JS[0].ISOwn = true;

            for ( i=0; i<objClassInterfaceMap.List_JS.Count; i++ )
            {
                if(objClassInterfaceMap.List_JS[i].ISOwn==true)
                {
                    lat = objClassInterfaceMap.List_JS[i].Coordinates.Latitude;
                    lon = objClassInterfaceMap.List_JS[i].Coordinates.Longitude;
                    i = objClassInterfaceMap.List_JS.Count + 1;
                }
            }

            var p = Mercator.FromLonLat(lon, lat);
            x = p.X;
            y = p.Y;
            // -------------------------------------------------------------------------------------------------
            // MercatorMap or GeographicMap?

            string s = "";
            InitMapYaml initMapYaml = new InitMapYaml();
            s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
            // -------------------------------------------------------------------------------------------------
            // MercatorMap

            if(initMapYaml.TypeMap==0)
            {
                // Center map on pozition XY (Mercator)
                bvar = objClassInterfaceMap.CenterMapToXY(x, y);
            } // MercatorMap
            // -------------------------------------------------------------------------------------------------
            // GeographicMap

            else
            {
                // Center map on pozition LatLong (degree)
                bvar = objClassInterfaceMap.CenterMapToLatLong(lat, lon);
            } // GeographicMap
            // -------------------------------------------------------------------------------------------------
            if (bvar == false)
            {
                if (GlobalVarMap.FlagLanguageMap == false)
                    MessageBox.Show("Can't center map on this pozition");
                else
                    MessageBox.Show("Невозможно сцентрировать карту по этой позиции");

                return;

            } // The base scale has been set
            // -------------------------------------------------------------------------------------------------

        } // ButtonSenter
          // **************************************************************************************** ButtonSenter

        // OpenMap_Menu ****************************************************************************************
        // Open map from dialog window

        private void MenuItemOpen_Click(object sender, RoutedEventArgs e)
        {
            string s = "";

            // ................................................................................................
            // Path to map from dialog window
            s = objClassMapRastrMenu.PathDialog();

            if (s == "")
            {
                if (GlobalVarMap.FlagLanguageMap == false)
                    MessageBox.Show("Can't get map path from dialog window");
                else
                    MessageBox.Show("Невозможно получить путь к карте из диалогового окна");

                return;
            }
            // ................................................................................................

            // Map wasn't opened
            if (GlobalVarMap.MapOpened == false)
            {
                // Open map
                objClassInterfaceMap.OpenMap(s);
                GlobalVarMap.MapOpened = objClassInterfaceMap.MapOpened;

                if (GlobalVarMap.MapOpened == false)
                {
                    if (GlobalVarMap.FlagLanguageMap == false)
                        MessageBox.Show("Can't load map");
                    else
                        MessageBox.Show("Невозможно загрузить карту");

                    return;
                }
            } // Map wasn't opened

            // Map was opened
            else
            {
                if (GlobalVarMap.FlagLanguageMap == false)
                    MessageBox.Show("Map is opened");
                else
                    MessageBox.Show("Карта уже открыта");

                return;

            } // else (Map was opened)
            // ................................................................................................

        }
        // **************************************************************************************** OpenMap_Menu

        // OpenMatrix_Menu *************************************************************************************
        private void MenuItemMatr_Click(object sender, RoutedEventArgs e)
        {
            string s = "";

            // ................................................................................................
            // Path to the height matrix from dialog window

            s = objClassMapRastrMenu.PathDialog();

            if (s == "")
            {
                if (GlobalVarMap.FlagLanguageMap == false)
                    MessageBox.Show("Can't get height matrix path from dialog window");
                else
                    MessageBox.Show("Невозможно получить путь к матрице высот из диалогового окна");

                return;
            }
            // ................................................................................................

            // Matrix wasn't opened
            if (GlobalVarMap.MatrixOpened == false)
            {
                // Open Matrix
                objClassInterfaceMap.OpenMatrix(s);
                GlobalVarMap.MatrixOpened = objClassInterfaceMap.MatrixOpened;

                if (GlobalVarMap.MatrixOpened == false)
                {
                    if (GlobalVarMap.FlagLanguageMap == false)
                        MessageBox.Show("Can't load the height matrix");
                    else
                        MessageBox.Show("Невозможно загрузить матрицу высот");

                    return;
                }
            } // Matrix wasn't opened

            // Matrix was opened
            else
            {
                if (GlobalVarMap.FlagLanguageMap == false)
                    MessageBox.Show("The height matrix is opened");
                else
                    MessageBox.Show("Матрица высот уже загружена");

                return;

            } // else (Matrix was opened)
            // ................................................................................................

        }
        // ************************************************************************************* OpenMatrix_Menu

        // CloseMapMatr_Menu ***********************************************************************************
        // Close map, matrix, save current scale

        private void MenuItemClose_Click(object sender, RoutedEventArgs e)
        {
            string s = "";

            // CloseMap >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            if (GlobalVarMap.MapOpened == true)
            {
              objClassInterfaceMap.CloseMap();
              GlobalVarMap.MapOpened = objClassInterfaceMap.MapOpened;

                if (GlobalVarMap.MapOpened == true)
                {
                    if (GlobalVarMap.FlagLanguageMap == false)
                        MessageBox.Show("Can't close map");
                    else
                        MessageBox.Show("Невозможно закрыть карту");

                    return;
                }

            } // if(GlobalVarMap.MapOpened == true)
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CloseMap

            // CloseMatrix >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            if (GlobalVarMap.MatrixOpened == true)
            {
                objClassInterfaceMap.CloseMatrix();
                GlobalVarMap.MatrixOpened = objClassInterfaceMap.MatrixOpened;

                if (GlobalVarMap.MatrixOpened == true)
                {
                    if (GlobalVarMap.FlagLanguageMap == false)
                        MessageBox.Show("Can't close the height matrix");
                    else
                        MessageBox.Show("Невозможно закрыть матрицу высот");

                    return;
                }

            } // if(GlobalVarMap.MatrixOpened == true)
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CloseMatrix

            // Scale >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // Save current scale to . yaml file

            s = objClassMapRastrMenu.WriteScaleFile();

            if (s != "")
                MessageBox.Show(s);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Scale

        }
        // *********************************************************************************** CloseMapMatr_Menu

        // MOUSE_MOVE ******************************************************************************************
        // Element MapControl event: mouse motion on the map
        // 999
        
            private void MapControl_MapMouseMoveEvent(object sender, Location e)
            {
            
            // -------------------------------------------------------------------------------------------------
            // INI

            string s = "";
            InitMapYaml initMapYaml = new InitMapYaml();
            s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
            // -------------------------------------------------------------------------------------------------

            // .................................................................................................
            // File .yaml was read

            if (s == "")
            {
                if (initMapYaml.TypeMap == 0)
                {
                    // MERCATOR_MAP **********************************************************************************

                    if ((e.Latitude != null) && (e.Longitude != null))

                    {
                        try
                        {
                            GlobalVarMap.X_Rastr = e.Longitude;
                            GlobalVarMap.Y_Rastr = e.Latitude;
                            // GlobalVarMap.H_Rastr = (double)e.Altitude;
                        }
                        catch
                        {
                            if (GlobalVarMap.FlagLanguageMap == false)
                                MessageBox.Show("Unable to determine map coordinates");
                            else
                                MessageBox.Show("Невозможно определить координаты карты");

                            return;
                        }

                        // Converting to LatLong
                        try
                        {
                            var p = Mercator.ToLonLat(GlobalVarMap.X_Rastr, GlobalVarMap.Y_Rastr);
                            GlobalVarMap.LAT_Rastr = p.Y;
                            GlobalVarMap.LONG_Rastr = p.X;

                            if (GlobalVarMap.LAT_Rastr < -90)
                                GlobalVarMap.LAT_Rastr = -90;
                            if (GlobalVarMap.LAT_Rastr > 90)
                                GlobalVarMap.LAT_Rastr = 90;

                            if (GlobalVarMap.LONG_Rastr < -180)
                                GlobalVarMap.LONG_Rastr = -180;
                            if (GlobalVarMap.LONG_Rastr > 180)
                                GlobalVarMap.LONG_Rastr = 180;


                        }
                        catch
                        {
                            if (GlobalVarMap.FlagLanguageMap == false)
                                MessageBox.Show("Unable to determine Latitude and Longitude coordinates");
                            else
                                MessageBox.Show("Невозможно определить широту и долготу");

                            return;
                        }

                    } // (e.Latitude != null) && (e.Longitude != null) && (e.Altitude != null)
                      // -------------------------------------------------------------------------------------------------

                    DrawCurrentCoordinates();

                } // TypeMap==0 
                // ********************************************************************************** MERCATOR_MAP

                else
                {
                    // GEOGRAPHICS_MAP ***********************************************************************************

                    // -------------------------------------------------------------------------------------------------
                    // Current map coordinates
                    // !!! Хотя за пределами карты долгота и широта показывают, а высота==null

                    // !!! Vernut
                    //if ((e.Latitude != null) && (e.Longitude != null) && (e.Altitude != null))
                    if ((e.Latitude != null) && (e.Longitude != null))

                    {
                        try
                        {
                            GlobalVarMap.LAT_Rastr = e.Latitude;
                            GlobalVarMap.LONG_Rastr = e.Longitude;
                            // GlobalVarMap.H_Rastr = (double)e.Altitude;

                            if (GlobalVarMap.LAT_Rastr < -90)
                                GlobalVarMap.LAT_Rastr = -90;
                            if (GlobalVarMap.LAT_Rastr > 90)
                                GlobalVarMap.LAT_Rastr = 90;

                            if (GlobalVarMap.LONG_Rastr < -180)
                                GlobalVarMap.LONG_Rastr = -180;
                            if (GlobalVarMap.LONG_Rastr > 180)
                                GlobalVarMap.LONG_Rastr = 180;

                        }
                        catch
                        {
                            if (GlobalVarMap.FlagLanguageMap == false)
                                MessageBox.Show("Unable to determine map coordinates");
                            else
                                MessageBox.Show("Невозможно определить координаты карты");

                            return;
                        }

                        // Converting to Merkator
                        try
                        {
                            var p = Mercator.FromLonLat(e.Longitude, e.Latitude);
                            GlobalVarMap.X_Rastr = p.X;
                            GlobalVarMap.Y_Rastr = p.Y;

                        }
                        catch
                        {
                            if (GlobalVarMap.FlagLanguageMap == false)
                                MessageBox.Show("Unable to determine Merkator coordinates");
                            else
                                MessageBox.Show("Невозможно определить координаты Меркатора");

                            return;
                        }

                    } // (e.Latitude != null) && (e.Longitude != null) && (e.Altitude != null)
                      // -------------------------------------------------------------------------------------------------

                    DrawCurrentCoordinates();

                    // *********************************************************************************** GEOGRAPHICS_MAP

                } // TypeMap=1

            } // IF( Файл .yaml was read)
              // ...........................................................................
              // File .yaml wasn't read

            else
            {
                if (GlobalVarMap.FlagLanguageMap == false)
                    MessageBox.Show("Can't read .yaml file");
                else
                    MessageBox.Show("Невозможно прочитать .yaml файл");


            } // ELSE (// File .yaml wasn't read)
              // ...........................................................................

        }
        // ****************************************************************************************** MOUSE_MOVE

        // MAP_CLICK *******************************************************************************************
        // Element MapControl event: mouse click on the map

        //private void MapControl_MapClickEvent(object sender, MapMouseClickEventArgs e)
        public void MapControl_MapClickEvent(object sender, MapMouseClickEventArgs e)
        {
            // 666
            OnMouseCl(this, new ClassArg(GlobalVarMap.LAT_Rastr, GlobalVarMap.LONG_Rastr, GlobalVarMap.H_Rastr, 
                                         GlobalVarMap.X_Rastr, GlobalVarMap.Y_Rastr, GlobalVarMap.Str_Rastr));

        }
        // ******************************************************************************************* MAP_CLICK

        // checkedBoxJS_Checked ********************************************************************************
        private void checkBoxJS_Checked(object sender, RoutedEventArgs e)
        {

         // DrawJS ------------------------------------------------------------------------------------------
         objClassInterfaceMap.ShowJS = true;
         objClassMapRastrReDraw.ReDrawMapAll();
         // ------------------------------------------------------------------------------------------ DrawJS
        }
        // ******************************************************************************** checkedBoxJS_Checked

        // checkedBoxJS_Unchecked ******************************************************************************

        private void checkBoxJS_Unchecked(object sender, RoutedEventArgs e)
        {
            // DelJS ------------------------------------------------------------------------------------------
            objClassInterfaceMap.ShowJS = false;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ------------------------------------------------------------------------------------------ DelJS
        }
        // ****************************************************************************** checkedBoxJS_Unchecked

        // checkedBoxAntennas_Checked **************************************************************************
        private void checkBoxAntennas_Checked(object sender, RoutedEventArgs e)
        {

            // DrawAntennas ------------------------------------------------------------------------------------
            objClassInterfaceMap.ShowAntennas = true;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ------------------------------------------------------------------------------------ DrawAntennas
        }
        // ************************************************************************** checkedBoxAntennas_Checked

        // checkedBoxAntennas_Unchecked ************************************************************************

        private void checkBoxAntennas_Unchecked(object sender, RoutedEventArgs e)
        {
            // DelAntennas -------------------------------------------------------------------------------------
            objClassInterfaceMap.ShowAntennas = false;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ------------------------------------------------------------------------------------- DelAntennas
        }
        // ************************************************************************ checkedBoxAntennas_Unchecked

        // checkBoxObj1_Checked ********************************************************************************
        // ИРИ ФРЧ

        private void checkBoxObj1_Checked(object sender, RoutedEventArgs e)
        {
            // DrawObj1 -----------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_SRW_FRF = true;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ----------------------------------------------------------------------------------------- DrawObj1
        }
        // checkBoxObj1_Checked ********************************************************************************

        // checkBoxObj1_Unchecked ******************************************************************************
        private void checkBoxObj1_Unchecked(object sender, RoutedEventArgs e)
        {
            // DelObj1 ------------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_SRW_FRF = false;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ------------------------------------------------------------------------------------------ DelObj1
        }

        // ****************************************************************************** checkBoxObj1_Unchecked

        // checkBox_SRW_FRF_TD_Checked ********************************************************************************
        // ИРИ ФРЧ ЦР

        private void checkBoxObj2_Checked(object sender, RoutedEventArgs e)
        {
            // DrawObj2 -----------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_SRW_FRF_TD = true;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ----------------------------------------------------------------------------------------- DrawObj2
        }
        // ******************************************************************************** checkBoxObj2_Checked

        // checkBoxObj2_Unchecked ******************************************************************************

        private void checkBoxObj2_Unchecked(object sender, RoutedEventArgs e)
        {
            // DelOb2 ------------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_SRW_FRF_TD = false;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ------------------------------------------------------------------------------------------ DelObj2
        }
        // ****************************************************************************** checkBoxObj2_Unchecked

        // checkBox_SRW_FRF_RS_Checked ********************************************************************************
        // ИРИ ФРЧ РП

        private void checkBoxObj3_Checked(object sender, RoutedEventArgs e)
        {
            // DrawObj3 -----------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_SRW_FRF_RS = true;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ----------------------------------------------------------------------------------------- DrawObj3

        }
        // ******************************************************************************** checkBox_SRW_FRF_RS_Checked

        // checkBoxObj3_Unchecked ******************************************************************************

        private void checkBoxObj3_Unchecked(object sender, RoutedEventArgs e)
        {
            // DelOb3 ------------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_SRW_FRF_RS = false;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ------------------------------------------------------------------------------------------ DelObj3

        }
        // ****************************************************************************** checkBoxObj3_Unchecked

        // checkBox_SRW_STRF_Checked ***************************************************************************

        private void checkBoxObj4_Checked(object sender, RoutedEventArgs e)
        {

            // DrawObj4 -----------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_SRW_STRF = true;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ----------------------------------------------------------------------------------------- DrawObj4

        }
        // *************************************************************************** checkBox_SRW_STRF_Checked

        // checkBoxObj4_Unchecked ******************************************************************************

        private void checkBoxObj4_Unchecked(object sender, RoutedEventArgs e)
        {
            // DelOb4 ------------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_SRW_STRF = false;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ------------------------------------------------------------------------------------------ DelObj4
        }
        // ****************************************************************************** checkBoxObj4_Unchecked

        // checkBox_SRW_STRF_RS Checked ************************************************************************
        // Сейчас это пеленги

        private void checkBoxObj5_Checked(object sender, RoutedEventArgs e)
        {
            // DrawObj5 -----------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_SRW_STRF_RS = true;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ----------------------------------------------------------------------------------------- DrawObj5

        }
        // ************************************************************************ checkBox_SRW_STRF_RS Checked

        // checkBoxObj5_Unchecked ******************************************************************************

        private void checkBoxObj5_Unchecked(object sender, RoutedEventArgs e)
        {
            // DelOb5 ------------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_SRW_STRF_RS = false;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ------------------------------------------------------------------------------------------ DelObj5

        }
        // ****************************************************************************** checkBoxObj5_Unchecked

        // checkBox_Airplanes Checked **************************************************************************

        private void checkBoxAirplanes_Checked(object sender, RoutedEventArgs e)
        {

            // DrawAP ------------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_AP = true;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ------------------------------------------------------------------------------------------ DrawAP

        }
        // ************************************************************************** checkBox_Airplanes Checked

        // checkBoxAirplanes_Unchecked *************************************************************************

        private void checkBoxAirplanes_Unchecked(object sender, RoutedEventArgs e)
        {
            // DelAP ------------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_AP = false;

            objClassMapRastrReDraw.ReDrawMapAll();
            // ------------------------------------------------------------------------------------------ DelAP
            Txt10.Text = "";

        }
        // ************************************************************************* checkBoxAirplanes_Unchecked


        // INTERFACE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // Functions of interface

        // OpenMap ******************************************************************************
        // Open Map (path - path to map: from dialog or from ini file)

        public void OpenMap(string pathMap)
        {
            objClassInterfaceMap.OpenMap(pathMap);
        }
        // ****************************************************************************** OpenMap

        // OpenMatrix ***************************************************************************
        // Open Matrix (path - path to Matrix: from dialog or from ini file)

        public void OpenMatrix(string pathMatrix)
        {
            objClassInterfaceMap.OpenMatrix(pathMatrix);
        }
        // *************************************************************************** OpenMatrix

        // SetScale *****************************************************************************

        public void SetScale(double scale)
        {
            objClassInterfaceMap.SetScale(scale);
        }
        // ***************************************************************************** SetScale

        // CloseMap *****************************************************************************
        // Close Map 

        public void CloseMap()
        {
            objClassInterfaceMap.CloseMap();
        }
        // ***************************************************************************** CloseMap

        // CloseMatrix **************************************************************************
        // Close the height matrix 

        public void CloseMatrix()
        {
            objClassInterfaceMap.CloseMatrix();
        }
        // ************************************************************************** CloseMatrix

        // Increase the scale *******************************************************************

        public bool IncreaseScale()
        {
            bool bvar=objClassInterfaceMap.IncreaseScale();
            return bvar;
        }
        // ******************************************************************* Increase the scale

        // Decrease the scale *******************************************************************

        public bool DecreaseScale()
        {
            bool bvar=objClassInterfaceMap.DecreaseScale();
            return bvar;
        }
        // ******************************************************************* Decrease the scale

        // Base scale ***************************************************************************

        public bool BaseScale()
        {
            bool bvar=objClassInterfaceMap.BaseScale();
            return bvar;
        }
        // *************************************************************************** Base scale

        // CenterMapToXY ************************************************************************
        // Center the map on pozition XY (Mercator)

        public bool CenterMapToXY(double x, double y)
        {
            bool bvar=objClassInterfaceMap.CenterMapToXY(x,y);
            return bvar;
        }
        // ************************************************************************ CenterMapToXY

        // CenterMapToLatLong *******************************************************************
        // Center the map on pozition Lat,Long (degree)

        public bool CenterMapToLatLong(double Lat, double Long)
        {
            bool bvar=objClassInterfaceMap.CenterMapToLatLong(Lat,Long);
            return bvar;
        }
        // ******************************************************************* CenterMapToLatLong

        // DrawImage ****************************************************************************
        // Draw Image with path to image

        public string DrawImage(
                       // Degree
                       double Lat,
                       double Long,
                       // Path to image
                       String s1,
                       // Title
                       String s,
                       // Scale of image
                       double scl
                      )
        {

            string svar=objClassInterfaceMap.DrawImage(Lat, Long, s1, s, scl);
            return svar;
        }
        // **************************************************************************** DrawImage

        // Rotate image ************************************************************************
        public Bitmap RotateImage1(Bitmap input, double angle)
        {
            Bitmap svar = objClassInterfaceMap.RotateImage1(input,angle);
            return svar;
        }
        // *********************************************************************** Rotate image

        // DrawImageRotate ********************************************************************
        // Draw Image with rotate

        public string DrawImageRotate(
                       // Degree
                       double Lat,
                       double Long,
                       // Path to image
                       String s1,
                       // Angle of rotate, degree
                       double angle,
                       // Title
                       String s,
                       // Scale of image
                       double scl
                      )
        {

            string svar = objClassInterfaceMap.DrawImageRotate(Lat, Long, s1, angle, s, scl,0);
            return svar;
        }
        // ******************************************************************** DrawImageWithRotate

        // Draw PolygonXY ***********************************************************************
        // Draw Polygon: points.X, points.Y -> Mercator

        public string DrawPolygonXY(List<Point> points, byte Color1, byte Color2, byte Color3, byte Color4)

        {
            string svar=objClassInterfaceMap.DrawPolygonXY(points, Color1, Color2, Color3, Color4);
            return svar;

        } // DrawPolygonXY
        // *********************************************************************** Draw PolygonXY

        // Draw PolygonLatLong ******************************************************************
        // Draw Polygon: points.X=Long, points.Y=Lat -> degree

        public string DrawPolygonLatLong(List<Mapsui.Geometries.Point> pointPel, byte Color1, byte Color2, byte Color3, byte Color4)
        {
            string svar = objClassInterfaceMap.DrawPolygonLatLong(pointPel, Color1, Color2, Color3, Color4);
            return svar;

        } // DrawPolygonLatLong
        // ****************************************************************** Draw PolygonLatLong

        // Draw LinesLatLong ******************************************************************
        // Draw Polygon: points.X=Long, points.Y=Lat -> degree

        public string DrawLinesLatLong(List<Mapsui.Geometries.Point> pointPel, byte Color1, byte Color2, byte Color3, byte Color4)
        {
            string svar = objClassInterfaceMap.DrawLinesLatLong(pointPel, Color1, Color2, Color3, Color4);
            return svar;

        } // DrawPolygonLatLong
        // ****************************************************************** Draw PolygonLatLong

        // DrawSector ************************************************************************
        // Sector

        public string DrawSectorXY(
                                   Point tpCenterPoint,
                                   // Color
                                   byte clr1,
                                   byte clr2,
                                   byte clr3,
                                   byte clr4,
                                   // m
                                   long iRadiusZone,
                                   // degree
                                   float SectorLeft,
                                   float SectorRight
                                   )
        {
            var svar=objClassInterfaceMap.DrawSectorXY(tpCenterPoint,clr1,clr2,clr3,clr4,iRadiusZone,SectorLeft,SectorRight);
            return svar;
        }
        // ************************************************************************ DrawSector

        // DrawSector ************************************************************************
        // Draw Sector LatLong: tpCenterPoint.X=Long, tpCenterPoint.Y=Lat -> degree

        public string DrawSectorLatLong(
                                   // Degree
                                   Mapsui.Geometries.Point tpCenterPoint,
                                   // Color
                                   byte clr1,
                                   byte clr2,
                                   byte clr3,
                                   byte clr4,
                                   // m
                                   long iRadiusZone,
                                   // degree
                                   float SectorLeft,
                                   float SectorRight
                                   )
        {
            var svar = objClassInterfaceMap.DrawSectorLatLong(tpCenterPoint, clr1, clr2, clr3, clr4, iRadiusZone, SectorLeft, SectorRight);
            return svar;
        }
        // ************************************************************************ DrawSector

        // CalculationAzimuth ****************************************************************
        // Calculation of azimuth from Point1 to Point2

        public double CalcAzimuth(double Lat1, double Long1, double Lat2, double Long2)
        {
            var Azimuth = objClassInterfaceMap.CalcAzimuth(Lat1, Long1, Lat2, Long2);
            return Azimuth;
        }


        // **************************************************************** CalculationAzimuth

        // DrawCurrentCoordinates ************************************************************
        // Нарисовать текущие координаты в зависимости от установленной в ini файле СК и 
        // формата координат

            public void DrawCurrentCoordinates()
            {
            // -------------------------------------------------------------------------------------------------
            long ichislo1 = 0;
            double dchislo1 = 0;
            long ichislo2 = 0;
            double dchislo2 = 0;
            long ichislo3 = 0;
            double dchislo3 = 0;

            int ilatgrad = 0;
            double dlatmin = 0;
            int ilatmin = 0;
            long Llatmin = 0;
            double dlatsec = 0;
            int ilatsec = 0;
            long Llatsec = 0;

            int ilongrad = 0;
            double dlonmin = 0;
            int ilonmin = 0;
            long Llonmin = 0;
            double dlonsec = 0;
            int ilonsec = 0;
            long Llonsec = 0;

            // -------------------------------------------------------------------------------------------------
            // INI

            string s = "";
            InitMapYaml initMapYaml = new InitMapYaml();
            s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
            // -------------------------------------------------------------------------------------------------
            // WGS84

                // Latitude
                ichislo1 = (long)(GlobalVarMap.LAT_Rastr * 1000000);
                dchislo1 = ((double)ichislo1) / 1000000;
                // Longitude
                ichislo2 = (long)(GlobalVarMap.LONG_Rastr * 1000000);
                dchislo2 = ((double)ichislo2) / 1000000;
                // Altitude
                ichislo3 = (long)(GlobalVarMap.H_Rastr * 1000000);
                dchislo3 = ((double)ichislo3) / 1000000;

                if(GlobalVarMap.FlagLanguageMap==false)
                 Txt5.Text = Convert.ToString(dchislo3) + " m";
                else
                 Txt5.Text = Convert.ToString(dchislo3) + " м";

            // ...................................................................................................
            // WGS 84

            if (initMapYaml.SysCoord == 0)
            {
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD.dddddd

                if (initMapYaml.FormatCoord == 0)
                {
                    Txt1.Text = "N " + Convert.ToString(dchislo1) + '\xB0'; // Lat
                    Txt3.Text = "E " + Convert.ToString(dchislo2) + '\xB0'; // Long
                }
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD MM.mmmm

                else if (initMapYaml.FormatCoord == 1)
                {
                    // Lat
                    ClassGeoCalculator.f_Grad_GM(
                                                  dchislo1,
                                                  ref ilatgrad,
                                                  ref dlatmin
                                                );
                    Llatmin = (long)(dlatmin * 10000);
                    dlatmin = ((double)Llatmin) / 10000;
                    Txt1.Text = "N " + Convert.ToString(ilatgrad) + '\xB0' + Convert.ToString(dlatmin) + "'"; // Lat

                    // Long
                    ClassGeoCalculator.f_Grad_GM(
                                                  dchislo2,
                                                  ref ilongrad,
                                                  ref dlonmin
                                                );
                    Llonmin = (long)(dlonmin * 10000);
                    dlonmin = ((double)Llonmin) / 10000;
                    Txt3.Text = "E " + Convert.ToString(ilongrad) + '\xB0' + Convert.ToString(dlonmin) + "'"; // Lon

                } // DD MM.mmmm
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD MM SS.ss

                else 
                {
                    // Lat
                    ClassGeoCalculator.f_Grad_GMS(
                                                  dchislo1,
                                                  ref ilatgrad,
                                                  ref ilatmin,
                                                  ref dlatsec
                                                );
                    Llatsec = (long)(dlatsec * 100);
                    dlatsec = ((double)Llatsec) / 100;
                    Txt1.Text = "N " + Convert.ToString(ilatgrad) + '\xB0' + Convert.ToString(ilatmin) + "'" + Convert.ToString(dlatsec) + "''"; // Lat

                    // Long
                    ClassGeoCalculator.f_Grad_GMS(
                                                  dchislo2,
                                                  ref ilongrad,
                                                  ref ilonmin,
                                                  ref dlonsec
                                                );
                    Llonsec = (long)(dlonsec * 100);
                    dlonsec = ((double)Llonsec) / 100;
                    Txt3.Text = "E " + Convert.ToString(ilongrad) + '\xB0' + Convert.ToString(ilonmin) + "'" + Convert.ToString(dlonsec) + "''"; // Lat

                } // DD MM SS.ss
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            } // WGS84
              // ...................................................................................................
              // SK42,m

            else if (initMapYaml.SysCoord == 2)
             {
                  double xxx = 0;
                  double yyy = 0;
                  ClassGeoCalculator.f_WGS84_Krug(GlobalVarMap.LAT_Rastr,
                                                  GlobalVarMap.LONG_Rastr,
                                                  ref xxx, ref yyy);

                if (GlobalVarMap.FlagLanguageMap == false)
                    Txt1.Text = "X " + Convert.ToString((int)xxx) + " m";
                else
                    Txt1.Text = "X " + Convert.ToString((int)xxx) + " м";

                if (GlobalVarMap.FlagLanguageMap == false)
                    Txt3.Text = "Y " + Convert.ToString((int)yyy) + " m";
                else
                    Txt3.Text = "Y " + Convert.ToString((int)yyy) + " м";


            } // SK42,m
              // ...................................................................................................
              // SK42,grad

            else if (initMapYaml.SysCoord == 1)
            {
                double xxx = 0;
                double yyy = 0;
                ClassGeoCalculator.f_WGS84_SK42_BL(GlobalVarMap.LAT_Rastr,
                                                 GlobalVarMap.LONG_Rastr,
                                                 ref xxx, ref yyy);
                // Latitude
                ichislo1 = (long)(xxx * 1000000);
                dchislo1 = ((double)ichislo1) / 1000000;
                // Longitude
                ichislo2 = (long)(yyy * 1000000);
                dchislo2 = ((double)ichislo2) / 1000000;

                //Txt1.Text = "N " + Convert.ToString(dchislo1) + '\xB0';
                //Txt3.Text = "E " + Convert.ToString(dchislo2) + '\xB0';

                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD.dddddd

                if (initMapYaml.FormatCoord == 0)
                {
                    Txt1.Text = "N " + Convert.ToString(dchislo1) + '\xB0'; // Lat
                    Txt3.Text = "E " + Convert.ToString(dchislo2) + '\xB0'; // Long
                }
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD MM.mmmm

                else if (initMapYaml.FormatCoord == 1)
                {
                    // Lat
                    ClassGeoCalculator.f_Grad_GM(
                                                  dchislo1,
                                                  ref ilatgrad,
                                                  ref dlatmin
                                                );
                    Llatmin = (long)(dlatmin * 10000);
                    dlatmin = ((double)Llatmin) / 10000;
                    Txt1.Text = "N " + Convert.ToString(ilatgrad) + '\xB0' + Convert.ToString(dlatmin) + "'"; // Lat

                    // Long
                    ClassGeoCalculator.f_Grad_GM(
                                                  dchislo2,
                                                  ref ilongrad,
                                                  ref dlonmin
                                                );
                    Llonmin = (long)(dlonmin * 10000);
                    dlonmin = ((double)Llonmin) / 10000;
                    Txt3.Text = "E " + Convert.ToString(ilongrad) + '\xB0' + Convert.ToString(dlonmin) + "'"; // Lon

                } // DD MM.mmmm
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD MM SS.ss

                else
                {
                    // Lat
                    ClassGeoCalculator.f_Grad_GMS(
                                                  dchislo1,
                                                  ref ilatgrad,
                                                  ref ilatmin,
                                                  ref dlatsec
                                                );
                    Llatsec = (long)(dlatsec * 100);
                    dlatsec = ((double)Llatsec) / 100;
                    Txt1.Text = "N " + Convert.ToString(ilatgrad) + '\xB0' + Convert.ToString(ilatmin) + "'" + Convert.ToString(dlatsec) + "''"; // Lat

                    // Long
                    ClassGeoCalculator.f_Grad_GMS(
                                                  dchislo2,
                                                  ref ilongrad,
                                                  ref ilonmin,
                                                  ref dlonsec
                                                );
                    Llonsec = (long)(dlonsec * 100);
                    dlonsec = ((double)Llonsec) / 100;
                    Txt3.Text = "E " + Convert.ToString(ilongrad) + '\xB0' + Convert.ToString(ilonmin) + "'" + Convert.ToString(dlonsec) + "''"; // Lat

                } // DD MM SS.ss
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            } // SK42,grad
              // ...................................................................................................
              // Mercator

            else if (initMapYaml.SysCoord == 3)
            {
                double xxx = 0;
                double yyy = 0;
                ClassGeoCalculator.f_WGS84_Mercator(GlobalVarMap.LAT_Rastr,
                                                    GlobalVarMap.LONG_Rastr,
                                                    ref xxx, ref yyy);

                if (GlobalVarMap.FlagLanguageMap == false)
                {
                    Txt1.Text = "X " + Convert.ToString((int)xxx) + " m";
                    Txt3.Text = "Y " + Convert.ToString((int)yyy) + " m";
                }
                else
                {
                    Txt1.Text = "X " + Convert.ToString((int)xxx) + " м";
                    Txt3.Text = "Y " + Convert.ToString((int)yyy) + " м";
                }

            } // Mercator
            // ...................................................................................................

            // -------------------------------------------------------------------------------------------------
            try
            {
                    GlobalVarMap.Str_Rastr = ClassGeoCalculator.f_WGS84_MGRS(GlobalVarMap.LAT_Rastr, GlobalVarMap.LONG_Rastr);
                    Txt7.Text = "MGRS: " + GlobalVarMap.Str_Rastr;
            }
            catch
            {
                if (GlobalVarMap.FlagLanguageMap == false)
                    MessageBox.Show("Invalid coordinates");
                else
                    MessageBox.Show("Недопустимые координаты");

                return;
            }
            // -------------------------------------------------------------------------------------------------

            }
        // ************************************************************ DrawCurrentCoordinates

        // ChangeLanguage ********************************************************************************
        // AAA
        // Флаг языка: по умолчанию английский (false)
        // true  -> русский
        //FlagLanguageMap = false;


        public void fChangeLanguage()
        {
            // Переход на русский **********************************************************************

            if (GlobalVarMap.FlagLanguageMap == false)
            {
                GlobalVarMap.FlagLanguageMap = true;
            }
            // ********************************************************************** Переход на русский

            // Переход на английский *******************************************************************

            else
            {

                GlobalVarMap.FlagLanguageMap = false;

            }
            // ******************************************************************* Переход на английский

        }
        // ******************************************************************************** ChangeLanguage

        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! INTERFACE



    } // Class
} // Namespace
