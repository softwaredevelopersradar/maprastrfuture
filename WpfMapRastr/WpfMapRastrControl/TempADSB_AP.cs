﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ModelsTablesDBLib;


namespace WpfMapRastrControl
{
    public class TempADSB_AP: TempADSB
    {
        // 033
        // 11_13
        public double LatitudePrev=-1;
        public double LongitudePrev = -1;
        public double Azimuth = 0;
        public double AzimuthPrev = 0;
        public string ICAO = "";
        public bool fdraw = false;
        public double S_traj = 0;

        //11_13new
        // 0->серый, 1->оранжевый
        public int flColor = 0;

        public List<Objects> list_air_traj = new List<Objects>();


    } // Class
} // Namespase
