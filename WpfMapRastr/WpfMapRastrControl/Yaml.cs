﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;

namespace WpfMapRastrControl
{
    public partial class ClassMapRastrReDraw
    {
        // T YamlLoad *******************************************************************************

        public T YamlLoad<T>(string NameDotYaml) where T : new()
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader(NameDotYaml, System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            var deserializer = new DeserializerBuilder().Build();

            var t = new T();
            try
            {
                t = deserializer.Deserialize<T>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            //if (t == null)
            //{
            //    t = new T();
            //    YamlSave(t, NameDotYaml);
            //}
            return t;
        }
        // ******************************************************************************* T YamlLoad

        // YamlSave<T> ******************************************************************************

        public void YamlSave<T>(T t, string NameDotYaml) where T : new()
        {
            try
            {
                var serializer = new SerializerBuilder().EmitDefaults().Build();
                var yaml = serializer.Serialize(t);

                using (StreamWriter sw = new StreamWriter(NameDotYaml, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        // ****************************************************************************** YamlSave<T>

        // VARS *************************************************************************************

        public class GlobalVarMap4Yaml
        {
            // Antennas 

            // Сектора антенн (grad)
            public double sect_RRS1 { get; set; }
            public double sect_RRS2 { get; set; }
            public double sect_LPA13 { get; set; }
            public double sect_LPA24 { get; set; }
            public double sect_LPA510 { get; set; }
            public double sect_Radius { get; set; }

            public GlobalVarMap4Yaml()
            {
                sect_RRS1 = 7;
                sect_RRS2 = 7;
                sect_LPA13 = 120;
                sect_LPA24 = 120;
                sect_LPA510 = 120;
                sect_Radius = 10000;
            }
        }
        // ************************************************************************************* VARS

    } // Class
} // Namespace
