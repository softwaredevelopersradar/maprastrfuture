﻿using System.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using System.Drawing;
using System.Runtime.InteropServices;
using System.IO;
using WpfMapControl;
using System.Data;
using System.Reflection;
using System.ServiceModel;
using System.Diagnostics;
using System.Threading;
using System.Globalization;
using Microsoft.Win32;

using Bitmap = System.Drawing.Bitmap;
using Point = System.Drawing.Point;

// ???
using Image = System.Drawing.Image;


using ModelsTablesDBLib;


// !!! Model of Map functions
namespace WpfMapRastrControl
{
    public interface InterfaceMap
    {
        // Properties ******************************************************************

        // Map .........................................................................
        // =1 -> map is opened
        bool MapOpened { get; }
        // =1 -> the height matrix is opened
        bool MatrixOpened { get; }
        // get scale
        double Scale { get; }
        // ......................................................................... Map

        // JS ..........................................................................
        //List<TableASP> List_JS { get; set; }
        List<ClassJS> List_JS { get; set; }

        // =1 -> draw JS
        bool ShowJS { get; set; }

        // =1 -> draw AntennasDirections
        bool ShowAntennas { get; set; }

        // .......................................................................... JS

        // Objects .....................................................................

        // Objects1 (ИРИ ФРЧ)
        //List<TempFWS> List_SRW_FRF { get; set; }
        List<Class_IRIFRCH> List_SRW_FRF { get; set; }
        // =1 -> draw Objects1
        bool Show_SRW_FRF { get; set; }

        // Objects2 (ИРИ ФРЧ ЦР)
        List<TableReconFWS> List_SRW_FRF_TD { get; set; }
        // =1 -> draw Objects2
        bool Show_SRW_FRF_TD { get; set; }

        // Objects3 (ИРИ ФРЧ РП)
        List<TableSuppressFWS> List_SRW_FRF_RS { get; set; }
        // =1 -> draw Objects3
        bool Show_SRW_FRF_RS { get; set; }

        // Objects4 (ИРИ ППРЧ)
        List<TableReconFHSS> List_SRW_STRF_Recon { get; set; }
        List<Class_IRIPPRCH> List_SRW_STRF { get; set; }
        // For Coordinates
        List<TableSourceFHSS> List_SRW_STRF_Coord { get; set; }
        // =1 -> draw Objects4
        bool Show_SRW_STRF { get; set; }

        // Objects5 (ИРИ ППРЧ РП)
        List<TableSuppressFHSS> List_SRW_STRF_RS { get; set; }
        // =1 -> draw Objects5
        bool Show_SRW_STRF_RS { get; set; }

        // Airplanes
        // 11_13
        //List<TempADSB> List_AP { get; set; }
        List<TempADSB_AP> List_AP { get; set; }

        // =1 -> draw Objects5
        bool Show_AP { get; set; }

        // ..................................................................... Objects

        // Map coordinates .............................................................

        /*
                // get Lat from Map on mouse click
                double LatMap { get; }
                // get Long from Map on mouse click
                double LongMap { get; }
                // get H from Map on mouse click
                double HMap { get; }
                // get X(Mercator) from Map on mouse click
                double XMap { get; }
                // get Y(Mercator) from Map on mouse click
                double YMap { get; }
        */
        // ............................................................. Map coordinates

        // Azimuth .....................................................................

        // =true -> активна вкладка Азимут в TabControl задач
        bool flAzimuth { get; set; }
        // =true -> отображать значение азимута около станций
        bool CheckShowAzimuth { get; set; }
        // Coordinates of source
        double LatSource_Azimuth { get; set; }
        double LongSource_Azimuth { get; set; }
        double HSource_Azimuth { get; set; }

        // ..................................................................... Azimuth

        // ****************************************************************** Properties

        // Methods **********************************************************************

        // ..............................................................................
        // Open Map (pathMap - path to map: from dialog or from ini file)
        void OpenMap(string pathMap);

        // Open the height matrix (path - path to matrix: from dialog or from ini file)
        void OpenMatrix(string pathMatrix);

        // Set scale
        void SetScale(double scale);

        // Close Map
        void CloseMap();

        // Close the height matrix
        void CloseMatrix();

        // Increase the scale 
        bool IncreaseScale();

        // Decrease the scale 
        bool DecreaseScale();

        // Base scale 
        bool BaseScale();

        // Center map on pozition XY (For Mercator Map)
        bool CenterMapToXY(double x, double y);

        // Center map on pozition lat,long (degree) for geographic Map
        bool CenterMapToLatLong(double Lat, double Long);

        // Calculation of azimuth from Point1 to Point2
        double CalcAzimuth(double Lat1,double Long1,double Lat2,double Long2);
        // ..............................................................................
        // DRAW

        // Draw Image with path to image
        string DrawImage(
                       // Degree (for Geographic Map)
                       // Long=X, Lat=Y for Mercator Map
                       double Lat,
                       double Long,
                       // Path to image
                       String s1,
                       // Title
                       String s,
                       // Scale of image
                       double scl
                      );


        // Rotate image
        Bitmap RotateImage1(Bitmap input, double angle);

        // Draw Image with rotate
        string DrawImageRotate(
                       // Degree (for Geographic Map)
                       // Long=X, Lat=Y for Mercator Map
                       double Lat,
                       double Long,
                       // Path to image
                       String s1,
                       // Angle of rotate, degree
                       double angle,
                       // Title
                       String s,
                       // Scale of image
                       double scl,
                       byte Select
                      );

        // Draw Polygon: points.X, points.Y -> Mercator
        string DrawPolygonXY(List<Point> points, byte Color1, byte Color2, byte Color3, byte Color4);
        // Draw Polygon: points.X=Long, points.Y=Lat -> degree
        string DrawPolygonLatLong(List<Mapsui.Geometries.Point> points, byte Color1, byte Color2, byte Color3, byte Color4);
        // Draw Sector XY
        string DrawSectorXY(
                            // Mercator
                            Point tpCenterPoint,
                            // Color
                            byte clr1,
                            byte clr2,
                            byte clr3,
                            byte clr4,
                            // m
                            long iRadiusZone,
                            // degree
                            float SectorLeft,
                            float SectorRight
                           );
        // Draw Sector LatLong: tpCenterPoint.X=Long, tpCenterPoint.Y=Lat -> degree
        string DrawSectorLatLong(
                            // Degree
                            Mapsui.Geometries.Point tpCenterPoint,
                            // Color
                            byte clr1,
                            byte clr2,
                            byte clr3,
                            byte clr4,
                            // m
                            long iRadiusZone,
                            // degree
                            float SectorLeft,
                            float SectorRight
                           );

        // Draw Lines: points.X=Long, points.Y=Lat -> degree(for geographic map)
        // points.X=X, points.Y=Y -> for Mercator map
        string DrawLinesLatLong(List<Mapsui.Geometries.Point> points, byte Color1, byte Color2, byte Color3, byte Color4);

        // ..............................................................................


        // ********************************************************************** Methods


    } // Interface
} // Namespace
