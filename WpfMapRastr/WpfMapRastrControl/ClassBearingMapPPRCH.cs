﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfMapRastrControl
{
    public class ClassBearingMapPPRCH
    {

        public double Bearing = 0;
        public int NumberASP = 0;
        public double Latitude = 0;
        public double Longitude = 0;

        // Точки пеленга
        public List<Objects> list_bearing = new List<Objects>();

    } // Class
} // NameSpace
