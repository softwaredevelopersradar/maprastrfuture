﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfMapRastrControl
{
    // Route
    // Маршрут
    public class ClassRoute
    {
        // Название маршрута
        public string Name_Route = "";
        // Длина маршрута
        public double Length_Route = 0;

        public List<Objects> list_Points_Route = new List<Objects>();

    } // Class
} // Namespace
