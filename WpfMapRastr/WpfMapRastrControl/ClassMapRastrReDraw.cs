﻿using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.IO;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.Windows.Shapes;

using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Navigation;

using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;

using Bitmap = System.Drawing.Bitmap;
using Point = System.Drawing.Point;

// ???
using Image = System.Drawing.Image;
using Icon = System.Drawing.Icon;

using ModelsTablesDBLib;

// Для YAML реализации
using YamlDotNet.Serialization;
using ClassLibraryIniFiles;

using GeoCalculator;
using Bearing;

//using GlobalVarMap4Yaml;
namespace WpfMapRastrControl
{
    public partial class ClassMapRastrReDraw
    {

        // Element for MAP
        private RasterMapControl objRasterMapControl;
        // Class for Interface
        private ClassInterfaceMap objClassInterfaceMap;

        public ClassMapRastrReDraw(RasterMapControl objRasterMapControl1, ClassInterfaceMap objClassInterfaceMap1)
        {
            objRasterMapControl = objRasterMapControl1;
            objClassInterfaceMap = objClassInterfaceMap1;
        }

        // ReDrawMapAll ***********************************************************************
        // Returns path to map from .yaml file/"" - in Setting.json/Setting.yaml/Setting.xml)

        public void ReDrawMapAll()
        {
            // -------------------------------------------------------------------------------
            // Delete all
            if(objRasterMapControl.MapObjects.Count > 0)
            objRasterMapControl.RemoveAllObjects();

            // -------------------------------------------------------------------------------
            // Azimuth

            ReDrawAzimuthNew();
            // -------------------------------------------------------------------------------
            // JS

            ReDrawJS();
            // -------------------------------------------------------------------------------
            // Objects1 (ИРИ ФРЧ)

            ReDraw_SRW_FRF();
            // -------------------------------------------------------------------------------
            // Пеленги ИРИ ФРЧ

            ReDraw_Pelengs_IRIFRCH();
            // -------------------------------------------------------------------------------
            // Objects2 (ИРИ ФРЧ ЦР)

            ReDraw_SRW_FRF_TD();
            // -------------------------------------------------------------------------------
            // Objects3 (ИРИ ФРЧ РП)

            ReDraw_SRW_FRF_RS();
            // -------------------------------------------------------------------------------
            // Objects4 (ИРИ ППРЧ)

            ReDraw_SRW_STRF();
            // -------------------------------------------------------------------------------
            // Пеленги ИРИ ФРЧ

            ReDraw_Pelengs_IRIPPRCH();
            // -------------------------------------------------------------------------------
            // Objects5 (ИРИ ППРЧ РП)

            ReDraw_SRW_STRF_RS();
            // -------------------------------------------------------------------------------
            // Airplanes

            ReDraw_AP();
            // -------------------------------------------------------------------------------
            // Azimuth
            
            //ReDrawAzimuthNew();

            // -------------------------------------------------------------------------------

        }
        // *********************************************************************** ReDrawMapAll

        // Azimuth **************************************************************************** 
        // 999

        public void ReDrawAzimuth(
                                  bool flAz,
                                  bool checkAz,
                                  double latSource,
                                  double lonSource,
                                  List<double> listAzimuth
                                 )
        {
        }
        // ****************************************************************************  Azimuth

        // AzimuthNew **************************************************************************

        public void ReDrawAzimuthNew(
                                    )
        {

            //List<ClassJS> List_JS1 = new List<ClassJS>(objClassInterfaceMap.List_JS);

            if (
                (objClassInterfaceMap.ShowJS == true)&&
                (objClassInterfaceMap.flAzimuth==true) &&
                (objClassInterfaceMap.LatSource_Azimuth!=-1)&&
                (objClassInterfaceMap.LongSource_Azimuth!=-1)
               )
            {
                // ----------------------------------------------------------------------------------------
                double az = 0;
                string s3 = "";
                string s1 = "";
                string s6 = "";
                string s7 = "";
                string s9 = "";
                long ichislo = 0;
                double dchislo = 0;


                // Get current dir
                string dir = "";
                dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                // -------------------------------------------------------------------------------------------------
                // MercatorMap or GeographicMap?

                string s5 = "";
                InitMapYaml initMapYaml = new InitMapYaml();
                s5 = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
                // -------------------------------------------------------------------------------------------------

                // SOURCE ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

                // .............................................................................................
                // GeographicMap

                if (initMapYaml.TypeMap == 1)
                {
                    s1 = objClassInterfaceMap.DrawImage(
                                                objClassInterfaceMap.LatSource_Azimuth,
                                                objClassInterfaceMap.LongSource_Azimuth,
                                                // Path to image
                                                dir + "\\Images\\OtherObject\\" + "SquareRed.png",
                                                // Text
                                                "",
                                                // Scale
                                                0.6
                                     );
                } // GeographicMap
                  // .............................................................................................
                  // MercatorMap

                else
                {
                    double xcntr = 0;
                    double ycntr = 0;
                    var p = Mercator.FromLonLat(objClassInterfaceMap.LongSource_Azimuth, objClassInterfaceMap.LatSource_Azimuth);
                    xcntr = p.X;
                    ycntr = p.Y;

                    s1 = objClassInterfaceMap.DrawImage(
                                             ycntr,
                                             xcntr,
                                             // Path to image
                                             dir + "\\Images\\OtherObject\\" + "SquareRed.png",
                                             // Text
                                             "",
                                             // Scale
                                             0.6
                                  );

                } // MercatorMap
                  // .............................................................................................

                // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ SOURCE

                // FOR_JS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                for (int i = 0; i < objClassInterfaceMap.List_JS.Count; i++)
                {

                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // Calculate Azimuth

                    az = objClassInterfaceMap.CalcAzimuth(objClassInterfaceMap.List_JS[i].Coordinates.Latitude,
                                                          objClassInterfaceMap.List_JS[i].Coordinates.Longitude,
                                                          objClassInterfaceMap.LatSource_Azimuth,
                                                          objClassInterfaceMap.LongSource_Azimuth);

                    ichislo = (long)(az * 10);
                    dchislo = ((double)ichislo) / 10;
                    az = dchislo;

                    // ***
                    objClassInterfaceMap.List_JS[i].Azimuth = az;
                    // ------------------------------------------------------------------------------------------


                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // Line from object to station

                    List<Mapsui.Geometries.Point> pointPel = new List<Mapsui.Geometries.Point>();

                    // .............................................................................................
                    // GeographicMap

                    if (initMapYaml.TypeMap == 1)
                    {
                        pointPel.Add(new Mapsui.Geometries.Point(objClassInterfaceMap.List_JS[i].Coordinates.Longitude, objClassInterfaceMap.List_JS[i].Coordinates.Latitude));
                        pointPel.Add(new Mapsui.Geometries.Point(objClassInterfaceMap.LongSource_Azimuth, objClassInterfaceMap.LatSource_Azimuth));
                        s6 = objClassInterfaceMap.DrawLinesLatLong(
                                                            // Draw Lines: points.X=Long, points.Y=Lat -> degree(for geographic map)
                                                            // points.X=X, points.Y=Y -> for Mercator map
                                                            pointPel,
                                                            // Color
                                                            100,
                                                            255,
                                                            0,
                                                            0
                                                           );
                    } // GeographicMap
                      // .............................................................................................
                      // MercatorMap

                    else
                    {
                        double xcntr1 = 0;
                        double ycntr1 = 0;
                        double xcntrJS = 0;
                        double ycntrJS = 0;

                        var p = Mercator.FromLonLat(objClassInterfaceMap.LongSource_Azimuth, objClassInterfaceMap.LatSource_Azimuth);
                        xcntr1 = p.X;
                        ycntr1 = p.Y;
                        var p1 = Mercator.FromLonLat(objClassInterfaceMap.List_JS[i].Coordinates.Longitude, objClassInterfaceMap.List_JS[i].Coordinates.Latitude);
                        xcntrJS = p1.X;
                        ycntrJS = p1.Y;

                        pointPel.Add(new Mapsui.Geometries.Point(xcntrJS, ycntrJS));
                        pointPel.Add(new Mapsui.Geometries.Point(xcntr1, ycntr1));
                        s6 = objClassInterfaceMap.DrawLinesLatLong(
                                                            // Draw Lines: points.X=Long, points.Y=Lat -> degree(for geographic map)
                                                            // points.X=X, points.Y=Y -> for Mercator map
                                                            pointPel,
                                                            // Color
                                                            100,
                                                            255,
                                                            0,
                                                            0
                                                           );

                    } // MercatorMap
                      // .............................................................................................

                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // JS+Title

                    // 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
                    if (objClassInterfaceMap.CheckShowAzimuth == true)
                    {
                        // .............................................................................................
                        // GeographicMap

                        if (initMapYaml.TypeMap == 1)
                        {
                            s7 = objClassInterfaceMap.DrawImage(
                                                     objClassInterfaceMap.List_JS[i].Coordinates.Latitude,
                                                     objClassInterfaceMap.List_JS[i].Coordinates.Longitude,
                                                     // Path to image
                                                     dir + "\\Images\\Jammer\\" + "1.png",
                                                     // Text
                                                     Convert.ToString(az)+ objClassInterfaceMap.List_JS[i].Caption,

                                                     // Scale
                                                     2
                                          );
                        } // GeographicMap
                          // .............................................................................................
                          // MercatorMap

                        else
                        {
                            double xcntr2 = 0;
                            double ycntr2 = 0;
                            var p = Mercator.FromLonLat(objClassInterfaceMap.List_JS[i].Coordinates.Longitude, objClassInterfaceMap.List_JS[i].Coordinates.Latitude);
                            xcntr2 = p.X;
                            ycntr2 = p.Y;

                            s7 = objClassInterfaceMap.DrawImage(
                                                     ycntr2,
                                                     xcntr2,
                                                     // Path to image
                                                     dir + "\\Images\\Jammer\\" + "1.png",
                                                     // Text
                                                     Convert.ToString(az)+ objClassInterfaceMap.List_JS[i].Caption,
                                                     // Scale
                                                     2
                                          );

                        } // MercatorMap
                          // .............................................................................................

                    } // objClassInterfaceMap.CheckShowAzimuth == true
                    // 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111

                    // 222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222
                    // objClassInterfaceMap.CheckShowAzimuth == false

                    else
                    {
                        // .............................................................................................
                        // GeographicMap

                        if (initMapYaml.TypeMap == 1)
                        {
                            s9 = objClassInterfaceMap.DrawImage(
                                                     objClassInterfaceMap.List_JS[i].Coordinates.Latitude,
                                                     objClassInterfaceMap.List_JS[i].Coordinates.Longitude,
                                                     // Path to image
                                                     dir + "\\Images\\Jammer\\" + "1.png",
                                                     // Text
                                                     objClassInterfaceMap.List_JS[i].Caption,
                                                     // Scale
                                                     2
                                          );

                        } // GeographicMap
                          // .............................................................................................
                          // MercatorMap

                        else
                        {
                            double xcntr3 = 0;
                            double ycntr3 = 0;
                            var p = Mercator.FromLonLat(objClassInterfaceMap.List_JS[i].Coordinates.Longitude, objClassInterfaceMap.List_JS[i].Coordinates.Latitude);
                            xcntr3 = p.X;
                            ycntr3 = p.Y;

                            s9 = objClassInterfaceMap.DrawImage(
                                                     ycntr3,
                                                     xcntr3,
                                                     // Path to image
                                                     dir + "\\Images\\Jammer\\" + "1.png",
                                                     // Text
                                                     objClassInterfaceMap.List_JS[i].Caption,
                                                     // Scale
                                                     2
                                          );

                        } // MercatorMap
                          // .............................................................................................

                    } // objClassInterfaceMap.CheckShowAzimuth == false
                    // 222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222

                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                } // FOR JS
                // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ FOR_JS


            } // IF(.ShowJS==true & flAzimuth=true & LatSource_Azimuth!=-1 & LongSource_Azimuth!=-1)

        }
        // *************************************************************************  AzimuthNew

        // JS *********************************************************************************
        // ReDraw JS

        public void ReDrawJS()
        {

            List<ClassJS> List_JS1 = new List<ClassJS>(objClassInterfaceMap.List_JS);

            if (objClassInterfaceMap.ShowJS == true)
            {

                try
                {
                    string s = "";

                    if (List_JS1.Count == 0)
                    {
                        // MessageBox.Show("JS not detected");
                        return;
                    }

                    // Get current dir
                    string dir = "";
                    dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                    // -------------------------------------------------------------------------------------------------
                    // MercatorMap or GeographicMap?

                    string s1 = "";
                    InitMapYaml initMapYaml = new InitMapYaml();
                    s1 = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
                    // -------------------------------------------------------------------------------------------------


                    GlobalVarMap4Yaml globalvarmap4Yaml = YamlLoad<GlobalVarMap4Yaml>("SectorSettings.yaml");


                    GlobalVarMap.sect_LPA13 = globalvarmap4Yaml.sect_LPA13;
                    GlobalVarMap.sect_LPA24 = globalvarmap4Yaml.sect_LPA24;
                    GlobalVarMap.sect_LPA510 = globalvarmap4Yaml.sect_LPA510;
                    GlobalVarMap.sect_RRS1 = globalvarmap4Yaml.sect_RRS1;
                    GlobalVarMap.sect_RRS2 = globalvarmap4Yaml.sect_RRS2;
                    GlobalVarMap.rant = (int)globalvarmap4Yaml.sect_Radius;
                    // -------------------------------------------------------------------------------------------------

                    for (int i = 0; i < List_JS1.Count; i++)
                    {

/*
                        if(List_JS1[i].ISOwn==true)
                        {
                            GlobalVarMap.sect_LPA13 = List_JS1[i].LPA13;
                            GlobalVarMap.sect_LPA24 = List_JS1[i].LPA24;
                            GlobalVarMap.sect_LPA510 = List_JS1[i].LPA510;
                            GlobalVarMap.sect_RRS1 = List_JS1[i].RRS1;
                            GlobalVarMap.sect_RRS2 = List_JS1[i].RRS2;
                        }
                        else
                        {
                            GlobalVarMap.sect_LPA13 = globalvarmap4Yaml.sect_LPA13;
                            GlobalVarMap.sect_LPA24 = globalvarmap4Yaml.sect_LPA24;
                            GlobalVarMap.sect_LPA510 = globalvarmap4Yaml.sect_LPA510;
                            GlobalVarMap.sect_RRS1 = globalvarmap4Yaml.sect_RRS1;
                            GlobalVarMap.sect_RRS2 = globalvarmap4Yaml.sect_RRS2;
                        }
*/
                        // .............................................................................................
                        // GeographicMap

                        if (initMapYaml.TypeMap == 1)
                        {

                            if ((List_JS1[i].Azimuth!= -1)&&(objClassInterfaceMap.CheckShowAzimuth==true))
                            {
                                s = objClassInterfaceMap.DrawImage(
                                                         List_JS1[i].Coordinates.Latitude,
                                                         List_JS1[i].Coordinates.Longitude,
                                                         // Path to image
                                                         dir + "\\Images\\Jammer\\" + "1.png",
                                                         // Text
                                                         Convert.ToString(List_JS1[i].Azimuth) + List_JS1[i].Caption,
                                                         // Scale
                                                         2
                                              );
                            }

                            else
                            {
                                s = objClassInterfaceMap.DrawImage(
                                                         List_JS1[i].Coordinates.Latitude,
                                                         List_JS1[i].Coordinates.Longitude,
                                                         // Path to image
                                                         dir + "\\Images\\Jammer\\" + "1.png",
                                                         // Text
                                                         //"",
                                                         List_JS1[i].Caption,
                                                         // Scale
                                                         2
                                              );
                            }
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // !!! ANTENNAS

                            if (
                                objClassInterfaceMap.ShowAntennas == true
                                )
                            {
                                // ...........................................................................................
                                // LPA13

                                if (List_JS1[i].LPA13 != -1)
                                {
                                    ShowDirectionGeogr(
                                             List_JS1[i].LPA13,
                                             (float)GlobalVarMap.sect_LPA13,
                                             100,
                                             (byte)GlobalVarMap.col1_1,
                                             (byte)GlobalVarMap.col1_2,
                                             (byte)GlobalVarMap.col1_3,
                                             List_JS1[i].Coordinates.Latitude,
                                             List_JS1[i].Coordinates.Longitude,
                                             GlobalVarMap.rant
                                             );
                                }
                                // ...........................................................................................
                                // LPA24

                                // Otl
                                //List_JS1[i].LPA24 = 90;
                                if (List_JS1[i].LPA24 != -1)
                                {
                                    ShowDirectionGeogr(
                                             List_JS1[i].LPA24,
                                             (float)GlobalVarMap.sect_LPA24,
                                             100,
                                             (byte)GlobalVarMap.col2_1,
                                             (byte)GlobalVarMap.col2_2,
                                             (byte)GlobalVarMap.col2_3,
                                             List_JS1[i].Coordinates.Latitude,
                                             List_JS1[i].Coordinates.Longitude,
                                             GlobalVarMap.rant
                                             );

                                }
                                // ...........................................................................................
                                // LPA510

                                // Otl
                                //List_JS1[i].LPA510 = 180;
                                if (List_JS1[i].LPA510 != -1)
                                {
                                    ShowDirectionGeogr(
                                             List_JS1[i].LPA510,
                                             (float)GlobalVarMap.sect_LPA510,
                                             100,
                                             (byte)GlobalVarMap.col3_1,
                                             (byte)GlobalVarMap.col3_2,
                                             (byte)GlobalVarMap.col3_3,
                                             List_JS1[i].Coordinates.Latitude,
                                             List_JS1[i].Coordinates.Longitude,
                                             GlobalVarMap.rant
                                             );
                                }
                                // ...........................................................................................
                                // RRS1

                                // Otl
                                //List_JS1[i].RRS1 = 290;
                                if (List_JS1[i].RRS1 != -1)
                                {
                                    ShowDirectionGeogr(
                                             List_JS1[i].RRS1,
                                             (float)GlobalVarMap.sect_RRS1,
                                             100,
                                             (byte)GlobalVarMap.col4_1,
                                             (byte)GlobalVarMap.col4_2,
                                             (byte)GlobalVarMap.col4_3,
                                             List_JS1[i].Coordinates.Latitude,
                                             List_JS1[i].Coordinates.Longitude,
                                             GlobalVarMap.rant
                                             );
                                }
                                // ...........................................................................................
                                // RRS2

                                // Otl
                                //List_JS1[i].RRS2 = 310;
                                if (List_JS1[i].RRS2 != -1)
                                {
                                    ShowDirectionGeogr(
                                             List_JS1[i].RRS2,
                                             (float)GlobalVarMap.sect_RRS2,
                                             100,
                                             (byte)GlobalVarMap.col5_1,
                                             (byte)GlobalVarMap.col5_2,
                                             (byte)GlobalVarMap.col5_3,
                                             List_JS1[i].Coordinates.Latitude,
                                             List_JS1[i].Coordinates.Longitude,
                                             GlobalVarMap.rant
                                             );
                                }
                                // ...........................................................................................

                            } // objClassInterfaceMap.ShowAntennas == true
                              // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                        } // GeographicMap
                          // .............................................................................................
                          // MercatorMap

                        else
                        {
                            double xcntr = 0;
                            double ycntr = 0;
                            var p = Mercator.FromLonLat(List_JS1[i].Coordinates.Longitude, List_JS1[i].Coordinates.Latitude);
                            xcntr = p.X;
                            ycntr = p.Y;

                            if ((List_JS1[i].Azimuth != -1)&&(objClassInterfaceMap.CheckShowAzimuth == true))
                            {
                                s = objClassInterfaceMap.DrawImage(
                                                     ycntr,
                                                     xcntr,
                                                     // Path to image
                                                     dir + "\\Images\\Jammer\\" + "1.png",
                                                     // Text
                                                     Convert.ToString(List_JS1[i].Azimuth) + List_JS1[i].Caption,
                                                     // Scale
                                                     //2
                                                     2
                                          );
                            }
                            else
                            {
                                s = objClassInterfaceMap.DrawImage(
                                                     ycntr,
                                                     xcntr,
                                                     // Path to image
                                                     dir + "\\Images\\Jammer\\" + "1.png",
                                                     // Text
                                                     List_JS1[i].Caption,
                                                     // Scale
                                                     //2
                                                     2
                                          );
                            }
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // !!! ANTENNAS

                            if (objClassInterfaceMap.ShowAntennas == true)
                            {
                                // ...........................................................................................
                                // LPA13

                                if (List_JS1[i].LPA13 != -1)
                                {
                                    ShowDirection(
                                             List_JS1[i].LPA13,
                                             (float)GlobalVarMap.sect_LPA13,
                                             100,
                                             (byte)GlobalVarMap.col1_1,
                                             (byte)GlobalVarMap.col1_2,
                                             (byte)GlobalVarMap.col1_3,
                                             List_JS1[i].Coordinates.Latitude,
                                             List_JS1[i].Coordinates.Longitude,
                                             GlobalVarMap.rant
                                             );
                                }
                                // ...........................................................................................
                                // LPA24

                                // Otl
                                //List_JS1[i].LPA24 = 90;
                                if (List_JS1[i].LPA24 != -1)
                                {
                                    ShowDirection(
                                             List_JS1[i].LPA24,
                                             (float)GlobalVarMap.sect_LPA24,
                                             100,
                                             (byte)GlobalVarMap.col2_1,
                                             (byte)GlobalVarMap.col2_2,
                                             (byte)GlobalVarMap.col2_3,
                                             List_JS1[i].Coordinates.Latitude,
                                             List_JS1[i].Coordinates.Longitude,
                                             GlobalVarMap.rant
                                             );

                                }
                                // ...........................................................................................
                                // LPA510

                                // Otl
                                //List_JS1[i].LPA510 = 180;
                                if (List_JS1[i].LPA510 != -1)
                                {
                                    ShowDirection(
                                             List_JS1[i].LPA510,
                                             (float)GlobalVarMap.sect_LPA510,
                                             100,
                                             (byte)GlobalVarMap.col3_1,
                                             (byte)GlobalVarMap.col3_2,
                                             (byte)GlobalVarMap.col3_3,
                                             List_JS1[i].Coordinates.Latitude,
                                             List_JS1[i].Coordinates.Longitude,
                                             GlobalVarMap.rant
                                             );
                                }
                                // ...........................................................................................
                                // RRS1

                                // Otl
                                //List_JS1[i].RRS1 = 290;
                                if (List_JS1[i].RRS1 != -1)
                                {
                                    ShowDirection(
                                             List_JS1[i].RRS1,
                                             (float)GlobalVarMap.sect_RRS1,
                                             100,
                                             (byte)GlobalVarMap.col4_1,
                                             (byte)GlobalVarMap.col4_2,
                                             (byte)GlobalVarMap.col4_3,
                                             List_JS1[i].Coordinates.Latitude,
                                             List_JS1[i].Coordinates.Longitude,
                                             GlobalVarMap.rant
                                             );
                                }
                                // ...........................................................................................
                                // RRS2

                                // Otl
                                //List_JS1[i].RRS2 = 310;
                                if (List_JS1[i].RRS2 != -1)
                                {
                                    ShowDirection(
                                             List_JS1[i].RRS2,
                                             (float)GlobalVarMap.sect_RRS2,
                                             100,
                                             (byte)GlobalVarMap.col5_1,
                                             (byte)GlobalVarMap.col5_2,
                                             (byte)GlobalVarMap.col5_3,
                                             List_JS1[i].Coordinates.Latitude,
                                             List_JS1[i].Coordinates.Longitude,
                                             GlobalVarMap.rant
                                             );
                                }
                                // ...........................................................................................

                            } // objClassInterfaceMap.ShowAntennas == true
                              // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


                        } // MercatorMap
                          // .............................................................................................

                        if (s != "")
                        {
                            MessageBox.Show(s);
                        }

                    } // for
                      // -------------------------------------------------------------------------------------------------
                }

                catch
                {
                    if (GlobalVarMap.FlagLanguageMap == false)
                        MessageBox.Show("Draw error");
                    else
                        MessageBox.Show("Ошибка при отрисовке");

                }
            } // IF(objClassInterfaceMap.ShowJS==true)

        }
        // ********************************************************************************* JS

        // ************************************************************************************
        // Отрисовка сектора (полигон из точек: с шагом 3град по каждому направлению вычисляется
        // пеленг и в полигон добавляется последняя точка пеленга)-> Mercator
        // ************************************************************************************

        public void ShowDirection(
                                  short Luch, 
                                  float sector, 
                                  byte prozr, 
                                  byte col1, 
                                  byte col2, 
                                  byte col3,
                                  double ltSP,
                                  double lnSP,
                                  int r
                                 )

        {
            if (ltSP != 0 && lnSP != 0)
            {
                try
                {
                    Mapsui.Geometries.Point point = new Mapsui.Geometries.Point();

                    point.X = lnSP;
                    point.Y = ltSP;

                    float fSectorLeft = (float)((float)Luch - sector / 2);

                    if (fSectorLeft < 0)
                        fSectorLeft = 360 - Math.Abs(fSectorLeft);


                    float fSectorRight = (float)((float)Luch + sector / 2);

                    if (fSectorRight > 360)
                        fSectorRight = fSectorRight - 360;

                    Mapsui.Geometries.Point pointStationLatLong = new Mapsui.Geometries.Point();

                    pointStationLatLong.X = lnSP;
                    pointStationLatLong.Y = ltSP;

                    double step = 3;

                    int count_point = 0;

                    if (fSectorRight - fSectorLeft > 0)
                        count_point = (int)((fSectorRight - fSectorLeft) / step);
                    else
                        count_point = (int)((fSectorRight + 360 - fSectorLeft) / step);

                    List<Mapsui.Geometries.Point> points = new List<Mapsui.Geometries.Point>(count_point + 2);


                    points.Add(DefinePointSquare(pointStationLatLong, (double)fSectorLeft, 0));

                    for (int i = 0; i < count_point - 1; i++)
                    {
                        points.Add(DefinePointSquare(pointStationLatLong, (double)fSectorLeft + i *step, r));
                    }
                    points.Add(DefinePointSquare(pointStationLatLong, (double)fSectorRight, r));
                    points.Add(DefinePointSquare(pointStationLatLong, (double)fSectorLeft, 0));

                    // for Mercator
                    List<Mapsui.Geometries.Point> pointupd = new List<Mapsui.Geometries.Point>(points.Count);
                    for (int i = 0; i < points.Count; i++)
                    {
                        pointupd.Add(Mercator.FromLonLat(points[i].X, points[i].Y));

                    }

                    objRasterMapControl.AddPolygon(pointupd, Mapsui.Styles.Color.FromArgb(prozr, col1, col2, col3));

                }
                catch (Exception e)
                {
                    //MessageBox.Show(e.Message);
                }

            } // (pointASP.X != 0 && pointASP.Y != 0)
        }

       // ****************************************************************************************************
       // Вычисление пеленга и возврат последней точки пеленга
       // ****************************************************************************************************
       public Mapsui.Geometries.Point DefinePointSquare(Mapsui.Geometries.Point pointCenter, double Azimuth, double Distance)
        {
            Mapsui.Geometries.Point point = new Mapsui.Geometries.Point();

            try
            {
                double dAzimuthDeg = Azimuth;

                double dLat = 0;
                double dLong = 0;

                ClassBearing.f_Bearing(dAzimuthDeg, Distance, 1000, pointCenter.Y, pointCenter.X, 1, ref dLat, ref dLong);

                point.X = dLong;
                point.Y = dLat;

            }

            catch
            { }

            return point;
        }
        // ****************************************************************************************************


        // ************************************************************************************
        // Отрисовка сектора (полигон из точек: с шагом 3град по каждому направлению вычисляется
        // пеленг и в полигон добавляется последняя точка пеленга)-> Для географической карты
        // ************************************************************************************

        public void ShowDirectionGeogr(
                                  short Luch,
                                  float sector,
                                  byte prozr,
                                  byte col1,
                                  byte col2,
                                  byte col3,
                                  double ltSP,
                                  double lnSP,
                                  int r
                                 )

        {
            if (ltSP != 0 && lnSP != 0)
            {
                try
                {
                    Mapsui.Geometries.Point point = new Mapsui.Geometries.Point();

                    point.X = lnSP;
                    point.Y = ltSP;

                    float fSectorLeft = (float)((float)Luch - sector / 2);

                    if (fSectorLeft < 0)
                        fSectorLeft = 360 - Math.Abs(fSectorLeft);


                    float fSectorRight = (float)((float)Luch + sector / 2);

                    if (fSectorRight > 360)
                        fSectorRight = fSectorRight - 360;

                    Mapsui.Geometries.Point pointStationLatLong = new Mapsui.Geometries.Point();

                    pointStationLatLong.X = lnSP;
                    pointStationLatLong.Y = ltSP;

                    double step = 3;

                    int count_point = 0;

                    if (fSectorRight - fSectorLeft > 0)
                        count_point = (int)((fSectorRight - fSectorLeft) / step);
                    else
                        count_point = (int)((fSectorRight + 360 - fSectorLeft) / step);

                    List<Mapsui.Geometries.Point> points = new List<Mapsui.Geometries.Point>(count_point + 2);


                    points.Add(DefinePointSquare(pointStationLatLong, (double)fSectorLeft, 0));

                    for (int i = 0; i < count_point - 1; i++)
                    {
                        points.Add(DefinePointSquare(pointStationLatLong, (double)fSectorLeft + i * step, r));
                    }
                    points.Add(DefinePointSquare(pointStationLatLong, (double)fSectorRight, r));
                    points.Add(DefinePointSquare(pointStationLatLong, (double)fSectorLeft, 0));

                    // for Mercator
                    //List<Mapsui.Geometries.Point> pointupd = new List<Mapsui.Geometries.Point>(points.Count);
                    //for (int i = 0; i < points.Count; i++)
                    //{
                    //    pointupd.Add(Mercator.FromLonLat(points[i].X, points[i].Y));
                    //}

                    objRasterMapControl.AddPolygon(points, Mapsui.Styles.Color.FromArgb(prozr, col1, col2, col3));

                }
                catch (Exception e)
                {
                    //MessageBox.Show(e.Message);
                }

            } // (pointASP.X != 0 && pointASP.Y != 0)
        }

        // ****************************************************************************************************

        // Obj1 *******************************************************************************
        // ReDraw Objects1 (ИРИ ФРЧ)

        public void ReDraw_SRW_FRF()
        {
            List<Class_IRIFRCH> List_SRW_FRF_Del = new List<Class_IRIFRCH>(objClassInterfaceMap.List_SRW_FRF);

            if ((objClassInterfaceMap.Show_SRW_FRF == true)&&(List_SRW_FRF_Del.Count!=0))
            {
                string s = "";

                if (List_SRW_FRF_Del.Count == 0)
                {
                    return;
                }

                // Get current dir
                string dir = "";
                // -------------------------------------------------------------------------------------------------
                // MercatorMap or GeographicMap?

                string s1 = "";
                InitMapYaml initMapYaml = new InitMapYaml();
                s1 = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
                // -------------------------------------------------------------------------------------------------

                for (int i = 0; i < List_SRW_FRF_Del.Count; i++)
                {
                    if (
                        (List_SRW_FRF_Del[i].Latitude!=-1)&&
                        (List_SRW_FRF_Del[i].Longitude!=-1)&&
                        (List_SRW_FRF_Del[i].Latitude !=0) &&
                        (List_SRW_FRF_Del[i].Longitude !=0)
                       )
                    {

                        dir = GetPath_SRW_FRF(objClassInterfaceMap.List_SRW_FRF[i].FreqKHz);

                        // .............................................................................................
                        // GeographicMap

                        if (initMapYaml.TypeMap == 1)
                        {

                            long ichislo2 = (long)(List_SRW_FRF_Del[i].FreqKHz * 100);
                            double dchislo2 = ((double)ichislo2) / 100;

                            s = objClassInterfaceMap.DrawImage(
                                                     List_SRW_FRF_Del[i].Latitude,
                                                     List_SRW_FRF_Del[i].Longitude,
                                                     // Path to image
                                                     dir,
                                                    // Text
                                                    //"",
                                                    Convert.ToString(dchislo2),
                                                     // Scale
                                                     0.6
                                          );
                        } // GeographicMap
                          // .............................................................................................
                          // MercatorMap

                        else
                        {
                            double xcntr = 0;
                            double ycntr = 0;
                            var p = Mercator.FromLonLat(List_SRW_FRF_Del[i].Longitude, List_SRW_FRF_Del[i].Latitude);

                            //List_SRW_FRF_Del[i].FreqKHz


                            xcntr = p.X;
                            ycntr = p.Y;

                            long ichislo1 = (long)(List_SRW_FRF_Del[i].FreqKHz * 100);
                            double dchislo1 = ((double)ichislo1) / 100;

                            s = objClassInterfaceMap.DrawImage(
                                                     ycntr,
                                                     xcntr,
                                                     // Path to image
                                                     dir,
                                                     // Text
                                                    // "",
                                                    Convert.ToString(dchislo1),
                                                     // Scale
                                                     0.6
                                          );

                        } // MercatorMap
                          // .............................................................................................

                        if (s != "")
                        {
                            MessageBox.Show(s);
                        }

                    } // IF (есть координаты)

                } // for
                // -------------------------------------------------------------------------------------------------

            } // IF(objClassInterfaceMap.Show_SRW_FRF==true)

        }
        // ******************************************************************************* Obj1

        // PelengsObj1 ************************************************************************
        // ReDraw Pelengs For Objects1 (ИРИ ФРЧ)

        public void ReDraw_Pelengs_IRIFRCH()
        {

            //Convert.ToString(az) + objClassInterfaceMap.List_JS[i].Caption

            List<Class_IRIFRCH> List_SRW_FRF_Del = new List<Class_IRIFRCH>(objClassInterfaceMap.List_SRW_FRF);

            // Get current dir
            string dir = "";
            dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);


            if (
                (objClassInterfaceMap.ShowJS == true) &&
                (objClassInterfaceMap.Show_SRW_STRF_RS == true) && // Pelengs
                (List_SRW_FRF_Del.Count > 0)
                )
            {

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // Ищем в таблице ИРИ ФРЧ выбранную строку

                int index = List_SRW_FRF_Del.FindIndex(x => (x.IsSelected==true));
                if (index >= 0)
                {

                if(List_SRW_FRF_Del[index].list_JSBearing.Count!=0)
                 {
                   int i = 0;

                 // ----------------------------------------------------------------------------------
                 // MercatorMap or GeographicMap?

                 string s1 = "";
                 InitMapYaml initMapYaml = new InitMapYaml();
                 s1 = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
                 // ---------------------------------------------------------------------------------

                  // --------------------------------------------------------------------------------
                  for (i=0;i< List_SRW_FRF_Del[index].list_JSBearing.Count;i++)
                   {
                    // Находим JSi в основном списке
                    int index1 = -1;
                    index1 = objClassInterfaceMap.List_JS.FindIndex(x => (x.Id == List_SRW_FRF_Del[index].list_JSBearing[i].NumberASP));
                    if (
                        (index1 >= 0)&&
                        (List_SRW_FRF_Del[index].list_JSBearing[i].Bearing!=-1)&&
                        (List_SRW_FRF_Del[index].list_JSBearing[i].list_bearing.Count!=0)
                       )
                    {
               

                     List<Mapsui.Geometries.Point> pointPel1 = new List<Mapsui.Geometries.Point>();
                     int i1 = 0;

                     // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                     // GeographicMap

                    if (initMapYaml.TypeMap == 1)
                    {
                                    string s111 = objClassInterfaceMap.DrawImage(
                                                             List_SRW_FRF_Del[index].list_JSBearing[i].Latitude,
                                                             List_SRW_FRF_Del[index].list_JSBearing[i].Longitude,
                                                             // Path to image
                                                             dir + "\\Images\\Jammer\\" + "1.png",
                                                         // Text
                                                         objClassInterfaceMap.List_JS[index1].Caption + '\n' +
                                                         Convert.ToString((int)List_SRW_FRF_Del[index].list_JSBearing[i].Bearing),
                                                             //objClassInterfaceMap.List_JS[i].Caption,
                                                             // Scale
                                                             2
                                                  );


                                    // 1-я точка - это JSi
                                    pointPel1.Add(new Mapsui.Geometries.Point(List_SRW_FRF_Del[index].list_JSBearing[i].Longitude,
                                                                List_SRW_FRF_Del[index].list_JSBearing[i].Latitude));

                     for (i1 = 0; i1 < List_SRW_FRF_Del[index].list_JSBearing[i].list_bearing.Count; i1++)
                      {
                        pointPel1.Add(new Mapsui.Geometries.Point(List_SRW_FRF_Del[index].list_JSBearing[i].list_bearing[i1].Longitude,
                                                                  List_SRW_FRF_Del[index].list_JSBearing[i].list_bearing[i1].Latitude));
                      }

                      string s2 = objClassInterfaceMap.DrawLinesLatLong(
                                                                       // Degree
                                                                       pointPel1,
                                                                       // Color
                                                                       100,
                                                                       0,
                                                                       0,
                                                                       255
                                                                       );

                     } // Geographic Map
                     // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                     // Mercator Map

                    else
                     {
                                    // ##@##
                                    double xcntr11 = 0;
                                    double ycntr11 = 0;
                                    var p11 = Mercator.FromLonLat(List_SRW_FRF_Del[index].list_JSBearing[i].Longitude,
                                                                List_SRW_FRF_Del[index].list_JSBearing[i].Latitude);
                                    xcntr11 = p11.X;
                                    ycntr11 = p11.Y;
                                    string s11 = objClassInterfaceMap.DrawImage(
                                                         ycntr11,
                                                         xcntr11,
                                                         // Path to image
                                                         dir + "\\Images\\Jammer\\" + "1.png",
                                                         // Text
                                                         objClassInterfaceMap.List_JS[index1].Caption+'\n'+
                                                         Convert.ToString((int)List_SRW_FRF_Del[index].list_JSBearing[i].Bearing),
                                                         // Scale
                                                         2
                                                         );


                      double xcntrJS = 0;
                      double ycntrJS = 0;

                      // 1-я точка - это JSi
                      var p1 = Mercator.FromLonLat(List_SRW_FRF_Del[index].list_JSBearing[i].Longitude,
                                                   List_SRW_FRF_Del[index].list_JSBearing[i].Latitude);
                      xcntrJS = p1.X;
                      ycntrJS = p1.Y;
                      pointPel1.Add(new Mapsui.Geometries.Point(xcntrJS, ycntrJS));

                      for (i1 = 0; i1 < List_SRW_FRF_Del[index].list_JSBearing[i].list_bearing.Count; i1++)
                        {
                         double xcntr1 = 0;
                         double ycntr1 = 0;

                         var p = Mercator.FromLonLat(List_SRW_FRF_Del[index].list_JSBearing[i].list_bearing[i1].Longitude,
                                                     List_SRW_FRF_Del[index].list_JSBearing[i].list_bearing[i1].Latitude);

                         xcntr1 = p.X;
                         ycntr1 = p.Y;
                         pointPel1.Add(new Mapsui.Geometries.Point(xcntr1, ycntr1));
                       }

                       string s3 = objClassInterfaceMap.DrawLinesLatLong(
                                                                         // Degree
                                                                         pointPel1,
                                                                         // Color
                                                                         100,
                                                                         0,
                                                                         0,
                                                                         255
                                                                        );

                     } // // Mercator Map
                     // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

                     } // (JSi есть в основном списке) && (Bearing!=-1)&&(Count точек пеленга!=0)

                  } // FOR JSi
                  // --------------------------------------------------------------------------------

                 } // IF(objClassInterfaceMap.List_SRW_FRF[index].list_JSBearing.Count!=0)

                } // IF (index >= 0) -> есть выделенная строка
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            } // IF (нажаты птички СП, пеленги и лист ФРЧ.Count!=0)

        }
        // ************************************************************************ PelengsObj1

        // Obj2 *******************************************************************************
        // ReDraw Objects2 (ИРИ ФРЧ ЦР)
        // 999

        public void ReDraw_SRW_FRF_TD()
        {

            List<TableReconFWS> List_SRW_FRF_TD_Del = new List<TableReconFWS>(objClassInterfaceMap.List_SRW_FRF_TD);

            if ((objClassInterfaceMap.Show_SRW_FRF_TD == true)&&(List_SRW_FRF_TD_Del.Count>0))
            {
                string s = "";

                if (List_SRW_FRF_TD_Del.Count == 0)
                {
                    return;
                }

                // Get current dir
                string dir = "";
                // -------------------------------------------------------------------------------------------------
                // MercatorMap or GeographicMap?

                string s1 = "";
                InitMapYaml initMapYaml = new InitMapYaml();
                s1 = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
                // -------------------------------------------------------------------------------------------------

                for (int i = 0; i < List_SRW_FRF_TD_Del.Count; i++)
                {
                    dir = GetPath_SRW_FRF_TD(List_SRW_FRF_TD_Del[i].FreqKHz);

                    // .............................................................................................
                    // GeographicMap

                    if (initMapYaml.TypeMap == 1)
                    {
                        s = objClassInterfaceMap.DrawImage(
                                                 List_SRW_FRF_TD_Del[i].Coordinates.Latitude,
                                                 List_SRW_FRF_TD_Del[i].Coordinates.Longitude,
                                                 // Path to image
                                                 dir,
                                                 // Text
                                                 "",
                                                 // Scale
                                                 0.6
                                      );
                    } // GeographicMap
                    // .............................................................................................
                    // MercatorMap

                    else
                    {
                        double xcntr = 0;
                        double ycntr = 0;
                        var p = Mercator.FromLonLat(List_SRW_FRF_TD_Del[i].Coordinates.Longitude, List_SRW_FRF_TD_Del[i].Coordinates.Latitude);
                        xcntr = p.X;
                        ycntr = p.Y;

                        s = objClassInterfaceMap.DrawImage(
                                                 ycntr,
                                                 xcntr,
                                                 // Path to image
                                                 dir,
                                                 // Text
                                                 "",
                                                 // Scale
                                                 0.6
                                      );

                    } // MercatorMap
                    // .............................................................................................

                    if (s != "")
                    {
                        MessageBox.Show(s);
                    }

                } // for
                // -------------------------------------------------------------------------------------------------

            } // IF(objClassInterfaceMap.Show_SRW_FRF_TD==true)

        }
        // ******************************************************************************* Obj2

        // Obj3 *******************************************************************************
        // ReDraw Objects3 (ИРИ ФРЧ РП)

        public void ReDraw_SRW_FRF_RS()
        {
            List<TableSuppressFWS> List_SRW_FRF_RS_Del = new List<TableSuppressFWS>(objClassInterfaceMap.List_SRW_FRF_RS);

            if ((objClassInterfaceMap.Show_SRW_FRF_RS == true)&&(List_SRW_FRF_RS_Del.Count>0))
            {
                string s = "";

                if (List_SRW_FRF_RS_Del.Count == 0)
                {
                    return;
                }

                // Get current dir
                string dir = "";
                // -------------------------------------------------------------------------------------------------
                // MercatorMap or GeographicMap?

                string s1 = "";
                InitMapYaml initMapYaml = new InitMapYaml();
                s1 = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
                // -------------------------------------------------------------------------------------------------

                for (int i = 0; i < List_SRW_FRF_RS_Del.Count; i++)
                {
                    dir = GetPath_SRW_FRF_RS(List_SRW_FRF_RS_Del[i].FreqKHz.Value);

                    // .............................................................................................
                    // GeographicMap

                    if (initMapYaml.TypeMap == 1)
                    {
                        s = objClassInterfaceMap.DrawImage(
                                                 List_SRW_FRF_RS_Del[i].Coordinates.Latitude,
                                                 List_SRW_FRF_RS_Del[i].Coordinates.Longitude,
                                                 // Path to image
                                                 dir,
                                                 // Text
                                                 "",
                                                 // Scale
                                                 0.6
                                      );
                    } // GeographicMap
                    // .............................................................................................
                    // MercatorMap

                    else
                    {
                        double xcntr = 0;
                        double ycntr = 0;
                        var p = Mercator.FromLonLat(List_SRW_FRF_RS_Del[i].Coordinates.Longitude, List_SRW_FRF_RS_Del[i].Coordinates.Latitude);
                        xcntr = p.X;
                        ycntr = p.Y;

                        s = objClassInterfaceMap.DrawImage(
                                                 ycntr,
                                                 xcntr,
                                                 // Path to image
                                                 dir,
                                                 // Text
                                                 "",
                                                 // Scale
                                                 0.6
                                      );

                    } // MercatorMap
                    // .............................................................................................

                    if (s != "")
                    {
                        MessageBox.Show(s);
                    }

                } // for
                // -------------------------------------------------------------------------------------------------

            } // IF(objClassInterfaceMap.Show_SRW_FRF_RS==true)

        }
        // ******************************************************************************* Obj3

        // Obj4 *******************************************************************************
        // ReDraw Objects4 (ИРИ ППРЧ)

        public void ReDraw_SRW_STRF()
        {
            List<Class_IRIPPRCH> List_SRW_STRF_Del = new List<Class_IRIPPRCH>(objClassInterfaceMap.List_SRW_STRF);

            if ((objClassInterfaceMap.Show_SRW_STRF == true) && (List_SRW_STRF_Del.Count != 0))
            {
                string s = "";

                if (List_SRW_STRF_Del.Count == 0)
                {
                    return;
                }

                // Get current dir
                string dir = "";
                // -------------------------------------------------------------------------------------------------
                // MercatorMap or GeographicMap?

                string s1 = "";
                InitMapYaml initMapYaml = new InitMapYaml();
                s1 = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
                // -------------------------------------------------------------------------------------------------

                for (int i = 0; i < List_SRW_STRF_Del.Count; i++)
                {
                    if (
                        (List_SRW_STRF_Del[i].Latitude != -1) &&
                        (List_SRW_STRF_Del[i].Longitude != -1) &&
                        (List_SRW_STRF_Del[i].Latitude != 0) &&
                        (List_SRW_STRF_Del[i].Longitude != 0)
                       )
                    {

                        dir = GetPath_SRW_STRF(List_SRW_STRF_Del[i].FreqKHz);
                        // .............................................................................................
                        // GeographicMap

                        if (initMapYaml.TypeMap == 1)
                        {
                            s = objClassInterfaceMap.DrawImage(
                                                     List_SRW_STRF_Del[i].Latitude,
                                                     List_SRW_STRF_Del[i].Longitude,
                                                     // Path to image
                                                     dir,
                                                     // Text
                                                     "",
                                                     // Scale
                                                     0.6
                                          );
                        } // GeographicMap
                          // .............................................................................................
                          // MercatorMap

                        else
                        {
                            double xcntr = 0;
                            double ycntr = 0;
                            var p = Mercator.FromLonLat(List_SRW_STRF_Del[i].Longitude, List_SRW_STRF_Del[i].Latitude);
                            xcntr = p.X;
                            ycntr = p.Y;

                            s = objClassInterfaceMap.DrawImage(
                                                     ycntr,
                                                     xcntr,
                                                     // Path to image
                                                     dir,
                                                     // Text
                                                     "",
                                                     // Scale
                                                     0.6
                                          );

                        } // MercatorMap
                          // .............................................................................................

                        if (s != "")
                        {
                            MessageBox.Show(s);
                        }

                    } // IF (есть координаты)

                } // for
                // -------------------------------------------------------------------------------------------------

            } // IF(objClassInterfaceMap.Show_SRW_STRF==true)

        }
        // ******************************************************************************* Obj4

        // PelengsObj4 ************************************************************************
        // ReDraw Pelengs For Objects4 (ИРИ ППРЧ)

        public void ReDraw_Pelengs_IRIPPRCH()
        {

            ;
        }
        // ************************************************************************ PelengsObj4

        // Obj5 *******************************************************************************
        // ReDraw Objects5 (ИРИ ППРЧ РП)

        public void ReDraw_SRW_STRF_RS()
        {


/*
            if (objClassInterfaceMap.Show_SRW_STRF_RS == true)
            {
                string s = "";

                if (objClassInterfaceMap.List_SRW_STRF_RS.Count == 0)
                {
                    MessageBox.Show("Objects_SRW_STRF_RS not detected");
                    return;
                }

                // Get current dir
                string dir = "";
                //dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

                for (int i = 0; i < objClassInterfaceMap.List_SRW_STRF_RS.Count; i++)
                {
                    //dir = GetColorObject(objClassInterfaceMap.List_SRW_STRF_RS[i].Frequency);
                    dir = GetPath_SRW_STRF_RS(objClassInterfaceMap.List_SRW_STRF_RS[i].FreqKHz.Value);

                    s = objClassInterfaceMap.DrawImage(
                                             objClassInterfaceMap.List_SRW_STRF_RS[i].Coordinates.Latitude,
                                             objClassInterfaceMap.List_SRW_STRF_RS[i].Coordinates.Longitude,
                                             // Path to image
                                             //dir + "\\Images\\EnemyObject\\" + "1.png",
                                             dir,
                                             // Text
                                             "",
                                             // Scale
                                             0.6
                                  );

                    if (s != "")
                    {
                        MessageBox.Show(s);
                    }

                } // for

            } // IF(objClassInterfaceMap.Show_SRF_STRF_RS==true)
*/
        }
        // ******************************************************************************* Obj5

        // AirP *******************************************************************************
        // ReDraw Airplanes 

        public void ReDraw_AP()
        {

            List<TempADSB_AP> List_APDel = new List<TempADSB_AP>(objClassInterfaceMap.List_AP);

            if (objClassInterfaceMap.Show_AP == true)
            {
                
                string s = "";

                if (List_APDel.Count == 0)
                {
                    //MessageBox.Show("Airplanes not detected");
                    return;
                }

                // Get current dir
                string dir = "";
                dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

                // --------------------------------------------------------------------------
                // MercatorMap or GeographicMap?

                string s1 = "";
                InitMapYaml initMapYaml = new InitMapYaml();
                s1 = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
                // --------------------------------------------------------------------------

                // FOR_AIRPLANES ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

                int i1 = 0;
                double Lt1 = 0;
                double Ln1 = 0;
                double Lt2 = 0;
                double Ln2 = 0;

                //for (int i = 0; i < objClassInterfaceMap.List_AP.Count; i++)
                for (int i = 0; i < List_APDel.Count; i++)

                    {
                        // GEOGRAPHICS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                        if (initMapYaml.TypeMap == 1)
                    {

                        // ..................................................................
                        // Airplane

                        if (objClassInterfaceMap.List_AP[i].flColor==0)
                        {
                            s = objClassInterfaceMap.DrawImageRotate(
                                           // Degree
                                           //objClassInterfaceMap.List_AP[i].Coordinates.Latitude,
                                           //objClassInterfaceMap.List_AP[i].Coordinates.Longitude,
                                           List_APDel[i].Coordinates.Latitude,
                                           List_APDel[i].Coordinates.Longitude,
                                           dir + "\\Images\\OtherObject\\" + "Airpl10.png",
                                           //objClassInterfaceMap.List_AP[i].Azimuth,
                                           List_APDel[i].Azimuth,
                                           // Title
                                           //objClassInterfaceMap.List_AP[i].ICAO,
                                           List_APDel[i].ICAO,

                                           // Scale of image
                                           0.5,0
                                          );
                        }
                        else
                        {
                            s = objClassInterfaceMap.DrawImageRotate(
                                           // Degree
                                           //objClassInterfaceMap.List_AP[i].Coordinates.Latitude,
                                           //objClassInterfaceMap.List_AP[i].Coordinates.Longitude,
                                           List_APDel[i].Coordinates.Latitude,
                                           List_APDel[i].Coordinates.Longitude,
                                           dir + "\\Images\\OtherObject\\" + "Airpl10red.png",
                                           //objClassInterfaceMap.List_AP[i].Azimuth,
                                           List_APDel[i].Azimuth,
                                           // Title
                                           //objClassInterfaceMap.List_AP[i].ICAO,
                                           List_APDel[i].ICAO,
                                           // Scale of image
                                           0.5,1
                                          );

                        }
                        // ..................................................................
                        // Trajectory

                        List<Mapsui.Geometries.Point> pointPel = new List<Mapsui.Geometries.Point>();

                        // FOR2
                        //for (i1 = 0; i1 < objClassInterfaceMap.List_AP[i].list_air_traj.Count; i1++)
                        for (i1 = 0; i1 < List_APDel[i].list_air_traj.Count; i1++)
                        {

                            //pointPel.Add(new Mapsui.Geometries.Point(objClassInterfaceMap.List_AP[i].list_air_traj[i1].Longitude,
                            //                                      objClassInterfaceMap.List_AP[i].list_air_traj[i1].Latitude));
                            pointPel.Add(new Mapsui.Geometries.Point(List_APDel[i].list_air_traj[i1].Longitude,
                                                              List_APDel[i].list_air_traj[i1].Latitude));

                        } // FOR2

                        string s66 = objClassInterfaceMap.DrawLinesLatLong(
                                                            // Draw Lines: points.X=Long, points.Y=Lat -> degree(for geographic map)
                                                            // points.X=X, points.Y=Y -> for Mercator map
                                                            pointPel,
                                                            // Color
                                                            100,
                                                            255,
                                                            0,
                                                            0
                                                           );

                        // ..................................................................

                    }
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> GEOGRAPHICS

                    // MERCATOR >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // MercatorMap

                    else
                    {
                        // ..................................................................
                        // Airplane

                        double xcntr = 0;
                        double ycntr = 0;
                        //var p = Mercator.FromLonLat(objClassInterfaceMap.List_AP[i].Coordinates.Longitude, objClassInterfaceMap.List_AP[i].Coordinates.Latitude);
                        var p = Mercator.FromLonLat(List_APDel[i].Coordinates.Longitude, List_APDel[i].Coordinates.Latitude);

                        xcntr = p.X;
                        ycntr = p.Y;

                                try
                            {
                            if (objClassInterfaceMap.List_AP[i].flColor == 0)
                            {

                                s = objClassInterfaceMap.DrawImageRotate(
                                           ycntr,
                                           xcntr,
                                           dir + "\\Images\\OtherObject\\" + "Airpl10.png",
                                           List_APDel[i].Azimuth,
                                           // Title
                                           List_APDel[i].ICAO,
                                           // Scale of image
                                           0.5,0
                                          );
                            }
                            else

                            {
                                s = objClassInterfaceMap.DrawImageRotate(
                                               ycntr,
                                               xcntr,
                                               dir + "\\Images\\OtherObject\\" + "Airpl10red.png",
                                               objClassInterfaceMap.List_AP[i].Azimuth,
                                               // Title
                                               //objClassInterfaceMap.List_AP[i].ICAO + " " + Convert.ToString((int)objClassInterfaceMap.List_AP[i].Azimuth),
                                               objClassInterfaceMap.List_AP[i].ICAO,
                                               // Scale of image
                                               0.5,1
                                              );

                            }
                        }
                        catch
                            {
                               // MessageBox.Show("Error3");

                            }
                        // ..................................................................
                        // Trajectory

                        

                                                double xcntr1 = 0;
                                                double ycntr1 = 0;

                                                List<Mapsui.Geometries.Point> pointPel1 = new List<Mapsui.Geometries.Point>();

                                                // FOR22
                                                try
                                                {
                                                    //for (int i11 = 0; i11 < objClassInterfaceMap.List_AP[i].list_air_traj.Count; i11++)
                                                    for (int i11 = 0; i11 < List_APDel[i].list_air_traj.Count; i11++)
                                                    {
                                                        //var ppp = Mercator.FromLonLat(objClassInterfaceMap.List_AP[i].list_air_traj[i11].Longitude,
                                                        //                         objClassInterfaceMap.List_AP[i].list_air_traj[i11].Latitude);
                                                        var ppp = Mercator.FromLonLat(List_APDel[i].list_air_traj[i11].Longitude,
                                                                                 List_APDel[i].list_air_traj[i11].Latitude);


                                                        xcntr1 = ppp.X;
                                                        ycntr1 = ppp.Y;
                                                        pointPel1.Add(new Mapsui.Geometries.Point(xcntr1, ycntr1));

                                                    } // FOR22
                                                }
                                                catch
                                                {
                                                    //MessageBox.Show("Error30");

                                                }

                                                try
                                                {

                                                    string s66 = objClassInterfaceMap.DrawLinesLatLong(
                                                                                        // Draw Lines: points.X=Long, points.Y=Lat -> degree(for geographic map)
                                                                                        // points.X=X, points.Y=Y -> for Mercator map
                                                                                        pointPel1,
                                                                                        // Color
                                                                                        100,
                                                                                        255,
                                                                                        0,
                                                                                        0
                                                                                       );
                                                }
                                                catch
                                                {
                                                    //MessageBox.Show("Error5");

                                                }
                        
                        // ..................................................................



                    } // MercatorMap
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> MERCATOR

                    if (s != "")
                {
                    MessageBox.Show(s);
                }

                } // for
                // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ FOR_AIRPLANES

            } // IF(objClassInterfaceMap.Show_AP==true)


            List_APDel.Clear();


        }
        // ******************************************************************************* AirP




        /*
                        for (int i = 0; i<objClassInterfaceMap.List_JS.Count; i++)
                        {

                            List<Mapsui.Geometries.Point> pointPel = new List<Mapsui.Geometries.Point>();

                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // Line from object to station

                            // .............................................................................................
                            // GeographicMap

                            if (initMapYaml.TypeMap == 1)
                            {
                                pointPel.Add(new Mapsui.Geometries.Point(objClassInterfaceMap.List_JS[i].Coordinates.Longitude, objClassInterfaceMap.List_JS[i].Coordinates.Latitude));
                                pointPel.Add(new Mapsui.Geometries.Point(lonSource, latSource));
                                s6 = objClassInterfaceMap.DrawLinesLatLong(
                                                                    // Draw Lines: points.X=Long, points.Y=Lat -> degree(for geographic map)
                                                                    // points.X=X, points.Y=Y -> for Mercator map
                                                                    pointPel,
                                                                    // Color
                                                                    100,
                                                                    255,
                                                                    0,
                                                                    0
                                                                   );
                            } // GeographicMap
                              // .............................................................................................
                              // MercatorMap

                            else
                            {
                                double xcntr1 = 0;
                                double ycntr1 = 0;
                                double xcntrJS = 0;
                                double ycntrJS = 0;

                                var p = Mercator.FromLonLat(lonSource, latSource);
                                xcntr1 = p.X;
                                ycntr1 = p.Y;
                                var p1 = Mercator.FromLonLat(objClassInterfaceMap.List_JS[i].Coordinates.Longitude, objClassInterfaceMap.List_JS[i].Coordinates.Latitude);
                                xcntrJS = p1.X;
                                ycntrJS = p1.Y;

                                pointPel.Add(new Mapsui.Geometries.Point(xcntrJS, ycntrJS));
                                pointPel.Add(new Mapsui.Geometries.Point(xcntr1, ycntr1));
                                s6 = objClassInterfaceMap.DrawLinesLatLong(
                                                                    // Draw Lines: points.X=Long, points.Y=Lat -> degree(for geographic map)
                                                                    // points.X=X, points.Y=Y -> for Mercator map
                                                                    pointPel,
                                                                    // Color
                                                                    100,
                                                                    255,
                                                                    0,
                                                                    0
                                                                   );

                            } // MercatorMap
                              // .............................................................................................


        */




        // GetColorObject *********************************************************************
        // return: path to image

        public string GetColorObject(double Freq)
        {
            string path = "";
            string simage = "";

            // Get current dir
            string dir = "";
            dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            if ((Freq >= GlobalVarMap.fCl1) && (Freq < GlobalVarMap.fCl1_2))
                simage = "1.png";
            else if ((Freq >= GlobalVarMap.fCl2) && (Freq < GlobalVarMap.fCl2_2))
                simage = "2.png";
            else if ((Freq >= GlobalVarMap.fCl3) && (Freq < GlobalVarMap.fCl3_2))
                simage = "3.png";
            else if ((Freq >= GlobalVarMap.fCl4) && (Freq < GlobalVarMap.fCl4_2))
                simage = "4.png";
            else if ((Freq >= GlobalVarMap.fCl5) && (Freq < GlobalVarMap.fCl5_2))
                simage = "5.png";
            else if ((Freq >= GlobalVarMap.fCl6) && (Freq < GlobalVarMap.fCl6_2))
                simage = "6.png";
            else if ((Freq >= GlobalVarMap.fCl7) && (Freq < GlobalVarMap.fCl7_2))
                simage = "7.png";
            else if ((Freq >= GlobalVarMap.fCl8) && (Freq < GlobalVarMap.fCl8_2))
                simage = "8.png";
            else if ((Freq >= GlobalVarMap.fCl9) && (Freq < GlobalVarMap.fCl9_2))
                simage = "9.png";
            else 
                simage = "10.png";

            path = dir + "\\Images\\OtherObject\\DEL\\" + simage;

            return path;

        }
        // ******************************************************************************* AirP

        // GetPathToObject1 *******************************************************************
        // return: path to image
        // ИРИ ФРЧ

        public string GetPath_SRW_FRF(double Freq)
        {
            string path = "";
            string simage = "";

            // Get current dir
            string dir = "";
            dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            if ((Freq >= GlobalVarMap.fCl1) && (Freq < GlobalVarMap.fCl1_2))
                simage = "tr1.png";
            else if ((Freq >= GlobalVarMap.fCl2) && (Freq < GlobalVarMap.fCl2_2))
                simage = "tr2.png";
            else if ((Freq >= GlobalVarMap.fCl3) && (Freq < GlobalVarMap.fCl3_2))
                simage = "tr3.png";
            else if ((Freq >= GlobalVarMap.fCl4) && (Freq < GlobalVarMap.fCl4_2))
                simage = "tr4.png";
            else if ((Freq >= GlobalVarMap.fCl5) && (Freq < GlobalVarMap.fCl5_2))
                simage = "tr5.png";
            else if ((Freq >= GlobalVarMap.fCl6) && (Freq < GlobalVarMap.fCl6_2))
                simage = "tr6.png";
            else if ((Freq >= GlobalVarMap.fCl7) && (Freq < GlobalVarMap.fCl7_2))
                simage = "tr7.png";
            else if ((Freq >= GlobalVarMap.fCl8) && (Freq < GlobalVarMap.fCl8_2))
                simage = "tr8.png";
            else if ((Freq >= GlobalVarMap.fCl9) && (Freq < GlobalVarMap.fCl9_2))
                simage = "tr9.png";
            else
                simage = "tr10.png";

            path = dir + "\\Images\\OtherObject\\Triangle1\\" + simage;

            return path;

        }
        // ******************************************************************* GetPathToObject1

        // GetPathToObject2 *******************************************************************
        // return: path to image
        // ИРИ ФРЧ ЦР

        public string GetPath_SRW_FRF_TD(double Freq)
        {
            string path = "";
            string simage = "";

            // Get current dir
            string dir = "";
            dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            if ((Freq >= GlobalVarMap.fCl1) && (Freq < GlobalVarMap.fCl1_2))
                simage = "trLine1.png";
            else if ((Freq >= GlobalVarMap.fCl2) && (Freq < GlobalVarMap.fCl2_2))
                simage = "trLine2.png";
            else if ((Freq >= GlobalVarMap.fCl3) && (Freq < GlobalVarMap.fCl3_2))
                simage = "trLine3.png";
            else if ((Freq >= GlobalVarMap.fCl4) && (Freq < GlobalVarMap.fCl4_2))
                simage = "trLine4.png";
            else if ((Freq >= GlobalVarMap.fCl5) && (Freq < GlobalVarMap.fCl5_2))
                simage = "trLine5.png";
            else if ((Freq >= GlobalVarMap.fCl6) && (Freq < GlobalVarMap.fCl6_2))
                simage = "trLine6.png";
            else if ((Freq >= GlobalVarMap.fCl7) && (Freq < GlobalVarMap.fCl7_2))
                simage = "trLine7.png";
            else if ((Freq >= GlobalVarMap.fCl8) && (Freq < GlobalVarMap.fCl8_2))
                simage = "trLine8.png";
            else if ((Freq >= GlobalVarMap.fCl9) && (Freq < GlobalVarMap.fCl9_2))
                simage = "trLine9.png";
            else
                simage = "trLine10.png";

            path = dir + "\\Images\\OtherObject\\Triangle3\\" + simage;

            return path;

        }
        // ******************************************************************* GetPathToObject2

        // GetPathToObject3 *******************************************************************
        // return: path to image
        // ИРИ ФРЧ РП

        public string GetPath_SRW_FRF_RS(double Freq)
        {
            string path = "";
            string simage = "";

            // Get current dir
            string dir = "";
            dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            if ((Freq >= GlobalVarMap.fCl1) && (Freq < GlobalVarMap.fCl1_2))
                simage = "trLines1.png";
            else if ((Freq >= GlobalVarMap.fCl2) && (Freq < GlobalVarMap.fCl2_2))
                simage = "trLines2.png";
            else if ((Freq >= GlobalVarMap.fCl3) && (Freq < GlobalVarMap.fCl3_2))
                simage = "trLines3.png";
            else if ((Freq >= GlobalVarMap.fCl4) && (Freq < GlobalVarMap.fCl4_2))
                simage = "trLines4.png";
            else if ((Freq >= GlobalVarMap.fCl5) && (Freq < GlobalVarMap.fCl5_2))
                simage = "trLines5.png";
            else if ((Freq >= GlobalVarMap.fCl6) && (Freq < GlobalVarMap.fCl6_2))
                simage = "trLines6.png";
            else if ((Freq >= GlobalVarMap.fCl7) && (Freq < GlobalVarMap.fCl7_2))
                simage = "trLines7.png";
            else if ((Freq >= GlobalVarMap.fCl8) && (Freq < GlobalVarMap.fCl8_2))
                simage = "trLines8.png";
            else if ((Freq >= GlobalVarMap.fCl9) && (Freq < GlobalVarMap.fCl9_2))
                simage = "trLines9.png";
            else
                simage = "trLines10.png";

            path = dir + "\\Images\\OtherObject\\Triangle2\\" + simage;

            return path;

        }
        // ******************************************************************* GetPathToObject3

        // GetPathToObject4 *******************************************************************
        // return: path to image
        // ИРИ ППРЧ

        public string GetPath_SRW_STRF(double Freq)
        {
            string path = "";
            string simage = "";

            // Get current dir
            string dir = "";
            dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            if ((Freq >= GlobalVarMap.fCl1) && (Freq < GlobalVarMap.fCl1_2))
                simage = "sq1.png";
            else if ((Freq >= GlobalVarMap.fCl2) && (Freq < GlobalVarMap.fCl2_2))
                simage = "sq2.png";
            else if ((Freq >= GlobalVarMap.fCl3) && (Freq < GlobalVarMap.fCl3_2))
                simage = "sq3.png";
            else if ((Freq >= GlobalVarMap.fCl4) && (Freq < GlobalVarMap.fCl4_2))
                simage = "sq4.png";
            else if ((Freq >= GlobalVarMap.fCl5) && (Freq < GlobalVarMap.fCl5_2))
                simage = "sq5.png";
            else if ((Freq >= GlobalVarMap.fCl6) && (Freq < GlobalVarMap.fCl6_2))
                simage = "sq6.png";
            else if ((Freq >= GlobalVarMap.fCl7) && (Freq < GlobalVarMap.fCl7_2))
                simage = "sq7.png";
            else if ((Freq >= GlobalVarMap.fCl8) && (Freq < GlobalVarMap.fCl8_2))
                simage = "sq8.png";
            else if ((Freq >= GlobalVarMap.fCl9) && (Freq < GlobalVarMap.fCl9_2))
                simage = "sq9.png";
            else
                simage = "sq10.png";

            path = dir + "\\Images\\OtherObject\\Square1\\" + simage;

            return path;

        }
        // ******************************************************************* GetPathToObject4

        // GetPathToObject5 *******************************************************************
        // return: path to image
        // ИРИ ППРЧ РП

        public string GetPath_SRW_STRF_RS(double Freq)
        {

            string path = "";
            string simage = "";

            // Get current dir
            string dir = "";
            dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            if ((Freq >= GlobalVarMap.fCl1) && (Freq < GlobalVarMap.fCl1_2))
                simage = "sqLine1.png";
            else if ((Freq >= GlobalVarMap.fCl2) && (Freq < GlobalVarMap.fCl2_2))
                simage = "sqLine2.png";
            else if ((Freq >= GlobalVarMap.fCl3) && (Freq < GlobalVarMap.fCl3_2))
                simage = "sqLine3.png";
            else if ((Freq >= GlobalVarMap.fCl4) && (Freq < GlobalVarMap.fCl4_2))
                simage = "sqLine4.png";
            else if ((Freq >= GlobalVarMap.fCl5) && (Freq < GlobalVarMap.fCl5_2))
                simage = "sqLine5.png";
            else if ((Freq >= GlobalVarMap.fCl6) && (Freq < GlobalVarMap.fCl6_2))
                simage = "sqLine6.png";
            else if ((Freq >= GlobalVarMap.fCl7) && (Freq < GlobalVarMap.fCl7_2))
                simage = "sqLine7.png";
            else if ((Freq >= GlobalVarMap.fCl8) && (Freq < GlobalVarMap.fCl8_2))
                simage = "sqLine8.png";
            else if ((Freq >= GlobalVarMap.fCl9) && (Freq < GlobalVarMap.fCl9_2))
                simage = "sqLine9.png";
            else
                simage = "sqLine10.png";

            path = dir + "\\Images\\OtherObject\\Square2\\" + simage;

            return path;

        }
        // ******************************************************************* GetPathToObject5


    } // Class
} // Namespace
