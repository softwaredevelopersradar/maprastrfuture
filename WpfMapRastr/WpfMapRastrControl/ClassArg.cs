﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Для прерывания клика мыши на карте (прерывание с передачей параметров)
namespace WpfMapRastrControl
{
    public class ClassArg:EventArgs
    {
      public double lat { get; }
      public double lon { get; }
      public double h { get; }
      public double x { get; }
      public double y { get; }
      public string str { get; }

      public ClassArg(double Lat, double Lon, double H, double X, double Y, string STR )
      {
         lat = Lat;
         lon = Lon;
         h = H;
         x = X;
         y = Y;
         str = STR;

       }

    } // Class
} // Namespace
