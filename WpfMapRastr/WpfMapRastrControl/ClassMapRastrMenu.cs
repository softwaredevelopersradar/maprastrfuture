﻿using System.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using System.Drawing;
using System.Runtime.InteropServices;
using System.IO;
using WpfMapControl;
using System.Data;
using System.Reflection;
using System.ServiceModel;
using System.Diagnostics;
using System.Threading;
using System.Globalization;
using Microsoft.Win32;


// Для YAML реализации
using YamlDotNet.Serialization;
// Для JSON реализации
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
// Для реализации XML
using System.Xml;
using System.Xml.Linq;

using ClassLibraryIniFiles;



// XML >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// !!! Этот кусок НЕ убирать

/*            
            // Файл .xml
            //<?xml version="1.0" encoding="utf-8"?>
            //<settings>
            //<Path>E:\!!!2_MAP_WPF\Proj_2019_02_19_1\WpfMapRastr\WpfMapRastr\bin\Debug\data_map\GeoTif\UAENF390441.tif</Path>
            //<PathMatr>E:\\!!!2_MAP_WPF\\Proj_2019_02_19_1\\WpfMapRastr\\WpfMapRastr\\bin\\Debug\\data_map\\Dted\\DTL2\\UAE_E52_N23.dt2</PathMatr>
            //</settings>                 

            string pathMap = "";
            InitMap initMap = new InitMap("","");
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load("Setting.xml");
            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;

            // обход всех узлов в корневом элементе
            foreach (XmlNode xnode in xRoot)
            {
             // !!! Если значение задавали как атрибут
             // получаем атрибут name
             //if (xnode.Attributes.Count > 0)
             //{
             //    XmlNode attr = xnode.Attributes.GetNamedItem("name");
             //    if (attr != null)
             //        Console.WriteLine(attr.Value);
             //}

             // если узел - Path
             if (xnode.Name == "Path")
             {
              //Console.WriteLine("Компания: {0}", childnode.InnerText);
              initMap.Path = xnode.InnerText;
              pathMap = initMap.Path;
              objRasterMapControl.OpenMap(pathMap);

             }

            //// обходим все дочерние узлы элемента user
            //foreach (XmlNode childnode in xnode.ChildNodes)
           //{
           //    // если узел - company
           //    if (childnode.Name == "Path")
           //    {
           //        //Console.WriteLine("Компания: {0}", childnode.InnerText);
           //        initMap.Path = childnode.InnerText;
           //        pathMap = initMap.Path;
           //        objRasterMapControl.OpenMap(pathMap);
           //    }
           //    // если узел age
           //    //if (childnode.Name == "age")
           //    //{
           //    //    Console.WriteLine("Возраст: {0}", childnode.InnerText);
           //    //}

           //} // foreach (XmlNode childnode in xnode.ChildNodes)

            } // foreach(XmlNode xnode in xRoot)

            MapOpened = true;
            return "";
*/
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> XML

namespace WpfMapRastrControl
{
    public class ClassMapRastrMenu

    {
        // Element for MAP
        private RasterMapControl objRasterMapControl;
        // Class for Interface
        //private ClassInterfaceMap objClassInterfaceMap;

        public ClassMapRastrMenu(RasterMapControl objRasterMapControl1)
        {
            objRasterMapControl = objRasterMapControl1;
            //objClassInterfaceMap = new ClassInterfaceMap(objRasterMapControl);

        }

        // PathMapFile ***********************************************************************
        // Returns path to map from .yaml file/"" - in Setting.json/Setting.yaml/Setting.xml)

        public string PathMapFile()
        {

            // JSON >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            /*
                       // Функции чтения и записи в .json файл вынесены в отдельный класс

                        // Путь к карте
                        string pathMap = "";
                        string s = "";

                        InitMap initMap = new InitMap("","",0);

                        s = ClassJSON.ReadJson("Setting.json",ref initMap);

                        // Файл .json прочитался
                        if (s=="")
                            {

                            // Получить текущую директорию исполняемого файла
                            string dir = "";
                            dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

                            // Сама карта в текущей директории
                            string sdop = "";
                            sdop= initMap.Path;
                            // Полный путь
                            pathMap = dir+sdop;

                            // Карта открылась
                            try
                            {
                                   objRasterMapControl.OpenMap(pathMap);
                                   MapOpened = true;
                                   return "";
                                  } //try (Карта открылась)

                                 // Карта не открылась
                                 catch (Exception ex)
                                  {
                                   MapOpened = false;
                                   return "Can't load map";
                                  } // Catch (Карта не открылась)

                            } // IF( Файл .json прочитался)

                            // Файл .json не прочитался
                            else
                            {
                               MapOpened = false;
                               return s;
                            } // ELSE (// Файл .json не прочитался)
            */
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> JSON

            // YAML >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // Functions of reading/writing to .yaml file are in a separate class 

            // Path to map
            string pathMap = "";
            string s = "";

            InitMapYaml initMapYaml = new InitMapYaml();
            s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);

            // File .yaml was read
            if (s == "")
            {
                // 999 -> Now we use the full path
                // Get current directory of executable file
                string dir = "";
                //dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

                // The map from current directory
                string sdop = "";
                sdop = initMapYaml.Path + "\\1_1-1.tif";
                // Full path
                pathMap = dir + sdop;
                return pathMap;

            } // IF( Файл .yaml was read)

            // File .yaml wasn't read
            else
            {
                return "";

            } // ELSE (// File .yaml wasn't read)

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> YAML

            // XML >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // Функции чтения и записи в .xml файл вынесены в отдельный класс
            /*
                        // Путь к карте
                        string pathMap = "";
                        string s = "";
                        string s1 = "";

                        InitMapXml initMapXml = new InitMapXml();

                        s = ClassXML.ReadXml("Setting.xml", "Path",ref s1);

                        // Файл .yaml прочитался
                        if (s == "")
                        {
                            initMapXml.Path = s1;

                            // Получить текущую директорию исполняемого файла
                            string dir = "";
                            dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

                            // Сама карта в текущей директории
                            string sdop = "";
                            sdop = initMapXml.Path;
                            // Полный путь
                            pathMap = dir + sdop;

                            // Карта открылась
                            try
                            {
                                objRasterMapControl.OpenMap(pathMap);
                                MapOpened = true;
                                return "";
                            } //try (Карта открылась)

                            // Карта не открылась
                            catch (Exception ex)
                            {
                                MapOpened = false;
                                return "Can't load map";
                            } // Catch (Карта не открылась)

                        } // IF( Файл .xml прочитался)

                        // Файл .xml не прочитался
                        else
                        {
                            MapOpened = false;
                            return s;
                        } // ELSE (// Файл .xml не прочитался)
            */
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> XML

        }
        // *********************************************************************** PathMapFile

        // PathMapMatrixDialog ***************************************************************
        // Returns path to map/matrix from dialog window

        public string PathDialog()
        {

            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
              return openFileDialog.FileName;
            }

            else
            {
                return "";
            }

        }
        // *************************************************************** PathMapMatrixDialog

        // PathMatrixFile ********************************************************************
        // Get path to the height matrix from .yaml file (Setting.json/Setting.yaml/Setting.xml)

        public string PathMatrixFile()
        {

            // YAML >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // Functions of reading/writing to .yaml file are in a separate class 

            // Path to map
            string pathMatrix = "";
            string s = "";

            InitMapYaml initMapYaml = new InitMapYaml();
            s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);

            // File .yaml was read
            if (s == "")
            {
                // 999 -> Now we use the full path
                // Get current directory of executable file
                string dir = "";
                //dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

                // The height matrix from current directory
                string sdop = "";
                sdop = initMapYaml.PathMatr;
                // Full path
                pathMatrix = dir + sdop;

                return pathMatrix;

            } // IF( Файл .yaml was read)

            // File .yaml wasn't read
            else
            {
                return "";

            } // ELSE (// File .yaml wasn't read)

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> YAML

        }
        // ******************************************************************** PathMatrixFile

        // ReadScaleFile *********************************************************************
        // Read current scale from .yaml file

        public double ReadScaleFile()
        {
            // YAML >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // Functions of reading/writing to .yaml file are in a separate class 

            // Scale
            double Resol = 0;
            string s = "";

            InitMapYaml initMapYaml = new InitMapYaml();
            s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);

            // File .yaml was read
            if (s == "")
             {
                Resol = initMapYaml.Scale;
                return Resol;

            } // IF( Файл .yaml was read)

            // File .yaml wasn't read
            else
             {
              return -1;

             } // ELSE (// File .yaml wasn't read)

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> YAML
            
        }
        // ********************************************************************* ReadScaleFile

        // WriteScaleFile ********************************************************************
        // Write current scale to .yaml file

        public string WriteScaleFile()
        {
            // YAML >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            string s = "";

            InitMapYaml initMapYaml = new InitMapYaml();
            s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);

            // ...........................................................................
            // File .yaml was read

            if (s == "")
            {
                // Current scale
                initMapYaml.Scale = objRasterMapControl.Resolution;
                // otl
                //initMapYaml.Scale = 38271;

                // Пишем класс с новым масштабом обратно в файл
                try
                {
                    ClassYaml.WriteYaml(
                                        "Setting.yaml",
                                        initMapYaml
                                       );
                    return "";
                }
                catch
                {
                    if (GlobalVarMap.FlagLanguageMap == false)
                        return "Unable to keep current scale: can't write file";
                    else
                        return "Текущий масштаб не сохранен: невозможно записать файл";

                }

            } // IF( Файл .yaml was read)
            // ...........................................................................
            // File .yaml wasn't read

            else
            {
                if (GlobalVarMap.FlagLanguageMap == false)
                    return "Unable to keep current scale: can't read file";
                else
                    return "Текущий масштаб не определен: невозможно прочитать файл";


            } // ELSE (// File .yaml wasn't read)
            // ...........................................................................

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> YAML

        }
        // ******************************************************************** WriteScaleFile



    } // Class
} // Namespace
