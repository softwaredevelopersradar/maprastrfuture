﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

// Semen
using System.Drawing;
using System.Runtime.InteropServices;
using System.IO;

using System.Data;
using System.Reflection;
//using System.ServiceModel;
using System.Diagnostics;
using System.Threading;
using System.Globalization;
using Microsoft.Win32;

using ModelsTablesDBLib;

namespace WpfTasksControl
{
    /// <summary>
    /// Interaction logic for UserControlTasks.xaml
    /// </summary>
    public partial class UserControl1 : UserControl
    {

        // 333
        public event EventHandler OnTaskAzimuth;
        public event EventHandler OnTaskTriang;

        public UserControl1()
        {
            InitializeComponent();

        }

        // ButtonAzimuth **********************************************************************************
        // 333
        public void ButtonAzimuthTsk_Click(object sender, RoutedEventArgs e)
        {
            // 333
            OnTaskAzimuth?.Invoke(this, new EventArgs());
        }
        // ********************************************************************************** ButtonAzimuth

        // ButtonTriang ***********************************************************************************
        public void ButtonTriangTsk_Click(object sender, RoutedEventArgs e)
        {
            OnTaskTriang?.Invoke(this, new EventArgs());
        }
        // *********************************************************************************** ButtonTriang





    } //  Class
} // Namespace
