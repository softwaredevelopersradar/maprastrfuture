﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(bool), targetType: typeof(string))]
    public class MapConverter: System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)value == true)
                return "true";
            else
                return "false";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {

            switch (value.ToString())
            {
                case "yes":
                    return true;
                case "no":
                    return false;
            }
            return false;
        }

    } // Class
} // Namespace
